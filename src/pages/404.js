import React from 'react'

export default () => (
  <div style={{"text-align": "center"}}>
    <h1>404</h1>
    <h5>Life's the biggest trip there is, and you just made a wrong turn.</h5>
  </div>
)
