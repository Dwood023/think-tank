import React, { Component } from 'react'
import { createGlobalStyle } from 'styled-components';
import { Root, Head, Routes, withSiteData } from 'react-static'
import Header from './components/header'
import Footer from './components/footer'

import colors from './colors'

const GlobalStyles = createGlobalStyle`
	@import url('https://fonts.googleapis.com/css?family=Merriweather|Libre+Baskerville');

	h1,h2,h3,h4,h5,h6 {
		font-family: "Libre Baskerville";
        text-shadow: 2px 2px black;
	}
	body {
		background-color: ${colors.bg};
		color: ${colors.font};
		font-family: "Merriweather";
		font-size: 11pt;
		@media (max-width: 720px) {
			font-size: 9pt;
		}
		line-height: 1.5;

		margin: 0;
		padding: 0;
		width: 100%;
		height: 100%;
		#root {
			height: 100%;
			& > div {
				min-height: 80%;
			}
		}
	}
`;


class App extends Component {
	render() {
		return (
			<Root>
				<GlobalStyles/>
				<Head>
					<title>{this.props.title}</title>
					<link rel="apple-touch-icon" sizes="180x180" href="/media/icon/apple-touch-icon.png"/>
					<link rel="icon" type="image/png" sizes="32x32" href="/media/icon/favicon-32x32.png"/>
					<link rel="icon" type="image/png" sizes="16x16" href="/media/icon/favicon-16x16.png"/>
					<link rel="manifest" href="/media/icon/site.webmanifest"/>
					<link rel="mask-icon" href="/media/icon/safari-pinned-tab.svg" color="#5bbad5"/>
					<meta name="msapplication-TileColor" content="#da532c"/>
					<meta name="theme-color" content="#ffffff"></meta>
				</Head>
				<Header/>
				<main>
					<Routes />
				</main>
				<Footer/>
			</Root>
		)
	}
}

export default withSiteData(App)