import React from 'react'
import styled from 'styled-components'
import {withRouteData} from 'react-static'
import Column from './column'
import colors from '../colors'

const Header = styled.header`
    * {
        padding-bottom: 2rem;
    }
`;

const Content = styled.article`
    p {
        margin-bottom: 2rem;
    }
    a {
        color: ${colors.font}
    }
`;
const About = ({html, summary, title}) => (
    <Column>
        <Header>
            <h2>{title}</h2>
            <h3>{summary}</h3>
        </Header>
        <Content dangerouslySetInnerHTML={{__html: html}}/>
    </Column>
)
export default withRouteData(About)