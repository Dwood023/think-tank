import React from 'react'
import styled from 'styled-components';

import colors from '../colors'
import {NavLink} from './link'

const L = styled(NavLink)`
	font-size: 1.1rem;
	letter-spacing: 0.2rem;
	margin: 0rem 2rem;
	/* Extend underline to cover for letter-spacing */	
	padding-left: 0.2rem;
	@media (max-width: 500px) {
		letter-spacing: 0rem;
		padding-left: 0;
		margin: 0 1rem;
		font-size: 1rem;
	}
	&.active {
		border-bottom: 4px solid ${colors.font};
		border-radius: 0.2rem;
	}
`;

const is_root = (match, location) => match || location.pathname.startsWith("/goal")

const Nav = () => <nav> 
	<L to="/blog">Blog</L>
	<L isActive={is_root} exact to="/">Home</L>
	<L to="/about">About</L>
</nav>

export default Nav;