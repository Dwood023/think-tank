import React from 'react'
import styled from 'styled-components'
import Column from './column';

const Footer = styled.footer`
    font-size: 0.6rem;
    a {
        color: inherit;
        text-decoration: none;
    }
`;

const credit_icons = <div>
    Icons made by <a href="https://www.freepik.com/" title="Freepik">
        Freepik
    </a> from <a href="https://www.flaticon.com/" title="Flaticon">
        www.flaticon.com
    </a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">
        CC 3.0 BY
    </a>
</div>
const C = styled(Column)`
    padding: 2rem 5rem;
    text-align: center;
    filter: brightness(60%);
`;

const Bar = styled.hr`
    margin-bottom: 2rem;
    filter: brightness(80%);
`;

export default () => (

    <Footer>
        <C>
            <Bar/>
        </C>
    </Footer>
)
