import React from 'react'
import styled from 'styled-components';

const PostStyling = styled.div`
	.quoteblock {
		filter: brightness(80%);
		margin: 3rem 1rem;
		.attribution {
			text-align: right;
			font-size: 0.8rem; 
		}
		blockquote {
			text-align: center;
		}
	}

	.argument {
		a {
			&:before {
				content: "(See ";
			}
			&:after {
				content: ")";
			}
			font-size: 0.7rem;
			display: block;
		}
		ol {
			padding-left: 1rem;
		}
		.paragraph:last-child {
			&::before {
				border-top: 1px solid whitesmoke;
				content: "";
				width: 100%;
				display: inline-block;
			}
			p {
				margin-bottom: 0;
				padding: 0 1rem;
			}
		}
		.title {
			font-weight: bold;
			font-size: 0.7rem;
		}
		border: 1px dotted whitesmoke;
		padding: 1.5rem;
		font-size: 0.7rem;
		margin: 3rem 2rem;
		p {
			margin: 1rem 0;
		}

		.impl {
			&:before {
				content: "[";
			}
			&:after {
				content: "]";
			}
		}
	}
	line-height: 1.8;
    .sidenote {
		filter: brightness(70%);
        font-size: 0.7rem;
		display: block;
		padding: 1rem;

		@media only screen and (min-width: 1400px) {
			float: right;
			position: relative;
			top: -2rem;
			left: 250px;
			margin-left: -250px;
			width: 200px;
			clear: right;
			padding-top: 0;
		}
    }
	a {
		color: inherit;
	}
	p {
		margin: 2rem 0;
	}
	#footnotes {
		font-size: 0.7rem;
		filter: brightness(70%);
		hr {
			margin-bottom: 2rem;
		}
	}
`;

const PostContent = ({html, className}) => <PostStyling className="content" 
    dangerouslySetInnerHTML={{__html: html}}
    className={className}
/>

export default PostContent