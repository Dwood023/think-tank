import React from 'react'
import styled from 'styled-components';
import { withRouteData } from 'react-static';

import colors from '../../colors'
import Link from '../link'
import PostHeading from './post-heading'
import PostContent from './post-content'

const Page = styled.div`
    max-width: 900px;
    margin: 0 auto;
    padding: 0 2rem;
`;
const List = styled.ul`
    list-style: none;
    padding: 0;
`;
const Preview = styled(PostContent)`
    height: 4rem;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 0.8rem;
`;

const ListItem = styled.li`
    border-left: 10px solid ${colors.font};
    border-radius: 8px;
    padding-left: 1rem;
    margin-bottom: 1.5rem;
    background: linear-gradient(0deg, rgba(0,0,0,0.1) 0%, rgba(255,255,255,0) 20%); 
`;

const get_entry = post => (
    <ListItem key={post.title}>
        <Link to={`/blog/${post.slug}`}>
            <PostHeading post={post}/>
            <Preview html={post.body}/>
        </Link>
    </ListItem>
);

const Posts = ({posts}) => (
    <Page>
        <List>
            {posts.map(get_entry)}
        </List>
    </Page>
)

export default withRouteData(Posts)