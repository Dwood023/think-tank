import React from 'react'
import styled from 'styled-components';
import Date from '../date'

const Metadata = styled.div`
`;

const Title = styled.h2`
    @media (max-width: 700px) {
        font-size: 1rem;
    }
    font-size: 2rem;
    margin: 0 0 0.5rem 0;
    display: inline-block;
    filter: brightness(110%);
`;
const Author = styled.address`
    text-transform: uppercase;
    font-style: normal;
    font-weight: bold;
`;

const GoalRef = styled.div`
    font-weight: bold;
    color: whitesmoke;
    background-color: rgb(20,20,20);
    padding: 5px 10px;
    text-align: center;
    margin-bottom: 0.5rem;
    @media (min-width: 500px) {
        border-radius: 7px 0 0 7px;
        text-align: right;
        float: right;
    }
`;

const PostHeading = ({post}) => (
    <header>
        <Title>{post.title}</Title>
        <Metadata>
            {  
                post.related_goal ? 
                    <GoalRef>{post.related_goal}</GoalRef> : null 
            }
            <Author>{post.author}</Author>
            <Date date={post.date}/>
        </Metadata>
    </header>
)

export default PostHeading
