import React from 'react'
import { withRouteData } from 'react-static';
import styled from 'styled-components'

import Link from '../link'
import Column from '../column'
import PostHeading from './post-heading'
import PostContent from './post-content'

const Post = ({post}) => (
    <Column>
        <article>
            <PostHeading post={post}/>
            <br/>
            <PostContent html={post.body}/>
        </article>
    </Column>
)

export default withRouteData(Post)