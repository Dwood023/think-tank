import React from 'react'
import styled, { css } from 'styled-components'
import colors from '../colors'

import { 
    Link as RouterLink, 
    NavLink as RouterNavLink 
} from 'react-router-dom'

const common_style = css`
    text-decoration: none;
    color: inherit;
    outline:none;
`;

export const NavLink = styled(RouterNavLink)`
    ${common_style}

/*     &:hover {
        background-color: ${colors.font};
        color: ${colors.bg};
    } */
`;
export default styled(RouterLink)`${common_style}`;