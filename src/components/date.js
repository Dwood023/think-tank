import React, {dangerouslySetInnerHTML} from 'react'
import styled from 'styled-components';

const options = {
    day: "numeric",
    month: "short",
    year: "numeric"
}

export default ({date}) => <time dateTime={date}>
    {new Date(date).toLocaleString('en-GB', options)}
</time>