import React from 'react'
import styled from 'styled-components';
import Tile from './tile'
import { withRouteData } from 'react-static'

const Grid = styled.div`
    @media (min-width: 1024px) {
        display: grid;
        grid-template-columns: 1fr 1fr;
    }
    background-color: rgba(0,0,0,0.1);
`;

const Tiles = ({tiles}) => <Grid>
{ 
    tiles.map( tile =>
        <Tile key={tile.title} tile={tile} /> 
    )
}
</Grid>

export default Tiles