import React from 'react'
import styled from 'styled-components'

import Link from '../link'

const Goal = styled.div`
    background-image: url("${props => props.bg}");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

    padding: 2rem;
    text-align: center;

    height: 250px;
    display: flex;
    align-items: center;
    justify-content: center;

    :hover {
        filter: brightness(120%);
    }
`;

const TextArea = styled.div`
    color: whitesmoke;
    background-color: rgba(0,0,0,0.8);

    padding: 2rem 4rem;

    /* Cram description in on tiny screens */
    @media (max-width: 720px) {
        padding: 1rem 2rem;
        font-size: 10pt;
    }
`;

export default ({tile}) => (
    <Link to={tile.url}>
        <Goal bg={tile.background_image}>
            <TextArea>
                    <h3>{tile.title}</h3>
                    <p>{tile.description}</p>
            </TextArea>
        </Goal>
    </Link>
)