import React from 'react'
import styled from 'styled-components'
import Tiles from './tile/tiles'
import {withRouteData} from 'react-static'
import Column from './column'
import Commitee from './committee'
import Section from './section'

const Tagline = styled(Column)`
    text-shadow: 2px 2px black;
    padding: 8rem 4rem 7rem 4rem;
    sup {
        font-weight: 300;
    }
    p {
        text-align: right;
        strong { font-size: 1.5rem;}
    }
`;

const Home = ({goals, projects}) => <div>
    <Tagline>
        <h3>A student-led Think Tank for doers</h3> 
        <h2>Researching, discussing, and solving<sup>*</sup> life's big problems.</h2>
        <br/>
        <p>* hey, you never know <strong>&nbsp;😅</strong></p>
    </Tagline>
    <Section title="Goals">
        <Tiles tiles={goals}/>
    </Section>
    <Section title="Projects">
        <Tiles tiles={projects}/>
    </Section>
    <Section title="Committee">
        <Commitee/>
    </Section>
</div>

export default withRouteData(Home)
