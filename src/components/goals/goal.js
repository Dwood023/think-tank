import React, {dangerouslySetInnerHTML} from 'react'
import styled from 'styled-components';
import { withRouteData } from 'react-static';
import Column from '../column'
import Link from '../link'
    
const Background = styled.div`
    height: 100%;
    width: 100%;
    background-image: url(${props => props.bg});
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
`;

const Header = styled.header`
    text-align: center;
    color: whitesmoke;
`;

const Col = styled(Column)`
    padding: 4rem;
    height: 100%;
    background-color: rgba(0,0,0,0.8);
`;

const Content = styled.article`
    color: rgb(220,220,220);
`;

const L = styled(Link)`
    color: whitesmoke;
`;

const Goal = ({goal}) => (
    <Background bg={goal.background_image}>
        <Col>
            <L to="/">Back</L>
            <Header>
                <h2>{goal.title}</h2>
                <h5>{goal.description}</h5>
            </Header>
            <Content dangerouslySetInnerHTML={{__html: goal.body}}/>
        </Col>
    </Background>
)
export default withRouteData(Goal)