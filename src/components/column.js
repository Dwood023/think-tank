import React from 'react'
import styled from 'styled-components'

const Column = styled.div`
    max-width: 900px;
    margin: auto;
    padding: 0 4rem;
    box-sizing: border-box;
    @media (max-width: 500px) {
        padding: 2rem 2rem;
    }
`;

export default Column