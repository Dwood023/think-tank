import React from 'react'
import styled from 'styled-components'
import Nav from './nav'
import Title from './title'

const H = styled.header`
	text-transform: uppercase;
	padding-top: 2rem;
	text-align: center;
	margin-bottom: 2rem;
    h1,h2,h3,h4,h5,h6, a {
    }
`;

export default () => <H>
    <Title/>
    <Nav/>
</H>