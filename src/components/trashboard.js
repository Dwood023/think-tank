import React from 'react'
import styled from 'styled-components'
import {withRouteData} from 'react-static'

const Item = styled.span`
    background-color: rgba(0,0,0,0.1);
    padding: 0.5rem;
    margin: 1rem;
`;

const Trash = ({trashes}) => (
    <div>
        {
            trashes.ramblings.map(trash => <Item>{trash}</Item>)
        }
    </div>
)
export default withRouteData(Trash)