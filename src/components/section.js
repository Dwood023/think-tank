import React from 'react'
import styled from 'styled-components'

const S = styled.div`
`;

const Title = styled.h3`
    padding-top: 2.5rem;
    padding-bottom: 1rem;
    padding-left: 0.3rem;

    letter-spacing: 0.5rem;
    
    text-align: center;
    text-transform: uppercase;
    font-size: 1.6rem;
    @media (max-width: 600px) {
        font-size: 1rem;
    }
`;

const Section = props => (
    <S>
        <Title>{props.title}</Title>
        {props.children}
    </S>
)
export default Section
