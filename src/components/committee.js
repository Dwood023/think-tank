import React from 'react'
import styled from 'styled-components'

const members = [
    { 
        name: "David Wood", 
        roles: [
            "Founder",
            "President"
        ]
    },
    { 
        name: "Archie Hailes", 
        roles: [
            "Founder",
            "Vice-President",
            "Welfare Officer" 
        ]
    },
    { 
        name: "Alex Dann", 
        roles: [
            "Founder",
            "Secretary" 
        ]
    },
    { 
        name: "Jack Horniman", 
        roles: [
            "Founder",
            "Treasurer" 
        ]
    },
]

const Flex = styled.div`
    display: flex;
    justify-content: space-evenly;
    @media (max-width: 600px) {
        flex-direction: column;
    }
`;
const M = styled.div`
    text-align: center;
    padding: 0 1rem;
    p {
        font-size: 0.8rem;
    }
`;

const member = ({name, roles}) => (
    <M>
        <h3>{name}</h3>
        {
            roles.join("/")
        }
    </M>
)

const Commitee = () => <Flex>
    {
        members.map(member)
    }
</Flex>

export default Commitee