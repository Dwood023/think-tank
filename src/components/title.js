import React, { Component } from 'react'
import {withSiteData} from 'react-static'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const T = styled.h1`
	letter-spacing: 0.8rem;
	@media (max-width: 500px) {
		letter-spacing: 0.2rem;
	}
    margin-bottom: 0.2rem;
`;

const Tagline = styled.h6`
    font-size: 0.7rem;
    letter-spacing: 0.1rem;
    font-weight: 300;
    margin-top: 0;
`;

const Title = ({title}) => (
    <div>
        <T>{title}</T>
        <Tagline>@ Reading University</Tagline>
    </div>
)

export default withSiteData(Title)