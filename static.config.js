import fs from 'fs'
import glob from 'fast-glob'
import fm from 'front-matter'
import slug from 'slug'
import marked from 'marked'

let asciidoctor = require('asciidoctor.js')();

// There's a bug preventing this - check github
// import { reloadRoutes } from 'react-statinset ic/node'
// import chokidar from 'chokidar'
// chokidar.watch('./content').on('all', () => reloadRoutes())

export default {
	siteRoot: "https://think-tank.netlify.com/",
	plugins: [
		"react-static-plugin-styled-components",
		"react-static-plugin-react-router",
	],
	getSiteData: async () => ({
		title: 'Think Tank',
	}),
	getRoutes: async () => {
		
		const goals = await glob("content/goals/*.json")
			.then(paths => paths.map(
				path => JSON.parse(fs.readFileSync(path))
			).map( ({title, description, body, background_image}) => ({
				title, description, background_image,
				url: `/goal/${slug(title)}`,
				body: marked(body)
			}))
		);
		const projects = await glob("content/projects/*.json")
			.then(paths => paths.map(
				path => JSON.parse(fs.readFileSync(path))
			).map( ({title, description, body, related_goal, background_image}) => ({
				title, description, related_goal, background_image,
				url: `/project/${slug(title)}`,
				body: marked(body)
			}))
		);

		const formats = [
			{ ext: "md", conv: marked },
			{ ext: "adoc", conv: asciidoctor.convert.bind(asciidoctor) },
		];

		const get_posts = (format) => (
			glob.sync(`content/blog_posts/*.${format.ext}`)
				.map( path => fm(fs.readFileSync(path, 'utf8')))
				.map( post => ({
					slug: slug(post.attributes.title),
					title: post.attributes.title,
					author: post.attributes.author,
					date: post.attributes.date,
					related_goal: post.attributes.related_goal,
					body: format.conv(post.body),
				}))
		);

		const posts = [].concat.apply([], 
			formats.map(format => get_posts(format))
			).sort(
				(a, b) => new Date(a.date) - new Date(b.date)
			).reverse();

		const about = fm(fs.readFileSync('content/about.md', 'utf8'));
		const trashes = JSON.parse(fs.readFileSync('./ramblings.json'));
			
		return [
			{
				path: '/',
				component: 'src/components/home',
				getData: () => ({ goals, projects }),
				children: goals.map( goal => ({
					path: goal.url,
					component: 'src/components/goals/goal',
					getData: () => ({ goal })
				}))
			},
			{
				path: 'blog',
				component: 'src/components/blog/posts',
				getData: () => ({ posts }),
				children: posts.map( post => ({
					path: post.slug,
					component: 'src/components/blog/post',
					getData: () => ({ post })
				}))
			},
			{
				path: 'about',
				component: 'src/components/about',
				getData: () => ({
					title: about.attributes.title,
					summary: about.attributes.summary,
					html: marked(about.body)
				})
			},
			{
				path: 'trash',
				component: 'src/components/trashboard',
				getData: () => ({trashes})
			}
		]
	}
}