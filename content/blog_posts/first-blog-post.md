---
title: First Blog Post
date: 2019-03-18T18:26:01.362Z
author: David Wood
---
This is an example blog post, members will be able to write their own from the
[admin panel](https://analytic-society.netlify.com/admin/)
.

Right now, posts can be associated with an existing "goal".
In future this might be further subdivided into "subgoals".

The post editor in the admin panel is in a format called "Markdown", but can be switched to a simple "rich-text" editor (like word).
