---
title: Nonsense
date: 2019-03-18T20:26:01
author: David Wood
related_goal: Formulate psychically-healthy society
---

We've lived in such a small period of time, swamped in information, so that we believe we know all there is to know.

Things may not have always been this way.

The record of history being so degraded, might we not have entirely missed the point of life?

Could the 'end of human history' be just over the next horizon?
