---
title: Preliminary mission statement
summary: To undermine the broader foundations of human suffering
---
Modern life has become overwhelmingly complex. Problems are becoming ever more networked and technical, as society itself progresses in these directions.

The concept of the "Think Tank", as an independent research group has been one solution to this increasing complexity. However, conventional think tanks are inherently limited, and perhaps biased overall in several ways.

1. According to the availability of funding 

Typically from governments, coorperations, or unusually wealthy individuals.

2. According to the biases of their members.

Assuming only a certain kind of person is likely to seek, and succeed in recruitment by these organisations. 

This society seeks the same utility - collaboration of specialists into research and understanding of key problems - without succumbing to the limitations of privately funded groups.
This is a think tank of students -  for students, and the collective good of mankind.

Furthermore, unlike conventional think tanks, this group will attempt to **implement** whatever solutions it arrives at through research.

If you've noticed something wrong with your life, community, society, economy, or reality - join us, and we'll try to figure something out.

## How do I get involved?

Email [here](mailto:david.wood@student.reading.ac.uk), or come along to one of our meetings.

Once you've joined officially, you'll receive an email invite to contribute to the website via the [admin interface](/admin/). From there, you'll be able to directly add and update goals, projects, and blog posts.

​In the meantime, we need to find 50 signatures:

​In the meantime, we need to find 50 signatures:​

https://docs.google.com/forms/d/e/1FAIpQLSeO5Td5H9vyVqB8eL2G1k2tGI8tx-TYchGbAO8CgrZ9oaLkRw/viewform
