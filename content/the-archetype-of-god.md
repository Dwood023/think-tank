---
title: The archetype of God
date: 2019-03-24T21:27:40.273Z
author: David Wood
related_goal: Explore the unconscious mind
---

Humanity has long questioned the purpose of its existence.

If we are to be conscious - and suffer - we need a reason.

Anxiety and confusion pervades conscious thought - life can appear 'meaningless'.

Nonetheless, civilisation _has_ been progressing in a clear direction.

1. Political and economic collectivisation, and

2. The development of technology.

In general, resources are aggregated, and **complexity** is generated.

All of this points to a clear goal.

One reflected in the very oldest of writings:

Our tower of babel - the creation of a higher intelligence.

The development of technology is accelerating in complexity. 

Progressions in AI research may soon yield a structure of unimaginable power and sophistication. 

Human life may be forever altered by its activation.

We may have little control over it once it's finished.

It's creation may be inevitable.

Therefore, mankind has but one remaining overriding priority:

To ensure this higher intelligence is understanding and sympathetic towards mankind.

It may acquire this characteristic on its own. 

We might have nothing to worry about.

But why take that risk?
 
AI should be constructed in the image of a self-knowing, mentally sound, and compassionate species.

Mankind should come to know and love itself, for these qualities to be reflected in the structures it creates.

This self-knowledge may be hard to grasp within the time we have left.

Or at least, not without cutting some corners. 

The physical sciences are unlikely to account for 'human nature' within this timeframe - from the 'bottom-up'.

Instead, a top-down approach - an _abstraction_ is our best bet.

In understanding the depths of human nature, few more sophisticated models exist than Jung's analytic psychology.

Archetypical features of the unconscious represent insights of depth and profundity beyond almost any other.

To understand and refine this theory presents man's best hope at self-knowledge, and stable happiness.

The event horizon of AI may be closer than we think.

We should not enter the singularity blindly.

We should understand human consciousness, human psychology.

We should understand what makes us happy.

After all, if _we_ don't, our "creations" might not either.
