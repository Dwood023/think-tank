(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react')) :
	typeof define === 'function' && define.amd ? define(['exports', 'react'], factory) :
	(factory((global.ReactColor = {}),global.React));
}(this, (function (exports,React) { 'use strict';

	var React__default = 'default' in React ? React['default'] : React;

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function unwrapExports (x) {
		return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
	}

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	/** Detect free variable `global` from Node.js. */
	var freeGlobal = typeof commonjsGlobal == 'object' && commonjsGlobal && commonjsGlobal.Object === Object && commonjsGlobal;

	var _freeGlobal = freeGlobal;

	/** Detect free variable `self`. */
	var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

	/** Used as a reference to the global object. */
	var root = _freeGlobal || freeSelf || Function('return this')();

	var _root = root;

	/** Built-in value references. */
	var Symbol$1 = _root.Symbol;

	var _Symbol = Symbol$1;

	/** Used for built-in method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString = objectProto.toString;

	/** Built-in value references. */
	var symToStringTag = _Symbol ? _Symbol.toStringTag : undefined;

	/**
	 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the raw `toStringTag`.
	 */
	function getRawTag(value) {
	  var isOwn = hasOwnProperty.call(value, symToStringTag),
	      tag = value[symToStringTag];

	  try {
	    value[symToStringTag] = undefined;
	    var unmasked = true;
	  } catch (e) {}

	  var result = nativeObjectToString.call(value);
	  if (unmasked) {
	    if (isOwn) {
	      value[symToStringTag] = tag;
	    } else {
	      delete value[symToStringTag];
	    }
	  }
	  return result;
	}

	var _getRawTag = getRawTag;

	/** Used for built-in method references. */
	var objectProto$1 = Object.prototype;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString$1 = objectProto$1.toString;

	/**
	 * Converts `value` to a string using `Object.prototype.toString`.
	 *
	 * @private
	 * @param {*} value The value to convert.
	 * @returns {string} Returns the converted string.
	 */
	function objectToString(value) {
	  return nativeObjectToString$1.call(value);
	}

	var _objectToString = objectToString;

	/** `Object#toString` result references. */
	var nullTag = '[object Null]',
	    undefinedTag = '[object Undefined]';

	/** Built-in value references. */
	var symToStringTag$1 = _Symbol ? _Symbol.toStringTag : undefined;

	/**
	 * The base implementation of `getTag` without fallbacks for buggy environments.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the `toStringTag`.
	 */
	function baseGetTag(value) {
	  if (value == null) {
	    return value === undefined ? undefinedTag : nullTag;
	  }
	  return (symToStringTag$1 && symToStringTag$1 in Object(value))
	    ? _getRawTag(value)
	    : _objectToString(value);
	}

	var _baseGetTag = baseGetTag;

	/**
	 * Checks if `value` is classified as an `Array` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
	 * @example
	 *
	 * _.isArray([1, 2, 3]);
	 * // => true
	 *
	 * _.isArray(document.body.children);
	 * // => false
	 *
	 * _.isArray('abc');
	 * // => false
	 *
	 * _.isArray(_.noop);
	 * // => false
	 */
	var isArray = Array.isArray;

	var isArray_1 = isArray;

	/**
	 * Checks if `value` is object-like. A value is object-like if it's not `null`
	 * and has a `typeof` result of "object".
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 * @example
	 *
	 * _.isObjectLike({});
	 * // => true
	 *
	 * _.isObjectLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isObjectLike(_.noop);
	 * // => false
	 *
	 * _.isObjectLike(null);
	 * // => false
	 */
	function isObjectLike(value) {
	  return value != null && typeof value == 'object';
	}

	var isObjectLike_1 = isObjectLike;

	/** `Object#toString` result references. */
	var stringTag = '[object String]';

	/**
	 * Checks if `value` is classified as a `String` primitive or object.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
	 * @example
	 *
	 * _.isString('abc');
	 * // => true
	 *
	 * _.isString(1);
	 * // => false
	 */
	function isString(value) {
	  return typeof value == 'string' ||
	    (!isArray_1(value) && isObjectLike_1(value) && _baseGetTag(value) == stringTag);
	}

	var isString_1 = isString;

	/**
	 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
	 *
	 * @private
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {Function} Returns the new base function.
	 */
	function createBaseFor(fromRight) {
	  return function(object, iteratee, keysFunc) {
	    var index = -1,
	        iterable = Object(object),
	        props = keysFunc(object),
	        length = props.length;

	    while (length--) {
	      var key = props[fromRight ? length : ++index];
	      if (iteratee(iterable[key], key, iterable) === false) {
	        break;
	      }
	    }
	    return object;
	  };
	}

	var _createBaseFor = createBaseFor;

	/**
	 * The base implementation of `baseForOwn` which iterates over `object`
	 * properties returned by `keysFunc` and invokes `iteratee` for each property.
	 * Iteratee functions may exit iteration early by explicitly returning `false`.
	 *
	 * @private
	 * @param {Object} object The object to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @param {Function} keysFunc The function to get the keys of `object`.
	 * @returns {Object} Returns `object`.
	 */
	var baseFor = _createBaseFor();

	var _baseFor = baseFor;

	/**
	 * The base implementation of `_.times` without support for iteratee shorthands
	 * or max array length checks.
	 *
	 * @private
	 * @param {number} n The number of times to invoke `iteratee`.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the array of results.
	 */
	function baseTimes(n, iteratee) {
	  var index = -1,
	      result = Array(n);

	  while (++index < n) {
	    result[index] = iteratee(index);
	  }
	  return result;
	}

	var _baseTimes = baseTimes;

	/** `Object#toString` result references. */
	var argsTag = '[object Arguments]';

	/**
	 * The base implementation of `_.isArguments`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
	 */
	function baseIsArguments(value) {
	  return isObjectLike_1(value) && _baseGetTag(value) == argsTag;
	}

	var _baseIsArguments = baseIsArguments;

	/** Used for built-in method references. */
	var objectProto$2 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

	/** Built-in value references. */
	var propertyIsEnumerable = objectProto$2.propertyIsEnumerable;

	/**
	 * Checks if `value` is likely an `arguments` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
	 *  else `false`.
	 * @example
	 *
	 * _.isArguments(function() { return arguments; }());
	 * // => true
	 *
	 * _.isArguments([1, 2, 3]);
	 * // => false
	 */
	var isArguments = _baseIsArguments(function() { return arguments; }()) ? _baseIsArguments : function(value) {
	  return isObjectLike_1(value) && hasOwnProperty$1.call(value, 'callee') &&
	    !propertyIsEnumerable.call(value, 'callee');
	};

	var isArguments_1 = isArguments;

	/**
	 * This method returns `false`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.13.0
	 * @category Util
	 * @returns {boolean} Returns `false`.
	 * @example
	 *
	 * _.times(2, _.stubFalse);
	 * // => [false, false]
	 */
	function stubFalse() {
	  return false;
	}

	var stubFalse_1 = stubFalse;

	var isBuffer_1 = createCommonjsModule(function (module, exports) {
	/** Detect free variable `exports`. */
	var freeExports = exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule = freeExports && 'object' == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports = freeModule && freeModule.exports === freeExports;

	/** Built-in value references. */
	var Buffer = moduleExports ? _root.Buffer : undefined;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

	/**
	 * Checks if `value` is a buffer.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.3.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
	 * @example
	 *
	 * _.isBuffer(new Buffer(2));
	 * // => true
	 *
	 * _.isBuffer(new Uint8Array(2));
	 * // => false
	 */
	var isBuffer = nativeIsBuffer || stubFalse_1;

	module.exports = isBuffer;
	});

	/** Used as references for various `Number` constants. */
	var MAX_SAFE_INTEGER = 9007199254740991;

	/** Used to detect unsigned integer values. */
	var reIsUint = /^(?:0|[1-9]\d*)$/;

	/**
	 * Checks if `value` is a valid array-like index.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
	 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
	 */
	function isIndex(value, length) {
	  var type = typeof value;
	  length = length == null ? MAX_SAFE_INTEGER : length;

	  return !!length &&
	    (type == 'number' ||
	      (type != 'symbol' && reIsUint.test(value))) &&
	        (value > -1 && value % 1 == 0 && value < length);
	}

	var _isIndex = isIndex;

	/** Used as references for various `Number` constants. */
	var MAX_SAFE_INTEGER$1 = 9007199254740991;

	/**
	 * Checks if `value` is a valid array-like length.
	 *
	 * **Note:** This method is loosely based on
	 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
	 * @example
	 *
	 * _.isLength(3);
	 * // => true
	 *
	 * _.isLength(Number.MIN_VALUE);
	 * // => false
	 *
	 * _.isLength(Infinity);
	 * // => false
	 *
	 * _.isLength('3');
	 * // => false
	 */
	function isLength(value) {
	  return typeof value == 'number' &&
	    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER$1;
	}

	var isLength_1 = isLength;

	/** `Object#toString` result references. */
	var argsTag$1 = '[object Arguments]',
	    arrayTag = '[object Array]',
	    boolTag = '[object Boolean]',
	    dateTag = '[object Date]',
	    errorTag = '[object Error]',
	    funcTag = '[object Function]',
	    mapTag = '[object Map]',
	    numberTag = '[object Number]',
	    objectTag = '[object Object]',
	    regexpTag = '[object RegExp]',
	    setTag = '[object Set]',
	    stringTag$1 = '[object String]',
	    weakMapTag = '[object WeakMap]';

	var arrayBufferTag = '[object ArrayBuffer]',
	    dataViewTag = '[object DataView]',
	    float32Tag = '[object Float32Array]',
	    float64Tag = '[object Float64Array]',
	    int8Tag = '[object Int8Array]',
	    int16Tag = '[object Int16Array]',
	    int32Tag = '[object Int32Array]',
	    uint8Tag = '[object Uint8Array]',
	    uint8ClampedTag = '[object Uint8ClampedArray]',
	    uint16Tag = '[object Uint16Array]',
	    uint32Tag = '[object Uint32Array]';

	/** Used to identify `toStringTag` values of typed arrays. */
	var typedArrayTags = {};
	typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
	typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
	typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
	typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
	typedArrayTags[uint32Tag] = true;
	typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
	typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
	typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
	typedArrayTags[errorTag] = typedArrayTags[funcTag] =
	typedArrayTags[mapTag] = typedArrayTags[numberTag] =
	typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
	typedArrayTags[setTag] = typedArrayTags[stringTag$1] =
	typedArrayTags[weakMapTag] = false;

	/**
	 * The base implementation of `_.isTypedArray` without Node.js optimizations.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
	 */
	function baseIsTypedArray(value) {
	  return isObjectLike_1(value) &&
	    isLength_1(value.length) && !!typedArrayTags[_baseGetTag(value)];
	}

	var _baseIsTypedArray = baseIsTypedArray;

	/**
	 * The base implementation of `_.unary` without support for storing metadata.
	 *
	 * @private
	 * @param {Function} func The function to cap arguments for.
	 * @returns {Function} Returns the new capped function.
	 */
	function baseUnary(func) {
	  return function(value) {
	    return func(value);
	  };
	}

	var _baseUnary = baseUnary;

	var _nodeUtil = createCommonjsModule(function (module, exports) {
	/** Detect free variable `exports`. */
	var freeExports = exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule = freeExports && 'object' == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports = freeModule && freeModule.exports === freeExports;

	/** Detect free variable `process` from Node.js. */
	var freeProcess = moduleExports && _freeGlobal.process;

	/** Used to access faster Node.js helpers. */
	var nodeUtil = (function() {
	  try {
	    // Use `util.types` for Node.js 10+.
	    var types = freeModule && freeModule.require && freeModule.require('util').types;

	    if (types) {
	      return types;
	    }

	    // Legacy `process.binding('util')` for Node.js < 10.
	    return freeProcess && freeProcess.binding && freeProcess.binding('util');
	  } catch (e) {}
	}());

	module.exports = nodeUtil;
	});

	/* Node.js helper references. */
	var nodeIsTypedArray = _nodeUtil && _nodeUtil.isTypedArray;

	/**
	 * Checks if `value` is classified as a typed array.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
	 * @example
	 *
	 * _.isTypedArray(new Uint8Array);
	 * // => true
	 *
	 * _.isTypedArray([]);
	 * // => false
	 */
	var isTypedArray = nodeIsTypedArray ? _baseUnary(nodeIsTypedArray) : _baseIsTypedArray;

	var isTypedArray_1 = isTypedArray;

	/** Used for built-in method references. */
	var objectProto$3 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

	/**
	 * Creates an array of the enumerable property names of the array-like `value`.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @param {boolean} inherited Specify returning inherited property names.
	 * @returns {Array} Returns the array of property names.
	 */
	function arrayLikeKeys(value, inherited) {
	  var isArr = isArray_1(value),
	      isArg = !isArr && isArguments_1(value),
	      isBuff = !isArr && !isArg && isBuffer_1(value),
	      isType = !isArr && !isArg && !isBuff && isTypedArray_1(value),
	      skipIndexes = isArr || isArg || isBuff || isType,
	      result = skipIndexes ? _baseTimes(value.length, String) : [],
	      length = result.length;

	  for (var key in value) {
	    if ((inherited || hasOwnProperty$2.call(value, key)) &&
	        !(skipIndexes && (
	           // Safari 9 has enumerable `arguments.length` in strict mode.
	           key == 'length' ||
	           // Node.js 0.10 has enumerable non-index properties on buffers.
	           (isBuff && (key == 'offset' || key == 'parent')) ||
	           // PhantomJS 2 has enumerable non-index properties on typed arrays.
	           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
	           // Skip index properties.
	           _isIndex(key, length)
	        ))) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	var _arrayLikeKeys = arrayLikeKeys;

	/** Used for built-in method references. */
	var objectProto$4 = Object.prototype;

	/**
	 * Checks if `value` is likely a prototype object.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
	 */
	function isPrototype(value) {
	  var Ctor = value && value.constructor,
	      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$4;

	  return value === proto;
	}

	var _isPrototype = isPrototype;

	/**
	 * Creates a unary function that invokes `func` with its argument transformed.
	 *
	 * @private
	 * @param {Function} func The function to wrap.
	 * @param {Function} transform The argument transform.
	 * @returns {Function} Returns the new function.
	 */
	function overArg(func, transform) {
	  return function(arg) {
	    return func(transform(arg));
	  };
	}

	var _overArg = overArg;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeKeys = _overArg(Object.keys, Object);

	var _nativeKeys = nativeKeys;

	/** Used for built-in method references. */
	var objectProto$5 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$3 = objectProto$5.hasOwnProperty;

	/**
	 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function baseKeys(object) {
	  if (!_isPrototype(object)) {
	    return _nativeKeys(object);
	  }
	  var result = [];
	  for (var key in Object(object)) {
	    if (hasOwnProperty$3.call(object, key) && key != 'constructor') {
	      result.push(key);
	    }
	  }
	  return result;
	}

	var _baseKeys = baseKeys;

	/**
	 * Checks if `value` is the
	 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
	 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
	 * @example
	 *
	 * _.isObject({});
	 * // => true
	 *
	 * _.isObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isObject(_.noop);
	 * // => true
	 *
	 * _.isObject(null);
	 * // => false
	 */
	function isObject(value) {
	  var type = typeof value;
	  return value != null && (type == 'object' || type == 'function');
	}

	var isObject_1 = isObject;

	/** `Object#toString` result references. */
	var asyncTag = '[object AsyncFunction]',
	    funcTag$1 = '[object Function]',
	    genTag = '[object GeneratorFunction]',
	    proxyTag = '[object Proxy]';

	/**
	 * Checks if `value` is classified as a `Function` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
	 * @example
	 *
	 * _.isFunction(_);
	 * // => true
	 *
	 * _.isFunction(/abc/);
	 * // => false
	 */
	function isFunction(value) {
	  if (!isObject_1(value)) {
	    return false;
	  }
	  // The use of `Object#toString` avoids issues with the `typeof` operator
	  // in Safari 9 which returns 'object' for typed arrays and other constructors.
	  var tag = _baseGetTag(value);
	  return tag == funcTag$1 || tag == genTag || tag == asyncTag || tag == proxyTag;
	}

	var isFunction_1 = isFunction;

	/**
	 * Checks if `value` is array-like. A value is considered array-like if it's
	 * not a function and has a `value.length` that's an integer greater than or
	 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
	 * @example
	 *
	 * _.isArrayLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isArrayLike(document.body.children);
	 * // => true
	 *
	 * _.isArrayLike('abc');
	 * // => true
	 *
	 * _.isArrayLike(_.noop);
	 * // => false
	 */
	function isArrayLike(value) {
	  return value != null && isLength_1(value.length) && !isFunction_1(value);
	}

	var isArrayLike_1 = isArrayLike;

	/**
	 * Creates an array of the own enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects. See the
	 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
	 * for more details.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keys(new Foo);
	 * // => ['a', 'b'] (iteration order is not guaranteed)
	 *
	 * _.keys('hi');
	 * // => ['0', '1']
	 */
	function keys(object) {
	  return isArrayLike_1(object) ? _arrayLikeKeys(object) : _baseKeys(object);
	}

	var keys_1 = keys;

	/**
	 * The base implementation of `_.forOwn` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Object} object The object to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Object} Returns `object`.
	 */
	function baseForOwn(object, iteratee) {
	  return object && _baseFor(object, iteratee, keys_1);
	}

	var _baseForOwn = baseForOwn;

	/**
	 * This method returns the first argument it receives.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Util
	 * @param {*} value Any value.
	 * @returns {*} Returns `value`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 *
	 * console.log(_.identity(object) === object);
	 * // => true
	 */
	function identity(value) {
	  return value;
	}

	var identity_1 = identity;

	/**
	 * Casts `value` to `identity` if it's not a function.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @returns {Function} Returns cast function.
	 */
	function castFunction(value) {
	  return typeof value == 'function' ? value : identity_1;
	}

	var _castFunction = castFunction;

	/**
	 * Iterates over own enumerable string keyed properties of an object and
	 * invokes `iteratee` for each property. The iteratee is invoked with three
	 * arguments: (value, key, object). Iteratee functions may exit iteration
	 * early by explicitly returning `false`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.3.0
	 * @category Object
	 * @param {Object} object The object to iterate over.
	 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
	 * @returns {Object} Returns `object`.
	 * @see _.forOwnRight
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.forOwn(new Foo, function(value, key) {
	 *   console.log(key);
	 * });
	 * // => Logs 'a' then 'b' (iteration order is not guaranteed).
	 */
	function forOwn(object, iteratee) {
	  return object && _baseForOwn(object, _castFunction(iteratee));
	}

	var forOwn_1 = forOwn;

	/** Built-in value references. */
	var getPrototype = _overArg(Object.getPrototypeOf, Object);

	var _getPrototype = getPrototype;

	/** `Object#toString` result references. */
	var objectTag$1 = '[object Object]';

	/** Used for built-in method references. */
	var funcProto = Function.prototype,
	    objectProto$6 = Object.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString = funcProto.toString;

	/** Used to check objects for own properties. */
	var hasOwnProperty$4 = objectProto$6.hasOwnProperty;

	/** Used to infer the `Object` constructor. */
	var objectCtorString = funcToString.call(Object);

	/**
	 * Checks if `value` is a plain object, that is, an object created by the
	 * `Object` constructor or one with a `[[Prototype]]` of `null`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.8.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 * }
	 *
	 * _.isPlainObject(new Foo);
	 * // => false
	 *
	 * _.isPlainObject([1, 2, 3]);
	 * // => false
	 *
	 * _.isPlainObject({ 'x': 0, 'y': 0 });
	 * // => true
	 *
	 * _.isPlainObject(Object.create(null));
	 * // => true
	 */
	function isPlainObject(value) {
	  if (!isObjectLike_1(value) || _baseGetTag(value) != objectTag$1) {
	    return false;
	  }
	  var proto = _getPrototype(value);
	  if (proto === null) {
	    return true;
	  }
	  var Ctor = hasOwnProperty$4.call(proto, 'constructor') && proto.constructor;
	  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
	    funcToString.call(Ctor) == objectCtorString;
	}

	var isPlainObject_1 = isPlainObject;

	/**
	 * A specialized version of `_.map` for arrays without support for iteratee
	 * shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the new mapped array.
	 */
	function arrayMap(array, iteratee) {
	  var index = -1,
	      length = array == null ? 0 : array.length,
	      result = Array(length);

	  while (++index < length) {
	    result[index] = iteratee(array[index], index, array);
	  }
	  return result;
	}

	var _arrayMap = arrayMap;

	/**
	 * Removes all key-value entries from the list cache.
	 *
	 * @private
	 * @name clear
	 * @memberOf ListCache
	 */
	function listCacheClear() {
	  this.__data__ = [];
	  this.size = 0;
	}

	var _listCacheClear = listCacheClear;

	/**
	 * Performs a
	 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * comparison between two values to determine if they are equivalent.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 * var other = { 'a': 1 };
	 *
	 * _.eq(object, object);
	 * // => true
	 *
	 * _.eq(object, other);
	 * // => false
	 *
	 * _.eq('a', 'a');
	 * // => true
	 *
	 * _.eq('a', Object('a'));
	 * // => false
	 *
	 * _.eq(NaN, NaN);
	 * // => true
	 */
	function eq(value, other) {
	  return value === other || (value !== value && other !== other);
	}

	var eq_1 = eq;

	/**
	 * Gets the index at which the `key` is found in `array` of key-value pairs.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} key The key to search for.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function assocIndexOf(array, key) {
	  var length = array.length;
	  while (length--) {
	    if (eq_1(array[length][0], key)) {
	      return length;
	    }
	  }
	  return -1;
	}

	var _assocIndexOf = assocIndexOf;

	/** Used for built-in method references. */
	var arrayProto = Array.prototype;

	/** Built-in value references. */
	var splice = arrayProto.splice;

	/**
	 * Removes `key` and its value from the list cache.
	 *
	 * @private
	 * @name delete
	 * @memberOf ListCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function listCacheDelete(key) {
	  var data = this.__data__,
	      index = _assocIndexOf(data, key);

	  if (index < 0) {
	    return false;
	  }
	  var lastIndex = data.length - 1;
	  if (index == lastIndex) {
	    data.pop();
	  } else {
	    splice.call(data, index, 1);
	  }
	  --this.size;
	  return true;
	}

	var _listCacheDelete = listCacheDelete;

	/**
	 * Gets the list cache value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf ListCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function listCacheGet(key) {
	  var data = this.__data__,
	      index = _assocIndexOf(data, key);

	  return index < 0 ? undefined : data[index][1];
	}

	var _listCacheGet = listCacheGet;

	/**
	 * Checks if a list cache value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf ListCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function listCacheHas(key) {
	  return _assocIndexOf(this.__data__, key) > -1;
	}

	var _listCacheHas = listCacheHas;

	/**
	 * Sets the list cache `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf ListCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the list cache instance.
	 */
	function listCacheSet(key, value) {
	  var data = this.__data__,
	      index = _assocIndexOf(data, key);

	  if (index < 0) {
	    ++this.size;
	    data.push([key, value]);
	  } else {
	    data[index][1] = value;
	  }
	  return this;
	}

	var _listCacheSet = listCacheSet;

	/**
	 * Creates an list cache object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function ListCache(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `ListCache`.
	ListCache.prototype.clear = _listCacheClear;
	ListCache.prototype['delete'] = _listCacheDelete;
	ListCache.prototype.get = _listCacheGet;
	ListCache.prototype.has = _listCacheHas;
	ListCache.prototype.set = _listCacheSet;

	var _ListCache = ListCache;

	/**
	 * Removes all key-value entries from the stack.
	 *
	 * @private
	 * @name clear
	 * @memberOf Stack
	 */
	function stackClear() {
	  this.__data__ = new _ListCache;
	  this.size = 0;
	}

	var _stackClear = stackClear;

	/**
	 * Removes `key` and its value from the stack.
	 *
	 * @private
	 * @name delete
	 * @memberOf Stack
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function stackDelete(key) {
	  var data = this.__data__,
	      result = data['delete'](key);

	  this.size = data.size;
	  return result;
	}

	var _stackDelete = stackDelete;

	/**
	 * Gets the stack value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf Stack
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function stackGet(key) {
	  return this.__data__.get(key);
	}

	var _stackGet = stackGet;

	/**
	 * Checks if a stack value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf Stack
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function stackHas(key) {
	  return this.__data__.has(key);
	}

	var _stackHas = stackHas;

	/** Used to detect overreaching core-js shims. */
	var coreJsData = _root['__core-js_shared__'];

	var _coreJsData = coreJsData;

	/** Used to detect methods masquerading as native. */
	var maskSrcKey = (function() {
	  var uid = /[^.]+$/.exec(_coreJsData && _coreJsData.keys && _coreJsData.keys.IE_PROTO || '');
	  return uid ? ('Symbol(src)_1.' + uid) : '';
	}());

	/**
	 * Checks if `func` has its source masked.
	 *
	 * @private
	 * @param {Function} func The function to check.
	 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
	 */
	function isMasked(func) {
	  return !!maskSrcKey && (maskSrcKey in func);
	}

	var _isMasked = isMasked;

	/** Used for built-in method references. */
	var funcProto$1 = Function.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString$1 = funcProto$1.toString;

	/**
	 * Converts `func` to its source code.
	 *
	 * @private
	 * @param {Function} func The function to convert.
	 * @returns {string} Returns the source code.
	 */
	function toSource(func) {
	  if (func != null) {
	    try {
	      return funcToString$1.call(func);
	    } catch (e) {}
	    try {
	      return (func + '');
	    } catch (e) {}
	  }
	  return '';
	}

	var _toSource = toSource;

	/**
	 * Used to match `RegExp`
	 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
	 */
	var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

	/** Used to detect host constructors (Safari). */
	var reIsHostCtor = /^\[object .+?Constructor\]$/;

	/** Used for built-in method references. */
	var funcProto$2 = Function.prototype,
	    objectProto$7 = Object.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString$2 = funcProto$2.toString;

	/** Used to check objects for own properties. */
	var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

	/** Used to detect if a method is native. */
	var reIsNative = RegExp('^' +
	  funcToString$2.call(hasOwnProperty$5).replace(reRegExpChar, '\\$&')
	  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
	);

	/**
	 * The base implementation of `_.isNative` without bad shim checks.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a native function,
	 *  else `false`.
	 */
	function baseIsNative(value) {
	  if (!isObject_1(value) || _isMasked(value)) {
	    return false;
	  }
	  var pattern = isFunction_1(value) ? reIsNative : reIsHostCtor;
	  return pattern.test(_toSource(value));
	}

	var _baseIsNative = baseIsNative;

	/**
	 * Gets the value at `key` of `object`.
	 *
	 * @private
	 * @param {Object} [object] The object to query.
	 * @param {string} key The key of the property to get.
	 * @returns {*} Returns the property value.
	 */
	function getValue(object, key) {
	  return object == null ? undefined : object[key];
	}

	var _getValue = getValue;

	/**
	 * Gets the native function at `key` of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {string} key The key of the method to get.
	 * @returns {*} Returns the function if it's native, else `undefined`.
	 */
	function getNative(object, key) {
	  var value = _getValue(object, key);
	  return _baseIsNative(value) ? value : undefined;
	}

	var _getNative = getNative;

	/* Built-in method references that are verified to be native. */
	var Map = _getNative(_root, 'Map');

	var _Map = Map;

	/* Built-in method references that are verified to be native. */
	var nativeCreate = _getNative(Object, 'create');

	var _nativeCreate = nativeCreate;

	/**
	 * Removes all key-value entries from the hash.
	 *
	 * @private
	 * @name clear
	 * @memberOf Hash
	 */
	function hashClear() {
	  this.__data__ = _nativeCreate ? _nativeCreate(null) : {};
	  this.size = 0;
	}

	var _hashClear = hashClear;

	/**
	 * Removes `key` and its value from the hash.
	 *
	 * @private
	 * @name delete
	 * @memberOf Hash
	 * @param {Object} hash The hash to modify.
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function hashDelete(key) {
	  var result = this.has(key) && delete this.__data__[key];
	  this.size -= result ? 1 : 0;
	  return result;
	}

	var _hashDelete = hashDelete;

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED = '__lodash_hash_undefined__';

	/** Used for built-in method references. */
	var objectProto$8 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$6 = objectProto$8.hasOwnProperty;

	/**
	 * Gets the hash value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf Hash
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function hashGet(key) {
	  var data = this.__data__;
	  if (_nativeCreate) {
	    var result = data[key];
	    return result === HASH_UNDEFINED ? undefined : result;
	  }
	  return hasOwnProperty$6.call(data, key) ? data[key] : undefined;
	}

	var _hashGet = hashGet;

	/** Used for built-in method references. */
	var objectProto$9 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$7 = objectProto$9.hasOwnProperty;

	/**
	 * Checks if a hash value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf Hash
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function hashHas(key) {
	  var data = this.__data__;
	  return _nativeCreate ? (data[key] !== undefined) : hasOwnProperty$7.call(data, key);
	}

	var _hashHas = hashHas;

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

	/**
	 * Sets the hash `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf Hash
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the hash instance.
	 */
	function hashSet(key, value) {
	  var data = this.__data__;
	  this.size += this.has(key) ? 0 : 1;
	  data[key] = (_nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
	  return this;
	}

	var _hashSet = hashSet;

	/**
	 * Creates a hash object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function Hash(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `Hash`.
	Hash.prototype.clear = _hashClear;
	Hash.prototype['delete'] = _hashDelete;
	Hash.prototype.get = _hashGet;
	Hash.prototype.has = _hashHas;
	Hash.prototype.set = _hashSet;

	var _Hash = Hash;

	/**
	 * Removes all key-value entries from the map.
	 *
	 * @private
	 * @name clear
	 * @memberOf MapCache
	 */
	function mapCacheClear() {
	  this.size = 0;
	  this.__data__ = {
	    'hash': new _Hash,
	    'map': new (_Map || _ListCache),
	    'string': new _Hash
	  };
	}

	var _mapCacheClear = mapCacheClear;

	/**
	 * Checks if `value` is suitable for use as unique object key.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
	 */
	function isKeyable(value) {
	  var type = typeof value;
	  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
	    ? (value !== '__proto__')
	    : (value === null);
	}

	var _isKeyable = isKeyable;

	/**
	 * Gets the data for `map`.
	 *
	 * @private
	 * @param {Object} map The map to query.
	 * @param {string} key The reference key.
	 * @returns {*} Returns the map data.
	 */
	function getMapData(map, key) {
	  var data = map.__data__;
	  return _isKeyable(key)
	    ? data[typeof key == 'string' ? 'string' : 'hash']
	    : data.map;
	}

	var _getMapData = getMapData;

	/**
	 * Removes `key` and its value from the map.
	 *
	 * @private
	 * @name delete
	 * @memberOf MapCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function mapCacheDelete(key) {
	  var result = _getMapData(this, key)['delete'](key);
	  this.size -= result ? 1 : 0;
	  return result;
	}

	var _mapCacheDelete = mapCacheDelete;

	/**
	 * Gets the map value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf MapCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function mapCacheGet(key) {
	  return _getMapData(this, key).get(key);
	}

	var _mapCacheGet = mapCacheGet;

	/**
	 * Checks if a map value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf MapCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function mapCacheHas(key) {
	  return _getMapData(this, key).has(key);
	}

	var _mapCacheHas = mapCacheHas;

	/**
	 * Sets the map `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf MapCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the map cache instance.
	 */
	function mapCacheSet(key, value) {
	  var data = _getMapData(this, key),
	      size = data.size;

	  data.set(key, value);
	  this.size += data.size == size ? 0 : 1;
	  return this;
	}

	var _mapCacheSet = mapCacheSet;

	/**
	 * Creates a map cache object to store key-value pairs.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function MapCache(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `MapCache`.
	MapCache.prototype.clear = _mapCacheClear;
	MapCache.prototype['delete'] = _mapCacheDelete;
	MapCache.prototype.get = _mapCacheGet;
	MapCache.prototype.has = _mapCacheHas;
	MapCache.prototype.set = _mapCacheSet;

	var _MapCache = MapCache;

	/** Used as the size to enable large array optimizations. */
	var LARGE_ARRAY_SIZE = 200;

	/**
	 * Sets the stack `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf Stack
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the stack cache instance.
	 */
	function stackSet(key, value) {
	  var data = this.__data__;
	  if (data instanceof _ListCache) {
	    var pairs = data.__data__;
	    if (!_Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
	      pairs.push([key, value]);
	      this.size = ++data.size;
	      return this;
	    }
	    data = this.__data__ = new _MapCache(pairs);
	  }
	  data.set(key, value);
	  this.size = data.size;
	  return this;
	}

	var _stackSet = stackSet;

	/**
	 * Creates a stack cache object to store key-value pairs.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function Stack(entries) {
	  var data = this.__data__ = new _ListCache(entries);
	  this.size = data.size;
	}

	// Add methods to `Stack`.
	Stack.prototype.clear = _stackClear;
	Stack.prototype['delete'] = _stackDelete;
	Stack.prototype.get = _stackGet;
	Stack.prototype.has = _stackHas;
	Stack.prototype.set = _stackSet;

	var _Stack = Stack;

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

	/**
	 * Adds `value` to the array cache.
	 *
	 * @private
	 * @name add
	 * @memberOf SetCache
	 * @alias push
	 * @param {*} value The value to cache.
	 * @returns {Object} Returns the cache instance.
	 */
	function setCacheAdd(value) {
	  this.__data__.set(value, HASH_UNDEFINED$2);
	  return this;
	}

	var _setCacheAdd = setCacheAdd;

	/**
	 * Checks if `value` is in the array cache.
	 *
	 * @private
	 * @name has
	 * @memberOf SetCache
	 * @param {*} value The value to search for.
	 * @returns {number} Returns `true` if `value` is found, else `false`.
	 */
	function setCacheHas(value) {
	  return this.__data__.has(value);
	}

	var _setCacheHas = setCacheHas;

	/**
	 *
	 * Creates an array cache object to store unique values.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [values] The values to cache.
	 */
	function SetCache(values) {
	  var index = -1,
	      length = values == null ? 0 : values.length;

	  this.__data__ = new _MapCache;
	  while (++index < length) {
	    this.add(values[index]);
	  }
	}

	// Add methods to `SetCache`.
	SetCache.prototype.add = SetCache.prototype.push = _setCacheAdd;
	SetCache.prototype.has = _setCacheHas;

	var _SetCache = SetCache;

	/**
	 * A specialized version of `_.some` for arrays without support for iteratee
	 * shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {boolean} Returns `true` if any element passes the predicate check,
	 *  else `false`.
	 */
	function arraySome(array, predicate) {
	  var index = -1,
	      length = array == null ? 0 : array.length;

	  while (++index < length) {
	    if (predicate(array[index], index, array)) {
	      return true;
	    }
	  }
	  return false;
	}

	var _arraySome = arraySome;

	/**
	 * Checks if a `cache` value for `key` exists.
	 *
	 * @private
	 * @param {Object} cache The cache to query.
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function cacheHas(cache, key) {
	  return cache.has(key);
	}

	var _cacheHas = cacheHas;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG = 1,
	    COMPARE_UNORDERED_FLAG = 2;

	/**
	 * A specialized version of `baseIsEqualDeep` for arrays with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Array} array The array to compare.
	 * @param {Array} other The other array to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `array` and `other` objects.
	 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
	 */
	function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
	  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
	      arrLength = array.length,
	      othLength = other.length;

	  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
	    return false;
	  }
	  // Assume cyclic values are equal.
	  var stacked = stack.get(array);
	  if (stacked && stack.get(other)) {
	    return stacked == other;
	  }
	  var index = -1,
	      result = true,
	      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new _SetCache : undefined;

	  stack.set(array, other);
	  stack.set(other, array);

	  // Ignore non-index properties.
	  while (++index < arrLength) {
	    var arrValue = array[index],
	        othValue = other[index];

	    if (customizer) {
	      var compared = isPartial
	        ? customizer(othValue, arrValue, index, other, array, stack)
	        : customizer(arrValue, othValue, index, array, other, stack);
	    }
	    if (compared !== undefined) {
	      if (compared) {
	        continue;
	      }
	      result = false;
	      break;
	    }
	    // Recursively compare arrays (susceptible to call stack limits).
	    if (seen) {
	      if (!_arraySome(other, function(othValue, othIndex) {
	            if (!_cacheHas(seen, othIndex) &&
	                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
	              return seen.push(othIndex);
	            }
	          })) {
	        result = false;
	        break;
	      }
	    } else if (!(
	          arrValue === othValue ||
	            equalFunc(arrValue, othValue, bitmask, customizer, stack)
	        )) {
	      result = false;
	      break;
	    }
	  }
	  stack['delete'](array);
	  stack['delete'](other);
	  return result;
	}

	var _equalArrays = equalArrays;

	/** Built-in value references. */
	var Uint8Array = _root.Uint8Array;

	var _Uint8Array = Uint8Array;

	/**
	 * Converts `map` to its key-value pairs.
	 *
	 * @private
	 * @param {Object} map The map to convert.
	 * @returns {Array} Returns the key-value pairs.
	 */
	function mapToArray(map) {
	  var index = -1,
	      result = Array(map.size);

	  map.forEach(function(value, key) {
	    result[++index] = [key, value];
	  });
	  return result;
	}

	var _mapToArray = mapToArray;

	/**
	 * Converts `set` to an array of its values.
	 *
	 * @private
	 * @param {Object} set The set to convert.
	 * @returns {Array} Returns the values.
	 */
	function setToArray(set) {
	  var index = -1,
	      result = Array(set.size);

	  set.forEach(function(value) {
	    result[++index] = value;
	  });
	  return result;
	}

	var _setToArray = setToArray;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$1 = 1,
	    COMPARE_UNORDERED_FLAG$1 = 2;

	/** `Object#toString` result references. */
	var boolTag$1 = '[object Boolean]',
	    dateTag$1 = '[object Date]',
	    errorTag$1 = '[object Error]',
	    mapTag$1 = '[object Map]',
	    numberTag$1 = '[object Number]',
	    regexpTag$1 = '[object RegExp]',
	    setTag$1 = '[object Set]',
	    stringTag$2 = '[object String]',
	    symbolTag = '[object Symbol]';

	var arrayBufferTag$1 = '[object ArrayBuffer]',
	    dataViewTag$1 = '[object DataView]';

	/** Used to convert symbols to primitives and strings. */
	var symbolProto = _Symbol ? _Symbol.prototype : undefined,
	    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

	/**
	 * A specialized version of `baseIsEqualDeep` for comparing objects of
	 * the same `toStringTag`.
	 *
	 * **Note:** This function only supports comparing values with tags of
	 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {string} tag The `toStringTag` of the objects to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
	  switch (tag) {
	    case dataViewTag$1:
	      if ((object.byteLength != other.byteLength) ||
	          (object.byteOffset != other.byteOffset)) {
	        return false;
	      }
	      object = object.buffer;
	      other = other.buffer;

	    case arrayBufferTag$1:
	      if ((object.byteLength != other.byteLength) ||
	          !equalFunc(new _Uint8Array(object), new _Uint8Array(other))) {
	        return false;
	      }
	      return true;

	    case boolTag$1:
	    case dateTag$1:
	    case numberTag$1:
	      // Coerce booleans to `1` or `0` and dates to milliseconds.
	      // Invalid dates are coerced to `NaN`.
	      return eq_1(+object, +other);

	    case errorTag$1:
	      return object.name == other.name && object.message == other.message;

	    case regexpTag$1:
	    case stringTag$2:
	      // Coerce regexes to strings and treat strings, primitives and objects,
	      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
	      // for more details.
	      return object == (other + '');

	    case mapTag$1:
	      var convert = _mapToArray;

	    case setTag$1:
	      var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
	      convert || (convert = _setToArray);

	      if (object.size != other.size && !isPartial) {
	        return false;
	      }
	      // Assume cyclic values are equal.
	      var stacked = stack.get(object);
	      if (stacked) {
	        return stacked == other;
	      }
	      bitmask |= COMPARE_UNORDERED_FLAG$1;

	      // Recursively compare objects (susceptible to call stack limits).
	      stack.set(object, other);
	      var result = _equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
	      stack['delete'](object);
	      return result;

	    case symbolTag:
	      if (symbolValueOf) {
	        return symbolValueOf.call(object) == symbolValueOf.call(other);
	      }
	  }
	  return false;
	}

	var _equalByTag = equalByTag;

	/**
	 * Appends the elements of `values` to `array`.
	 *
	 * @private
	 * @param {Array} array The array to modify.
	 * @param {Array} values The values to append.
	 * @returns {Array} Returns `array`.
	 */
	function arrayPush(array, values) {
	  var index = -1,
	      length = values.length,
	      offset = array.length;

	  while (++index < length) {
	    array[offset + index] = values[index];
	  }
	  return array;
	}

	var _arrayPush = arrayPush;

	/**
	 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
	 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
	 * symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Function} keysFunc The function to get the keys of `object`.
	 * @param {Function} symbolsFunc The function to get the symbols of `object`.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function baseGetAllKeys(object, keysFunc, symbolsFunc) {
	  var result = keysFunc(object);
	  return isArray_1(object) ? result : _arrayPush(result, symbolsFunc(object));
	}

	var _baseGetAllKeys = baseGetAllKeys;

	/**
	 * A specialized version of `_.filter` for arrays without support for
	 * iteratee shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {Array} Returns the new filtered array.
	 */
	function arrayFilter(array, predicate) {
	  var index = -1,
	      length = array == null ? 0 : array.length,
	      resIndex = 0,
	      result = [];

	  while (++index < length) {
	    var value = array[index];
	    if (predicate(value, index, array)) {
	      result[resIndex++] = value;
	    }
	  }
	  return result;
	}

	var _arrayFilter = arrayFilter;

	/**
	 * This method returns a new empty array.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.13.0
	 * @category Util
	 * @returns {Array} Returns the new empty array.
	 * @example
	 *
	 * var arrays = _.times(2, _.stubArray);
	 *
	 * console.log(arrays);
	 * // => [[], []]
	 *
	 * console.log(arrays[0] === arrays[1]);
	 * // => false
	 */
	function stubArray() {
	  return [];
	}

	var stubArray_1 = stubArray;

	/** Used for built-in method references. */
	var objectProto$10 = Object.prototype;

	/** Built-in value references. */
	var propertyIsEnumerable$1 = objectProto$10.propertyIsEnumerable;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeGetSymbols = Object.getOwnPropertySymbols;

	/**
	 * Creates an array of the own enumerable symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of symbols.
	 */
	var getSymbols = !nativeGetSymbols ? stubArray_1 : function(object) {
	  if (object == null) {
	    return [];
	  }
	  object = Object(object);
	  return _arrayFilter(nativeGetSymbols(object), function(symbol) {
	    return propertyIsEnumerable$1.call(object, symbol);
	  });
	};

	var _getSymbols = getSymbols;

	/**
	 * Creates an array of own enumerable property names and symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function getAllKeys(object) {
	  return _baseGetAllKeys(object, keys_1, _getSymbols);
	}

	var _getAllKeys = getAllKeys;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$2 = 1;

	/** Used for built-in method references. */
	var objectProto$11 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$8 = objectProto$11.hasOwnProperty;

	/**
	 * A specialized version of `baseIsEqualDeep` for objects with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
	  var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
	      objProps = _getAllKeys(object),
	      objLength = objProps.length,
	      othProps = _getAllKeys(other),
	      othLength = othProps.length;

	  if (objLength != othLength && !isPartial) {
	    return false;
	  }
	  var index = objLength;
	  while (index--) {
	    var key = objProps[index];
	    if (!(isPartial ? key in other : hasOwnProperty$8.call(other, key))) {
	      return false;
	    }
	  }
	  // Assume cyclic values are equal.
	  var stacked = stack.get(object);
	  if (stacked && stack.get(other)) {
	    return stacked == other;
	  }
	  var result = true;
	  stack.set(object, other);
	  stack.set(other, object);

	  var skipCtor = isPartial;
	  while (++index < objLength) {
	    key = objProps[index];
	    var objValue = object[key],
	        othValue = other[key];

	    if (customizer) {
	      var compared = isPartial
	        ? customizer(othValue, objValue, key, other, object, stack)
	        : customizer(objValue, othValue, key, object, other, stack);
	    }
	    // Recursively compare objects (susceptible to call stack limits).
	    if (!(compared === undefined
	          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
	          : compared
	        )) {
	      result = false;
	      break;
	    }
	    skipCtor || (skipCtor = key == 'constructor');
	  }
	  if (result && !skipCtor) {
	    var objCtor = object.constructor,
	        othCtor = other.constructor;

	    // Non `Object` object instances with different constructors are not equal.
	    if (objCtor != othCtor &&
	        ('constructor' in object && 'constructor' in other) &&
	        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
	          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
	      result = false;
	    }
	  }
	  stack['delete'](object);
	  stack['delete'](other);
	  return result;
	}

	var _equalObjects = equalObjects;

	/* Built-in method references that are verified to be native. */
	var DataView = _getNative(_root, 'DataView');

	var _DataView = DataView;

	/* Built-in method references that are verified to be native. */
	var Promise$1 = _getNative(_root, 'Promise');

	var _Promise = Promise$1;

	/* Built-in method references that are verified to be native. */
	var Set = _getNative(_root, 'Set');

	var _Set = Set;

	/* Built-in method references that are verified to be native. */
	var WeakMap = _getNative(_root, 'WeakMap');

	var _WeakMap = WeakMap;

	/** `Object#toString` result references. */
	var mapTag$2 = '[object Map]',
	    objectTag$2 = '[object Object]',
	    promiseTag = '[object Promise]',
	    setTag$2 = '[object Set]',
	    weakMapTag$1 = '[object WeakMap]';

	var dataViewTag$2 = '[object DataView]';

	/** Used to detect maps, sets, and weakmaps. */
	var dataViewCtorString = _toSource(_DataView),
	    mapCtorString = _toSource(_Map),
	    promiseCtorString = _toSource(_Promise),
	    setCtorString = _toSource(_Set),
	    weakMapCtorString = _toSource(_WeakMap);

	/**
	 * Gets the `toStringTag` of `value`.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the `toStringTag`.
	 */
	var getTag = _baseGetTag;

	// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
	if ((_DataView && getTag(new _DataView(new ArrayBuffer(1))) != dataViewTag$2) ||
	    (_Map && getTag(new _Map) != mapTag$2) ||
	    (_Promise && getTag(_Promise.resolve()) != promiseTag) ||
	    (_Set && getTag(new _Set) != setTag$2) ||
	    (_WeakMap && getTag(new _WeakMap) != weakMapTag$1)) {
	  getTag = function(value) {
	    var result = _baseGetTag(value),
	        Ctor = result == objectTag$2 ? value.constructor : undefined,
	        ctorString = Ctor ? _toSource(Ctor) : '';

	    if (ctorString) {
	      switch (ctorString) {
	        case dataViewCtorString: return dataViewTag$2;
	        case mapCtorString: return mapTag$2;
	        case promiseCtorString: return promiseTag;
	        case setCtorString: return setTag$2;
	        case weakMapCtorString: return weakMapTag$1;
	      }
	    }
	    return result;
	  };
	}

	var _getTag = getTag;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$3 = 1;

	/** `Object#toString` result references. */
	var argsTag$2 = '[object Arguments]',
	    arrayTag$1 = '[object Array]',
	    objectTag$3 = '[object Object]';

	/** Used for built-in method references. */
	var objectProto$12 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$9 = objectProto$12.hasOwnProperty;

	/**
	 * A specialized version of `baseIsEqual` for arrays and objects which performs
	 * deep comparisons and tracks traversed objects enabling objects with circular
	 * references to be compared.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
	  var objIsArr = isArray_1(object),
	      othIsArr = isArray_1(other),
	      objTag = objIsArr ? arrayTag$1 : _getTag(object),
	      othTag = othIsArr ? arrayTag$1 : _getTag(other);

	  objTag = objTag == argsTag$2 ? objectTag$3 : objTag;
	  othTag = othTag == argsTag$2 ? objectTag$3 : othTag;

	  var objIsObj = objTag == objectTag$3,
	      othIsObj = othTag == objectTag$3,
	      isSameTag = objTag == othTag;

	  if (isSameTag && isBuffer_1(object)) {
	    if (!isBuffer_1(other)) {
	      return false;
	    }
	    objIsArr = true;
	    objIsObj = false;
	  }
	  if (isSameTag && !objIsObj) {
	    stack || (stack = new _Stack);
	    return (objIsArr || isTypedArray_1(object))
	      ? _equalArrays(object, other, bitmask, customizer, equalFunc, stack)
	      : _equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
	  }
	  if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
	    var objIsWrapped = objIsObj && hasOwnProperty$9.call(object, '__wrapped__'),
	        othIsWrapped = othIsObj && hasOwnProperty$9.call(other, '__wrapped__');

	    if (objIsWrapped || othIsWrapped) {
	      var objUnwrapped = objIsWrapped ? object.value() : object,
	          othUnwrapped = othIsWrapped ? other.value() : other;

	      stack || (stack = new _Stack);
	      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
	    }
	  }
	  if (!isSameTag) {
	    return false;
	  }
	  stack || (stack = new _Stack);
	  return _equalObjects(object, other, bitmask, customizer, equalFunc, stack);
	}

	var _baseIsEqualDeep = baseIsEqualDeep;

	/**
	 * The base implementation of `_.isEqual` which supports partial comparisons
	 * and tracks traversed objects.
	 *
	 * @private
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @param {boolean} bitmask The bitmask flags.
	 *  1 - Unordered comparison
	 *  2 - Partial comparison
	 * @param {Function} [customizer] The function to customize comparisons.
	 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 */
	function baseIsEqual(value, other, bitmask, customizer, stack) {
	  if (value === other) {
	    return true;
	  }
	  if (value == null || other == null || (!isObjectLike_1(value) && !isObjectLike_1(other))) {
	    return value !== value && other !== other;
	  }
	  return _baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
	}

	var _baseIsEqual = baseIsEqual;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$4 = 1,
	    COMPARE_UNORDERED_FLAG$2 = 2;

	/**
	 * The base implementation of `_.isMatch` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Object} object The object to inspect.
	 * @param {Object} source The object of property values to match.
	 * @param {Array} matchData The property names, values, and compare flags to match.
	 * @param {Function} [customizer] The function to customize comparisons.
	 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
	 */
	function baseIsMatch(object, source, matchData, customizer) {
	  var index = matchData.length,
	      length = index,
	      noCustomizer = !customizer;

	  if (object == null) {
	    return !length;
	  }
	  object = Object(object);
	  while (index--) {
	    var data = matchData[index];
	    if ((noCustomizer && data[2])
	          ? data[1] !== object[data[0]]
	          : !(data[0] in object)
	        ) {
	      return false;
	    }
	  }
	  while (++index < length) {
	    data = matchData[index];
	    var key = data[0],
	        objValue = object[key],
	        srcValue = data[1];

	    if (noCustomizer && data[2]) {
	      if (objValue === undefined && !(key in object)) {
	        return false;
	      }
	    } else {
	      var stack = new _Stack;
	      if (customizer) {
	        var result = customizer(objValue, srcValue, key, object, source, stack);
	      }
	      if (!(result === undefined
	            ? _baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
	            : result
	          )) {
	        return false;
	      }
	    }
	  }
	  return true;
	}

	var _baseIsMatch = baseIsMatch;

	/**
	 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` if suitable for strict
	 *  equality comparisons, else `false`.
	 */
	function isStrictComparable(value) {
	  return value === value && !isObject_1(value);
	}

	var _isStrictComparable = isStrictComparable;

	/**
	 * Gets the property names, values, and compare flags of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the match data of `object`.
	 */
	function getMatchData(object) {
	  var result = keys_1(object),
	      length = result.length;

	  while (length--) {
	    var key = result[length],
	        value = object[key];

	    result[length] = [key, value, _isStrictComparable(value)];
	  }
	  return result;
	}

	var _getMatchData = getMatchData;

	/**
	 * A specialized version of `matchesProperty` for source values suitable
	 * for strict equality comparisons, i.e. `===`.
	 *
	 * @private
	 * @param {string} key The key of the property to get.
	 * @param {*} srcValue The value to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function matchesStrictComparable(key, srcValue) {
	  return function(object) {
	    if (object == null) {
	      return false;
	    }
	    return object[key] === srcValue &&
	      (srcValue !== undefined || (key in Object(object)));
	  };
	}

	var _matchesStrictComparable = matchesStrictComparable;

	/**
	 * The base implementation of `_.matches` which doesn't clone `source`.
	 *
	 * @private
	 * @param {Object} source The object of property values to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function baseMatches(source) {
	  var matchData = _getMatchData(source);
	  if (matchData.length == 1 && matchData[0][2]) {
	    return _matchesStrictComparable(matchData[0][0], matchData[0][1]);
	  }
	  return function(object) {
	    return object === source || _baseIsMatch(object, source, matchData);
	  };
	}

	var _baseMatches = baseMatches;

	/** `Object#toString` result references. */
	var symbolTag$1 = '[object Symbol]';

	/**
	 * Checks if `value` is classified as a `Symbol` primitive or object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
	 * @example
	 *
	 * _.isSymbol(Symbol.iterator);
	 * // => true
	 *
	 * _.isSymbol('abc');
	 * // => false
	 */
	function isSymbol(value) {
	  return typeof value == 'symbol' ||
	    (isObjectLike_1(value) && _baseGetTag(value) == symbolTag$1);
	}

	var isSymbol_1 = isSymbol;

	/** Used to match property names within property paths. */
	var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
	    reIsPlainProp = /^\w*$/;

	/**
	 * Checks if `value` is a property name and not a property path.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {Object} [object] The object to query keys on.
	 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
	 */
	function isKey(value, object) {
	  if (isArray_1(value)) {
	    return false;
	  }
	  var type = typeof value;
	  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
	      value == null || isSymbol_1(value)) {
	    return true;
	  }
	  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
	    (object != null && value in Object(object));
	}

	var _isKey = isKey;

	/** Error message constants. */
	var FUNC_ERROR_TEXT = 'Expected a function';

	/**
	 * Creates a function that memoizes the result of `func`. If `resolver` is
	 * provided, it determines the cache key for storing the result based on the
	 * arguments provided to the memoized function. By default, the first argument
	 * provided to the memoized function is used as the map cache key. The `func`
	 * is invoked with the `this` binding of the memoized function.
	 *
	 * **Note:** The cache is exposed as the `cache` property on the memoized
	 * function. Its creation may be customized by replacing the `_.memoize.Cache`
	 * constructor with one whose instances implement the
	 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
	 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to have its output memoized.
	 * @param {Function} [resolver] The function to resolve the cache key.
	 * @returns {Function} Returns the new memoized function.
	 * @example
	 *
	 * var object = { 'a': 1, 'b': 2 };
	 * var other = { 'c': 3, 'd': 4 };
	 *
	 * var values = _.memoize(_.values);
	 * values(object);
	 * // => [1, 2]
	 *
	 * values(other);
	 * // => [3, 4]
	 *
	 * object.a = 2;
	 * values(object);
	 * // => [1, 2]
	 *
	 * // Modify the result cache.
	 * values.cache.set(object, ['a', 'b']);
	 * values(object);
	 * // => ['a', 'b']
	 *
	 * // Replace `_.memoize.Cache`.
	 * _.memoize.Cache = WeakMap;
	 */
	function memoize(func, resolver) {
	  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
	    throw new TypeError(FUNC_ERROR_TEXT);
	  }
	  var memoized = function() {
	    var args = arguments,
	        key = resolver ? resolver.apply(this, args) : args[0],
	        cache = memoized.cache;

	    if (cache.has(key)) {
	      return cache.get(key);
	    }
	    var result = func.apply(this, args);
	    memoized.cache = cache.set(key, result) || cache;
	    return result;
	  };
	  memoized.cache = new (memoize.Cache || _MapCache);
	  return memoized;
	}

	// Expose `MapCache`.
	memoize.Cache = _MapCache;

	var memoize_1 = memoize;

	/** Used as the maximum memoize cache size. */
	var MAX_MEMOIZE_SIZE = 500;

	/**
	 * A specialized version of `_.memoize` which clears the memoized function's
	 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
	 *
	 * @private
	 * @param {Function} func The function to have its output memoized.
	 * @returns {Function} Returns the new memoized function.
	 */
	function memoizeCapped(func) {
	  var result = memoize_1(func, function(key) {
	    if (cache.size === MAX_MEMOIZE_SIZE) {
	      cache.clear();
	    }
	    return key;
	  });

	  var cache = result.cache;
	  return result;
	}

	var _memoizeCapped = memoizeCapped;

	/** Used to match property names within property paths. */
	var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

	/** Used to match backslashes in property paths. */
	var reEscapeChar = /\\(\\)?/g;

	/**
	 * Converts `string` to a property path array.
	 *
	 * @private
	 * @param {string} string The string to convert.
	 * @returns {Array} Returns the property path array.
	 */
	var stringToPath = _memoizeCapped(function(string) {
	  var result = [];
	  if (string.charCodeAt(0) === 46 /* . */) {
	    result.push('');
	  }
	  string.replace(rePropName, function(match, number, quote, subString) {
	    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
	  });
	  return result;
	});

	var _stringToPath = stringToPath;

	/** Used as references for various `Number` constants. */
	var INFINITY = 1 / 0;

	/** Used to convert symbols to primitives and strings. */
	var symbolProto$1 = _Symbol ? _Symbol.prototype : undefined,
	    symbolToString = symbolProto$1 ? symbolProto$1.toString : undefined;

	/**
	 * The base implementation of `_.toString` which doesn't convert nullish
	 * values to empty strings.
	 *
	 * @private
	 * @param {*} value The value to process.
	 * @returns {string} Returns the string.
	 */
	function baseToString(value) {
	  // Exit early for strings to avoid a performance hit in some environments.
	  if (typeof value == 'string') {
	    return value;
	  }
	  if (isArray_1(value)) {
	    // Recursively convert values (susceptible to call stack limits).
	    return _arrayMap(value, baseToString) + '';
	  }
	  if (isSymbol_1(value)) {
	    return symbolToString ? symbolToString.call(value) : '';
	  }
	  var result = (value + '');
	  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
	}

	var _baseToString = baseToString;

	/**
	 * Converts `value` to a string. An empty string is returned for `null`
	 * and `undefined` values. The sign of `-0` is preserved.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to convert.
	 * @returns {string} Returns the converted string.
	 * @example
	 *
	 * _.toString(null);
	 * // => ''
	 *
	 * _.toString(-0);
	 * // => '-0'
	 *
	 * _.toString([1, 2, 3]);
	 * // => '1,2,3'
	 */
	function toString(value) {
	  return value == null ? '' : _baseToString(value);
	}

	var toString_1 = toString;

	/**
	 * Casts `value` to a path array if it's not one.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @param {Object} [object] The object to query keys on.
	 * @returns {Array} Returns the cast property path array.
	 */
	function castPath(value, object) {
	  if (isArray_1(value)) {
	    return value;
	  }
	  return _isKey(value, object) ? [value] : _stringToPath(toString_1(value));
	}

	var _castPath = castPath;

	/** Used as references for various `Number` constants. */
	var INFINITY$1 = 1 / 0;

	/**
	 * Converts `value` to a string key if it's not a string or symbol.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @returns {string|symbol} Returns the key.
	 */
	function toKey(value) {
	  if (typeof value == 'string' || isSymbol_1(value)) {
	    return value;
	  }
	  var result = (value + '');
	  return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
	}

	var _toKey = toKey;

	/**
	 * The base implementation of `_.get` without support for default values.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path of the property to get.
	 * @returns {*} Returns the resolved value.
	 */
	function baseGet(object, path) {
	  path = _castPath(path, object);

	  var index = 0,
	      length = path.length;

	  while (object != null && index < length) {
	    object = object[_toKey(path[index++])];
	  }
	  return (index && index == length) ? object : undefined;
	}

	var _baseGet = baseGet;

	/**
	 * Gets the value at `path` of `object`. If the resolved value is
	 * `undefined`, the `defaultValue` is returned in its place.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.7.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path of the property to get.
	 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
	 * @returns {*} Returns the resolved value.
	 * @example
	 *
	 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
	 *
	 * _.get(object, 'a[0].b.c');
	 * // => 3
	 *
	 * _.get(object, ['a', '0', 'b', 'c']);
	 * // => 3
	 *
	 * _.get(object, 'a.b.c', 'default');
	 * // => 'default'
	 */
	function get(object, path, defaultValue) {
	  var result = object == null ? undefined : _baseGet(object, path);
	  return result === undefined ? defaultValue : result;
	}

	var get_1 = get;

	/**
	 * The base implementation of `_.hasIn` without support for deep paths.
	 *
	 * @private
	 * @param {Object} [object] The object to query.
	 * @param {Array|string} key The key to check.
	 * @returns {boolean} Returns `true` if `key` exists, else `false`.
	 */
	function baseHasIn(object, key) {
	  return object != null && key in Object(object);
	}

	var _baseHasIn = baseHasIn;

	/**
	 * Checks if `path` exists on `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path to check.
	 * @param {Function} hasFunc The function to check properties.
	 * @returns {boolean} Returns `true` if `path` exists, else `false`.
	 */
	function hasPath(object, path, hasFunc) {
	  path = _castPath(path, object);

	  var index = -1,
	      length = path.length,
	      result = false;

	  while (++index < length) {
	    var key = _toKey(path[index]);
	    if (!(result = object != null && hasFunc(object, key))) {
	      break;
	    }
	    object = object[key];
	  }
	  if (result || ++index != length) {
	    return result;
	  }
	  length = object == null ? 0 : object.length;
	  return !!length && isLength_1(length) && _isIndex(key, length) &&
	    (isArray_1(object) || isArguments_1(object));
	}

	var _hasPath = hasPath;

	/**
	 * Checks if `path` is a direct or inherited property of `object`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path to check.
	 * @returns {boolean} Returns `true` if `path` exists, else `false`.
	 * @example
	 *
	 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
	 *
	 * _.hasIn(object, 'a');
	 * // => true
	 *
	 * _.hasIn(object, 'a.b');
	 * // => true
	 *
	 * _.hasIn(object, ['a', 'b']);
	 * // => true
	 *
	 * _.hasIn(object, 'b');
	 * // => false
	 */
	function hasIn(object, path) {
	  return object != null && _hasPath(object, path, _baseHasIn);
	}

	var hasIn_1 = hasIn;

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$5 = 1,
	    COMPARE_UNORDERED_FLAG$3 = 2;

	/**
	 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
	 *
	 * @private
	 * @param {string} path The path of the property to get.
	 * @param {*} srcValue The value to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function baseMatchesProperty(path, srcValue) {
	  if (_isKey(path) && _isStrictComparable(srcValue)) {
	    return _matchesStrictComparable(_toKey(path), srcValue);
	  }
	  return function(object) {
	    var objValue = get_1(object, path);
	    return (objValue === undefined && objValue === srcValue)
	      ? hasIn_1(object, path)
	      : _baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
	  };
	}

	var _baseMatchesProperty = baseMatchesProperty;

	/**
	 * The base implementation of `_.property` without support for deep paths.
	 *
	 * @private
	 * @param {string} key The key of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 */
	function baseProperty(key) {
	  return function(object) {
	    return object == null ? undefined : object[key];
	  };
	}

	var _baseProperty = baseProperty;

	/**
	 * A specialized version of `baseProperty` which supports deep paths.
	 *
	 * @private
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 */
	function basePropertyDeep(path) {
	  return function(object) {
	    return _baseGet(object, path);
	  };
	}

	var _basePropertyDeep = basePropertyDeep;

	/**
	 * Creates a function that returns the value at `path` of a given object.
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Util
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 * @example
	 *
	 * var objects = [
	 *   { 'a': { 'b': 2 } },
	 *   { 'a': { 'b': 1 } }
	 * ];
	 *
	 * _.map(objects, _.property('a.b'));
	 * // => [2, 1]
	 *
	 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
	 * // => [1, 2]
	 */
	function property(path) {
	  return _isKey(path) ? _baseProperty(_toKey(path)) : _basePropertyDeep(path);
	}

	var property_1 = property;

	/**
	 * The base implementation of `_.iteratee`.
	 *
	 * @private
	 * @param {*} [value=_.identity] The value to convert to an iteratee.
	 * @returns {Function} Returns the iteratee.
	 */
	function baseIteratee(value) {
	  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
	  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
	  if (typeof value == 'function') {
	    return value;
	  }
	  if (value == null) {
	    return identity_1;
	  }
	  if (typeof value == 'object') {
	    return isArray_1(value)
	      ? _baseMatchesProperty(value[0], value[1])
	      : _baseMatches(value);
	  }
	  return property_1(value);
	}

	var _baseIteratee = baseIteratee;

	/**
	 * Creates a `baseEach` or `baseEachRight` function.
	 *
	 * @private
	 * @param {Function} eachFunc The function to iterate over a collection.
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {Function} Returns the new base function.
	 */
	function createBaseEach(eachFunc, fromRight) {
	  return function(collection, iteratee) {
	    if (collection == null) {
	      return collection;
	    }
	    if (!isArrayLike_1(collection)) {
	      return eachFunc(collection, iteratee);
	    }
	    var length = collection.length,
	        index = fromRight ? length : -1,
	        iterable = Object(collection);

	    while ((fromRight ? index-- : ++index < length)) {
	      if (iteratee(iterable[index], index, iterable) === false) {
	        break;
	      }
	    }
	    return collection;
	  };
	}

	var _createBaseEach = createBaseEach;

	/**
	 * The base implementation of `_.forEach` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Array|Object} collection The collection to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array|Object} Returns `collection`.
	 */
	var baseEach = _createBaseEach(_baseForOwn);

	var _baseEach = baseEach;

	/**
	 * The base implementation of `_.map` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Array|Object} collection The collection to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the new mapped array.
	 */
	function baseMap(collection, iteratee) {
	  var index = -1,
	      result = isArrayLike_1(collection) ? Array(collection.length) : [];

	  _baseEach(collection, function(value, key, collection) {
	    result[++index] = iteratee(value, key, collection);
	  });
	  return result;
	}

	var _baseMap = baseMap;

	/**
	 * Creates an array of values by running each element in `collection` thru
	 * `iteratee`. The iteratee is invoked with three arguments:
	 * (value, index|key, collection).
	 *
	 * Many lodash methods are guarded to work as iteratees for methods like
	 * `_.every`, `_.filter`, `_.map`, `_.mapValues`, `_.reject`, and `_.some`.
	 *
	 * The guarded methods are:
	 * `ary`, `chunk`, `curry`, `curryRight`, `drop`, `dropRight`, `every`,
	 * `fill`, `invert`, `parseInt`, `random`, `range`, `rangeRight`, `repeat`,
	 * `sampleSize`, `slice`, `some`, `sortBy`, `split`, `take`, `takeRight`,
	 * `template`, `trim`, `trimEnd`, `trimStart`, and `words`
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Collection
	 * @param {Array|Object} collection The collection to iterate over.
	 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
	 * @returns {Array} Returns the new mapped array.
	 * @example
	 *
	 * function square(n) {
	 *   return n * n;
	 * }
	 *
	 * _.map([4, 8], square);
	 * // => [16, 64]
	 *
	 * _.map({ 'a': 4, 'b': 8 }, square);
	 * // => [16, 64] (iteration order is not guaranteed)
	 *
	 * var users = [
	 *   { 'user': 'barney' },
	 *   { 'user': 'fred' }
	 * ];
	 *
	 * // The `_.property` iteratee shorthand.
	 * _.map(users, 'user');
	 * // => ['barney', 'fred']
	 */
	function map(collection, iteratee) {
	  var func = isArray_1(collection) ? _arrayMap : _baseMap;
	  return func(collection, _baseIteratee(iteratee, 3));
	}

	var map_1 = map;

	var flattenNames_1 = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.flattenNames = undefined;



	var _isString3 = _interopRequireDefault(isString_1);



	var _forOwn3 = _interopRequireDefault(forOwn_1);



	var _isPlainObject3 = _interopRequireDefault(isPlainObject_1);



	var _map3 = _interopRequireDefault(map_1);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var flattenNames = exports.flattenNames = function flattenNames() {
	  var things = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

	  var names = [];

	  (0, _map3.default)(things, function (thing) {
	    if (Array.isArray(thing)) {
	      flattenNames(thing).map(function (name) {
	        return names.push(name);
	      });
	    } else if ((0, _isPlainObject3.default)(thing)) {
	      (0, _forOwn3.default)(thing, function (value, key) {
	        value === true && names.push(key);
	        names.push(key + '-' + value);
	      });
	    } else if ((0, _isString3.default)(thing)) {
	      names.push(thing);
	    }
	  });

	  return names;
	};

	exports.default = flattenNames;
	});

	unwrapExports(flattenNames_1);
	var flattenNames_2 = flattenNames_1.flattenNames;

	/**
	 * A specialized version of `_.forEach` for arrays without support for
	 * iteratee shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns `array`.
	 */
	function arrayEach(array, iteratee) {
	  var index = -1,
	      length = array == null ? 0 : array.length;

	  while (++index < length) {
	    if (iteratee(array[index], index, array) === false) {
	      break;
	    }
	  }
	  return array;
	}

	var _arrayEach = arrayEach;

	var defineProperty = (function() {
	  try {
	    var func = _getNative(Object, 'defineProperty');
	    func({}, '', {});
	    return func;
	  } catch (e) {}
	}());

	var _defineProperty = defineProperty;

	/**
	 * The base implementation of `assignValue` and `assignMergeValue` without
	 * value checks.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
	function baseAssignValue(object, key, value) {
	  if (key == '__proto__' && _defineProperty) {
	    _defineProperty(object, key, {
	      'configurable': true,
	      'enumerable': true,
	      'value': value,
	      'writable': true
	    });
	  } else {
	    object[key] = value;
	  }
	}

	var _baseAssignValue = baseAssignValue;

	/** Used for built-in method references. */
	var objectProto$13 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$10 = objectProto$13.hasOwnProperty;

	/**
	 * Assigns `value` to `key` of `object` if the existing value is not equivalent
	 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * for equality comparisons.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
	function assignValue(object, key, value) {
	  var objValue = object[key];
	  if (!(hasOwnProperty$10.call(object, key) && eq_1(objValue, value)) ||
	      (value === undefined && !(key in object))) {
	    _baseAssignValue(object, key, value);
	  }
	}

	var _assignValue = assignValue;

	/**
	 * Copies properties of `source` to `object`.
	 *
	 * @private
	 * @param {Object} source The object to copy properties from.
	 * @param {Array} props The property identifiers to copy.
	 * @param {Object} [object={}] The object to copy properties to.
	 * @param {Function} [customizer] The function to customize copied values.
	 * @returns {Object} Returns `object`.
	 */
	function copyObject(source, props, object, customizer) {
	  var isNew = !object;
	  object || (object = {});

	  var index = -1,
	      length = props.length;

	  while (++index < length) {
	    var key = props[index];

	    var newValue = customizer
	      ? customizer(object[key], source[key], key, object, source)
	      : undefined;

	    if (newValue === undefined) {
	      newValue = source[key];
	    }
	    if (isNew) {
	      _baseAssignValue(object, key, newValue);
	    } else {
	      _assignValue(object, key, newValue);
	    }
	  }
	  return object;
	}

	var _copyObject = copyObject;

	/**
	 * The base implementation of `_.assign` without support for multiple sources
	 * or `customizer` functions.
	 *
	 * @private
	 * @param {Object} object The destination object.
	 * @param {Object} source The source object.
	 * @returns {Object} Returns `object`.
	 */
	function baseAssign(object, source) {
	  return object && _copyObject(source, keys_1(source), object);
	}

	var _baseAssign = baseAssign;

	/**
	 * This function is like
	 * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
	 * except that it includes inherited enumerable properties.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function nativeKeysIn(object) {
	  var result = [];
	  if (object != null) {
	    for (var key in Object(object)) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	var _nativeKeysIn = nativeKeysIn;

	/** Used for built-in method references. */
	var objectProto$14 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$11 = objectProto$14.hasOwnProperty;

	/**
	 * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function baseKeysIn(object) {
	  if (!isObject_1(object)) {
	    return _nativeKeysIn(object);
	  }
	  var isProto = _isPrototype(object),
	      result = [];

	  for (var key in object) {
	    if (!(key == 'constructor' && (isProto || !hasOwnProperty$11.call(object, key)))) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	var _baseKeysIn = baseKeysIn;

	/**
	 * Creates an array of the own and inherited enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keysIn(new Foo);
	 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
	 */
	function keysIn$1(object) {
	  return isArrayLike_1(object) ? _arrayLikeKeys(object, true) : _baseKeysIn(object);
	}

	var keysIn_1 = keysIn$1;

	/**
	 * The base implementation of `_.assignIn` without support for multiple sources
	 * or `customizer` functions.
	 *
	 * @private
	 * @param {Object} object The destination object.
	 * @param {Object} source The source object.
	 * @returns {Object} Returns `object`.
	 */
	function baseAssignIn(object, source) {
	  return object && _copyObject(source, keysIn_1(source), object);
	}

	var _baseAssignIn = baseAssignIn;

	var _cloneBuffer = createCommonjsModule(function (module, exports) {
	/** Detect free variable `exports`. */
	var freeExports = exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule = freeExports && 'object' == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports = freeModule && freeModule.exports === freeExports;

	/** Built-in value references. */
	var Buffer = moduleExports ? _root.Buffer : undefined,
	    allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined;

	/**
	 * Creates a clone of  `buffer`.
	 *
	 * @private
	 * @param {Buffer} buffer The buffer to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Buffer} Returns the cloned buffer.
	 */
	function cloneBuffer(buffer, isDeep) {
	  if (isDeep) {
	    return buffer.slice();
	  }
	  var length = buffer.length,
	      result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

	  buffer.copy(result);
	  return result;
	}

	module.exports = cloneBuffer;
	});

	/**
	 * Copies the values of `source` to `array`.
	 *
	 * @private
	 * @param {Array} source The array to copy values from.
	 * @param {Array} [array=[]] The array to copy values to.
	 * @returns {Array} Returns `array`.
	 */
	function copyArray(source, array) {
	  var index = -1,
	      length = source.length;

	  array || (array = Array(length));
	  while (++index < length) {
	    array[index] = source[index];
	  }
	  return array;
	}

	var _copyArray = copyArray;

	/**
	 * Copies own symbols of `source` to `object`.
	 *
	 * @private
	 * @param {Object} source The object to copy symbols from.
	 * @param {Object} [object={}] The object to copy symbols to.
	 * @returns {Object} Returns `object`.
	 */
	function copySymbols(source, object) {
	  return _copyObject(source, _getSymbols(source), object);
	}

	var _copySymbols = copySymbols;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeGetSymbols$1 = Object.getOwnPropertySymbols;

	/**
	 * Creates an array of the own and inherited enumerable symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of symbols.
	 */
	var getSymbolsIn = !nativeGetSymbols$1 ? stubArray_1 : function(object) {
	  var result = [];
	  while (object) {
	    _arrayPush(result, _getSymbols(object));
	    object = _getPrototype(object);
	  }
	  return result;
	};

	var _getSymbolsIn = getSymbolsIn;

	/**
	 * Copies own and inherited symbols of `source` to `object`.
	 *
	 * @private
	 * @param {Object} source The object to copy symbols from.
	 * @param {Object} [object={}] The object to copy symbols to.
	 * @returns {Object} Returns `object`.
	 */
	function copySymbolsIn(source, object) {
	  return _copyObject(source, _getSymbolsIn(source), object);
	}

	var _copySymbolsIn = copySymbolsIn;

	/**
	 * Creates an array of own and inherited enumerable property names and
	 * symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function getAllKeysIn(object) {
	  return _baseGetAllKeys(object, keysIn_1, _getSymbolsIn);
	}

	var _getAllKeysIn = getAllKeysIn;

	/** Used for built-in method references. */
	var objectProto$15 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$12 = objectProto$15.hasOwnProperty;

	/**
	 * Initializes an array clone.
	 *
	 * @private
	 * @param {Array} array The array to clone.
	 * @returns {Array} Returns the initialized clone.
	 */
	function initCloneArray(array) {
	  var length = array.length,
	      result = new array.constructor(length);

	  // Add properties assigned by `RegExp#exec`.
	  if (length && typeof array[0] == 'string' && hasOwnProperty$12.call(array, 'index')) {
	    result.index = array.index;
	    result.input = array.input;
	  }
	  return result;
	}

	var _initCloneArray = initCloneArray;

	/**
	 * Creates a clone of `arrayBuffer`.
	 *
	 * @private
	 * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
	 * @returns {ArrayBuffer} Returns the cloned array buffer.
	 */
	function cloneArrayBuffer(arrayBuffer) {
	  var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
	  new _Uint8Array(result).set(new _Uint8Array(arrayBuffer));
	  return result;
	}

	var _cloneArrayBuffer = cloneArrayBuffer;

	/**
	 * Creates a clone of `dataView`.
	 *
	 * @private
	 * @param {Object} dataView The data view to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Object} Returns the cloned data view.
	 */
	function cloneDataView(dataView, isDeep) {
	  var buffer = isDeep ? _cloneArrayBuffer(dataView.buffer) : dataView.buffer;
	  return new dataView.constructor(buffer, dataView.byteOffset, dataView.byteLength);
	}

	var _cloneDataView = cloneDataView;

	/** Used to match `RegExp` flags from their coerced string values. */
	var reFlags = /\w*$/;

	/**
	 * Creates a clone of `regexp`.
	 *
	 * @private
	 * @param {Object} regexp The regexp to clone.
	 * @returns {Object} Returns the cloned regexp.
	 */
	function cloneRegExp(regexp) {
	  var result = new regexp.constructor(regexp.source, reFlags.exec(regexp));
	  result.lastIndex = regexp.lastIndex;
	  return result;
	}

	var _cloneRegExp = cloneRegExp;

	/** Used to convert symbols to primitives and strings. */
	var symbolProto$2 = _Symbol ? _Symbol.prototype : undefined,
	    symbolValueOf$1 = symbolProto$2 ? symbolProto$2.valueOf : undefined;

	/**
	 * Creates a clone of the `symbol` object.
	 *
	 * @private
	 * @param {Object} symbol The symbol object to clone.
	 * @returns {Object} Returns the cloned symbol object.
	 */
	function cloneSymbol(symbol) {
	  return symbolValueOf$1 ? Object(symbolValueOf$1.call(symbol)) : {};
	}

	var _cloneSymbol = cloneSymbol;

	/**
	 * Creates a clone of `typedArray`.
	 *
	 * @private
	 * @param {Object} typedArray The typed array to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Object} Returns the cloned typed array.
	 */
	function cloneTypedArray(typedArray, isDeep) {
	  var buffer = isDeep ? _cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
	  return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
	}

	var _cloneTypedArray = cloneTypedArray;

	/** `Object#toString` result references. */
	var boolTag$2 = '[object Boolean]',
	    dateTag$2 = '[object Date]',
	    mapTag$3 = '[object Map]',
	    numberTag$2 = '[object Number]',
	    regexpTag$2 = '[object RegExp]',
	    setTag$3 = '[object Set]',
	    stringTag$3 = '[object String]',
	    symbolTag$2 = '[object Symbol]';

	var arrayBufferTag$2 = '[object ArrayBuffer]',
	    dataViewTag$3 = '[object DataView]',
	    float32Tag$1 = '[object Float32Array]',
	    float64Tag$1 = '[object Float64Array]',
	    int8Tag$1 = '[object Int8Array]',
	    int16Tag$1 = '[object Int16Array]',
	    int32Tag$1 = '[object Int32Array]',
	    uint8Tag$1 = '[object Uint8Array]',
	    uint8ClampedTag$1 = '[object Uint8ClampedArray]',
	    uint16Tag$1 = '[object Uint16Array]',
	    uint32Tag$1 = '[object Uint32Array]';

	/**
	 * Initializes an object clone based on its `toStringTag`.
	 *
	 * **Note:** This function only supports cloning values with tags of
	 * `Boolean`, `Date`, `Error`, `Map`, `Number`, `RegExp`, `Set`, or `String`.
	 *
	 * @private
	 * @param {Object} object The object to clone.
	 * @param {string} tag The `toStringTag` of the object to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Object} Returns the initialized clone.
	 */
	function initCloneByTag(object, tag, isDeep) {
	  var Ctor = object.constructor;
	  switch (tag) {
	    case arrayBufferTag$2:
	      return _cloneArrayBuffer(object);

	    case boolTag$2:
	    case dateTag$2:
	      return new Ctor(+object);

	    case dataViewTag$3:
	      return _cloneDataView(object, isDeep);

	    case float32Tag$1: case float64Tag$1:
	    case int8Tag$1: case int16Tag$1: case int32Tag$1:
	    case uint8Tag$1: case uint8ClampedTag$1: case uint16Tag$1: case uint32Tag$1:
	      return _cloneTypedArray(object, isDeep);

	    case mapTag$3:
	      return new Ctor;

	    case numberTag$2:
	    case stringTag$3:
	      return new Ctor(object);

	    case regexpTag$2:
	      return _cloneRegExp(object);

	    case setTag$3:
	      return new Ctor;

	    case symbolTag$2:
	      return _cloneSymbol(object);
	  }
	}

	var _initCloneByTag = initCloneByTag;

	/** Built-in value references. */
	var objectCreate = Object.create;

	/**
	 * The base implementation of `_.create` without support for assigning
	 * properties to the created object.
	 *
	 * @private
	 * @param {Object} proto The object to inherit from.
	 * @returns {Object} Returns the new object.
	 */
	var baseCreate = (function() {
	  function object() {}
	  return function(proto) {
	    if (!isObject_1(proto)) {
	      return {};
	    }
	    if (objectCreate) {
	      return objectCreate(proto);
	    }
	    object.prototype = proto;
	    var result = new object;
	    object.prototype = undefined;
	    return result;
	  };
	}());

	var _baseCreate = baseCreate;

	/**
	 * Initializes an object clone.
	 *
	 * @private
	 * @param {Object} object The object to clone.
	 * @returns {Object} Returns the initialized clone.
	 */
	function initCloneObject(object) {
	  return (typeof object.constructor == 'function' && !_isPrototype(object))
	    ? _baseCreate(_getPrototype(object))
	    : {};
	}

	var _initCloneObject = initCloneObject;

	/** `Object#toString` result references. */
	var mapTag$4 = '[object Map]';

	/**
	 * The base implementation of `_.isMap` without Node.js optimizations.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a map, else `false`.
	 */
	function baseIsMap(value) {
	  return isObjectLike_1(value) && _getTag(value) == mapTag$4;
	}

	var _baseIsMap = baseIsMap;

	/* Node.js helper references. */
	var nodeIsMap = _nodeUtil && _nodeUtil.isMap;

	/**
	 * Checks if `value` is classified as a `Map` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.3.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a map, else `false`.
	 * @example
	 *
	 * _.isMap(new Map);
	 * // => true
	 *
	 * _.isMap(new WeakMap);
	 * // => false
	 */
	var isMap = nodeIsMap ? _baseUnary(nodeIsMap) : _baseIsMap;

	var isMap_1 = isMap;

	/** `Object#toString` result references. */
	var setTag$4 = '[object Set]';

	/**
	 * The base implementation of `_.isSet` without Node.js optimizations.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a set, else `false`.
	 */
	function baseIsSet(value) {
	  return isObjectLike_1(value) && _getTag(value) == setTag$4;
	}

	var _baseIsSet = baseIsSet;

	/* Node.js helper references. */
	var nodeIsSet = _nodeUtil && _nodeUtil.isSet;

	/**
	 * Checks if `value` is classified as a `Set` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.3.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a set, else `false`.
	 * @example
	 *
	 * _.isSet(new Set);
	 * // => true
	 *
	 * _.isSet(new WeakSet);
	 * // => false
	 */
	var isSet = nodeIsSet ? _baseUnary(nodeIsSet) : _baseIsSet;

	var isSet_1 = isSet;

	/** Used to compose bitmasks for cloning. */
	var CLONE_DEEP_FLAG = 1,
	    CLONE_FLAT_FLAG = 2,
	    CLONE_SYMBOLS_FLAG = 4;

	/** `Object#toString` result references. */
	var argsTag$3 = '[object Arguments]',
	    arrayTag$2 = '[object Array]',
	    boolTag$3 = '[object Boolean]',
	    dateTag$3 = '[object Date]',
	    errorTag$2 = '[object Error]',
	    funcTag$2 = '[object Function]',
	    genTag$1 = '[object GeneratorFunction]',
	    mapTag$5 = '[object Map]',
	    numberTag$3 = '[object Number]',
	    objectTag$4 = '[object Object]',
	    regexpTag$3 = '[object RegExp]',
	    setTag$5 = '[object Set]',
	    stringTag$4 = '[object String]',
	    symbolTag$3 = '[object Symbol]',
	    weakMapTag$2 = '[object WeakMap]';

	var arrayBufferTag$3 = '[object ArrayBuffer]',
	    dataViewTag$4 = '[object DataView]',
	    float32Tag$2 = '[object Float32Array]',
	    float64Tag$2 = '[object Float64Array]',
	    int8Tag$2 = '[object Int8Array]',
	    int16Tag$2 = '[object Int16Array]',
	    int32Tag$2 = '[object Int32Array]',
	    uint8Tag$2 = '[object Uint8Array]',
	    uint8ClampedTag$2 = '[object Uint8ClampedArray]',
	    uint16Tag$2 = '[object Uint16Array]',
	    uint32Tag$2 = '[object Uint32Array]';

	/** Used to identify `toStringTag` values supported by `_.clone`. */
	var cloneableTags = {};
	cloneableTags[argsTag$3] = cloneableTags[arrayTag$2] =
	cloneableTags[arrayBufferTag$3] = cloneableTags[dataViewTag$4] =
	cloneableTags[boolTag$3] = cloneableTags[dateTag$3] =
	cloneableTags[float32Tag$2] = cloneableTags[float64Tag$2] =
	cloneableTags[int8Tag$2] = cloneableTags[int16Tag$2] =
	cloneableTags[int32Tag$2] = cloneableTags[mapTag$5] =
	cloneableTags[numberTag$3] = cloneableTags[objectTag$4] =
	cloneableTags[regexpTag$3] = cloneableTags[setTag$5] =
	cloneableTags[stringTag$4] = cloneableTags[symbolTag$3] =
	cloneableTags[uint8Tag$2] = cloneableTags[uint8ClampedTag$2] =
	cloneableTags[uint16Tag$2] = cloneableTags[uint32Tag$2] = true;
	cloneableTags[errorTag$2] = cloneableTags[funcTag$2] =
	cloneableTags[weakMapTag$2] = false;

	/**
	 * The base implementation of `_.clone` and `_.cloneDeep` which tracks
	 * traversed objects.
	 *
	 * @private
	 * @param {*} value The value to clone.
	 * @param {boolean} bitmask The bitmask flags.
	 *  1 - Deep clone
	 *  2 - Flatten inherited properties
	 *  4 - Clone symbols
	 * @param {Function} [customizer] The function to customize cloning.
	 * @param {string} [key] The key of `value`.
	 * @param {Object} [object] The parent object of `value`.
	 * @param {Object} [stack] Tracks traversed objects and their clone counterparts.
	 * @returns {*} Returns the cloned value.
	 */
	function baseClone(value, bitmask, customizer, key, object, stack) {
	  var result,
	      isDeep = bitmask & CLONE_DEEP_FLAG,
	      isFlat = bitmask & CLONE_FLAT_FLAG,
	      isFull = bitmask & CLONE_SYMBOLS_FLAG;

	  if (customizer) {
	    result = object ? customizer(value, key, object, stack) : customizer(value);
	  }
	  if (result !== undefined) {
	    return result;
	  }
	  if (!isObject_1(value)) {
	    return value;
	  }
	  var isArr = isArray_1(value);
	  if (isArr) {
	    result = _initCloneArray(value);
	    if (!isDeep) {
	      return _copyArray(value, result);
	    }
	  } else {
	    var tag = _getTag(value),
	        isFunc = tag == funcTag$2 || tag == genTag$1;

	    if (isBuffer_1(value)) {
	      return _cloneBuffer(value, isDeep);
	    }
	    if (tag == objectTag$4 || tag == argsTag$3 || (isFunc && !object)) {
	      result = (isFlat || isFunc) ? {} : _initCloneObject(value);
	      if (!isDeep) {
	        return isFlat
	          ? _copySymbolsIn(value, _baseAssignIn(result, value))
	          : _copySymbols(value, _baseAssign(result, value));
	      }
	    } else {
	      if (!cloneableTags[tag]) {
	        return object ? value : {};
	      }
	      result = _initCloneByTag(value, tag, isDeep);
	    }
	  }
	  // Check for circular references and return its corresponding clone.
	  stack || (stack = new _Stack);
	  var stacked = stack.get(value);
	  if (stacked) {
	    return stacked;
	  }
	  stack.set(value, result);

	  if (isSet_1(value)) {
	    value.forEach(function(subValue) {
	      result.add(baseClone(subValue, bitmask, customizer, subValue, value, stack));
	    });

	    return result;
	  }

	  if (isMap_1(value)) {
	    value.forEach(function(subValue, key) {
	      result.set(key, baseClone(subValue, bitmask, customizer, key, value, stack));
	    });

	    return result;
	  }

	  var keysFunc = isFull
	    ? (isFlat ? _getAllKeysIn : _getAllKeys)
	    : (isFlat ? keysIn : keys_1);

	  var props = isArr ? undefined : keysFunc(value);
	  _arrayEach(props || value, function(subValue, key) {
	    if (props) {
	      key = subValue;
	      subValue = value[key];
	    }
	    // Recursively populate clone (susceptible to call stack limits).
	    _assignValue(result, key, baseClone(subValue, bitmask, customizer, key, value, stack));
	  });
	  return result;
	}

	var _baseClone = baseClone;

	/** Used to compose bitmasks for cloning. */
	var CLONE_DEEP_FLAG$1 = 1,
	    CLONE_SYMBOLS_FLAG$1 = 4;

	/**
	 * This method is like `_.clone` except that it recursively clones `value`.
	 *
	 * @static
	 * @memberOf _
	 * @since 1.0.0
	 * @category Lang
	 * @param {*} value The value to recursively clone.
	 * @returns {*} Returns the deep cloned value.
	 * @see _.clone
	 * @example
	 *
	 * var objects = [{ 'a': 1 }, { 'b': 2 }];
	 *
	 * var deep = _.cloneDeep(objects);
	 * console.log(deep[0] === objects[0]);
	 * // => false
	 */
	function cloneDeep(value) {
	  return _baseClone(value, CLONE_DEEP_FLAG$1 | CLONE_SYMBOLS_FLAG$1);
	}

	var cloneDeep_1 = cloneDeep;

	var mergeClasses_1 = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.mergeClasses = undefined;



	var _forOwn3 = _interopRequireDefault(forOwn_1);



	var _cloneDeep3 = _interopRequireDefault(cloneDeep_1);

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var mergeClasses = exports.mergeClasses = function mergeClasses(classes) {
	  var activeNames = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

	  var styles = classes.default && (0, _cloneDeep3.default)(classes.default) || {};
	  activeNames.map(function (name) {
	    var toMerge = classes[name];
	    if (toMerge) {
	      (0, _forOwn3.default)(toMerge, function (value, key) {
	        if (!styles[key]) {
	          styles[key] = {};
	        }

	        styles[key] = _extends({}, styles[key], toMerge[key]);
	      });
	    }

	    return name;
	  });
	  return styles;
	};

	exports.default = mergeClasses;
	});

	unwrapExports(mergeClasses_1);
	var mergeClasses_2 = mergeClasses_1.mergeClasses;

	var autoprefix_1 = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.autoprefix = undefined;



	var _forOwn3 = _interopRequireDefault(forOwn_1);

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var transforms = {
	  borderRadius: function borderRadius(value) {
	    return {
	      msBorderRadius: value,
	      MozBorderRadius: value,
	      OBorderRadius: value,
	      WebkitBorderRadius: value,
	      borderRadius: value
	    };
	  },
	  boxShadow: function boxShadow(value) {
	    return {
	      msBoxShadow: value,
	      MozBoxShadow: value,
	      OBoxShadow: value,
	      WebkitBoxShadow: value,
	      boxShadow: value
	    };
	  },
	  userSelect: function userSelect(value) {
	    return {
	      WebkitTouchCallout: value,
	      KhtmlUserSelect: value,
	      MozUserSelect: value,
	      msUserSelect: value,
	      WebkitUserSelect: value,
	      userSelect: value
	    };
	  },

	  flex: function flex(value) {
	    return {
	      WebkitBoxFlex: value,
	      MozBoxFlex: value,
	      WebkitFlex: value,
	      msFlex: value,
	      flex: value
	    };
	  },
	  flexBasis: function flexBasis(value) {
	    return {
	      WebkitFlexBasis: value,
	      flexBasis: value
	    };
	  },
	  justifyContent: function justifyContent(value) {
	    return {
	      WebkitJustifyContent: value,
	      justifyContent: value
	    };
	  },

	  transition: function transition(value) {
	    return {
	      msTransition: value,
	      MozTransition: value,
	      OTransition: value,
	      WebkitTransition: value,
	      transition: value
	    };
	  },

	  transform: function transform(value) {
	    return {
	      msTransform: value,
	      MozTransform: value,
	      OTransform: value,
	      WebkitTransform: value,
	      transform: value
	    };
	  },
	  absolute: function absolute(value) {
	    var direction = value && value.split(' ');
	    return {
	      position: 'absolute',
	      top: direction && direction[0],
	      right: direction && direction[1],
	      bottom: direction && direction[2],
	      left: direction && direction[3]
	    };
	  },
	  extend: function extend(name, otherElementStyles) {
	    var otherStyle = otherElementStyles[name];
	    if (otherStyle) {
	      return otherStyle;
	    }
	    return {
	      'extend': name
	    };
	  }
	};

	var autoprefix = exports.autoprefix = function autoprefix(elements) {
	  var prefixed = {};
	  (0, _forOwn3.default)(elements, function (styles, element) {
	    var expanded = {};
	    (0, _forOwn3.default)(styles, function (value, key) {
	      var transform = transforms[key];
	      if (transform) {
	        expanded = _extends({}, expanded, transform(value));
	      } else {
	        expanded[key] = value;
	      }
	    });
	    prefixed[element] = expanded;
	  });
	  return prefixed;
	};

	exports.default = autoprefix;
	});

	unwrapExports(autoprefix_1);
	var autoprefix_2 = autoprefix_1.autoprefix;

	var hover_1 = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.hover = undefined;

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



	var _react2 = _interopRequireDefault(React__default);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var hover = exports.hover = function hover(Component) {
	  var Span = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'span';

	  return function (_React$Component) {
	    _inherits(Hover, _React$Component);

	    function Hover() {
	      var _ref;

	      var _temp, _this, _ret;

	      _classCallCheck(this, Hover);

	      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	        args[_key] = arguments[_key];
	      }

	      return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Hover.__proto__ || Object.getPrototypeOf(Hover)).call.apply(_ref, [this].concat(args))), _this), _this.state = { hover: false }, _this.handleMouseOver = function () {
	        return _this.setState({ hover: true });
	      }, _this.handleMouseOut = function () {
	        return _this.setState({ hover: false });
	      }, _this.render = function () {
	        return _react2.default.createElement(
	          Span,
	          { onMouseOver: _this.handleMouseOver, onMouseOut: _this.handleMouseOut },
	          _react2.default.createElement(Component, _extends({}, _this.props, _this.state))
	        );
	      }, _temp), _possibleConstructorReturn(_this, _ret);
	    }

	    return Hover;
	  }(_react2.default.Component);
	};

	exports.default = hover;
	});

	unwrapExports(hover_1);
	var hover_2 = hover_1.hover;

	var active_1 = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.active = undefined;

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



	var _react2 = _interopRequireDefault(React__default);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var active = exports.active = function active(Component) {
	  var Span = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'span';

	  return function (_React$Component) {
	    _inherits(Active, _React$Component);

	    function Active() {
	      var _ref;

	      var _temp, _this, _ret;

	      _classCallCheck(this, Active);

	      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	        args[_key] = arguments[_key];
	      }

	      return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Active.__proto__ || Object.getPrototypeOf(Active)).call.apply(_ref, [this].concat(args))), _this), _this.state = { active: false }, _this.handleMouseDown = function () {
	        return _this.setState({ active: true });
	      }, _this.handleMouseUp = function () {
	        return _this.setState({ active: false });
	      }, _this.render = function () {
	        return _react2.default.createElement(
	          Span,
	          { onMouseDown: _this.handleMouseDown, onMouseUp: _this.handleMouseUp },
	          _react2.default.createElement(Component, _extends({}, _this.props, _this.state))
	        );
	      }, _temp), _possibleConstructorReturn(_this, _ret);
	    }

	    return Active;
	  }(_react2.default.Component);
	};

	exports.default = active;
	});

	unwrapExports(active_1);
	var active_2 = active_1.active;

	var loop = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var loopable = function loopable(i, length) {
	  var props = {};
	  var setProp = function setProp(name) {
	    var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	    props[name] = value;
	  };

	  i === 0 && setProp('first-child');
	  i === length - 1 && setProp('last-child');
	  (i === 0 || i % 2 === 0) && setProp('even');
	  Math.abs(i % 2) === 1 && setProp('odd');
	  setProp('nth-child', i);

	  return props;
	};

	exports.default = loopable;
	});

	unwrapExports(loop);

	var lib = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.ReactCSS = exports.loop = exports.handleActive = exports.handleHover = exports.hover = undefined;



	var _flattenNames2 = _interopRequireDefault(flattenNames_1);



	var _mergeClasses2 = _interopRequireDefault(mergeClasses_1);



	var _autoprefix2 = _interopRequireDefault(autoprefix_1);



	var _hover3 = _interopRequireDefault(hover_1);



	var _active2 = _interopRequireDefault(active_1);



	var _loop3 = _interopRequireDefault(loop);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.hover = _hover3.default;
	exports.handleHover = _hover3.default;
	exports.handleActive = _active2.default;
	exports.loop = _loop3.default;
	var ReactCSS = exports.ReactCSS = function ReactCSS(classes) {
	  for (var _len = arguments.length, activations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	    activations[_key - 1] = arguments[_key];
	  }

	  var activeNames = (0, _flattenNames2.default)(activations);
	  var merged = (0, _mergeClasses2.default)(classes, activeNames);
	  return (0, _autoprefix2.default)(merged);
	};

	exports.default = ReactCSS;
	});

	var reactCSS = unwrapExports(lib);
	var lib_1 = lib.ReactCSS;
	var lib_2 = lib.loop;
	var lib_3 = lib.handleActive;
	var lib_4 = lib.handleHover;
	var lib_5 = lib.hover;

	var calculateChange = function calculateChange(e, skip, props, container) {
	  e.preventDefault();
	  var containerWidth = container.clientWidth;
	  var containerHeight = container.clientHeight;
	  var x = typeof e.pageX === 'number' ? e.pageX : e.touches[0].pageX;
	  var y = typeof e.pageY === 'number' ? e.pageY : e.touches[0].pageY;
	  var left = x - (container.getBoundingClientRect().left + window.pageXOffset);
	  var top = y - (container.getBoundingClientRect().top + window.pageYOffset);

	  if (props.direction === 'vertical') {
	    var a = void 0;
	    if (top < 0) {
	      a = 0;
	    } else if (top > containerHeight) {
	      a = 1;
	    } else {
	      a = Math.round(top * 100 / containerHeight) / 100;
	    }

	    if (props.hsl.a !== a) {
	      return {
	        h: props.hsl.h,
	        s: props.hsl.s,
	        l: props.hsl.l,
	        a: a,
	        source: 'rgb'
	      };
	    }
	  } else {
	    var _a = void 0;
	    if (left < 0) {
	      _a = 0;
	    } else if (left > containerWidth) {
	      _a = 1;
	    } else {
	      _a = Math.round(left * 100 / containerWidth) / 100;
	    }

	    if (props.a !== _a) {
	      return {
	        h: props.hsl.h,
	        s: props.hsl.s,
	        l: props.hsl.l,
	        a: _a,
	        source: 'rgb'
	      };
	    }
	  }
	  return null;
	};

	var checkboardCache = {};

	var render = function render(c1, c2, size, serverCanvas) {
	  if (typeof document === 'undefined' && !serverCanvas) {
	    return null;
	  }
	  var canvas = serverCanvas ? new serverCanvas() : document.createElement('canvas');
	  canvas.width = size * 2;
	  canvas.height = size * 2;
	  var ctx = canvas.getContext('2d');
	  if (!ctx) {
	    return null;
	  } // If no context can be found, return early.
	  ctx.fillStyle = c1;
	  ctx.fillRect(0, 0, canvas.width, canvas.height);
	  ctx.fillStyle = c2;
	  ctx.fillRect(0, 0, size, size);
	  ctx.translate(size, size);
	  ctx.fillRect(0, 0, size, size);
	  return canvas.toDataURL();
	};

	var get$1 = function get(c1, c2, size, serverCanvas) {
	  var key = c1 + '-' + c2 + '-' + size + (serverCanvas ? '-server' : '');
	  var checkboard = render(c1, c2, size, serverCanvas);

	  if (checkboardCache[key]) {
	    return checkboardCache[key];
	  }
	  checkboardCache[key] = checkboard;
	  return checkboard;
	};

	var Checkboard = function Checkboard(_ref) {
	  var white = _ref.white,
	      grey = _ref.grey,
	      size = _ref.size,
	      renderers = _ref.renderers,
	      borderRadius = _ref.borderRadius,
	      boxShadow = _ref.boxShadow;

	  var styles = reactCSS({
	    'default': {
	      grid: {
	        borderRadius: borderRadius,
	        boxShadow: boxShadow,
	        absolute: '0px 0px 0px 0px',
	        background: 'url(' + get$1(white, grey, size, renderers.canvas) + ') center left'
	      }
	    }
	  });

	  return React__default.createElement('div', { style: styles.grid });
	};

	Checkboard.defaultProps = {
	  size: 8,
	  white: 'transparent',
	  grey: 'rgba(0,0,0,.08)',
	  renderers: {}
	};

	var classCallCheck = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

	var createClass = function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      Object.defineProperty(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	}();

	var defineProperty$1 = function (obj, key, value) {
	  if (key in obj) {
	    Object.defineProperty(obj, key, {
	      value: value,
	      enumerable: true,
	      configurable: true,
	      writable: true
	    });
	  } else {
	    obj[key] = value;
	  }

	  return obj;
	};

	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];

	    for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }

	  return target;
	};

	var inherits = function (subClass, superClass) {
	  if (typeof superClass !== "function" && superClass !== null) {
	    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	  }

	  subClass.prototype = Object.create(superClass && superClass.prototype, {
	    constructor: {
	      value: subClass,
	      enumerable: false,
	      writable: true,
	      configurable: true
	    }
	  });
	  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	};

	var possibleConstructorReturn = function (self, call) {
	  if (!self) {
	    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	  }

	  return call && (typeof call === "object" || typeof call === "function") ? call : self;
	};

	var Alpha = function (_ref) {
	  inherits(Alpha, _ref);

	  function Alpha() {
	    var _ref2;

	    var _temp, _this, _ret;

	    classCallCheck(this, Alpha);

	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref2 = Alpha.__proto__ || Object.getPrototypeOf(Alpha)).call.apply(_ref2, [this].concat(args))), _this), _this.handleChange = function (e, skip) {
	      var change = calculateChange(e, skip, _this.props, _this.container);
	      change && _this.props.onChange && _this.props.onChange(change, e);
	    }, _this.handleMouseDown = function (e) {
	      _this.handleChange(e, true);
	      window.addEventListener('mousemove', _this.handleChange);
	      window.addEventListener('mouseup', _this.handleMouseUp);
	    }, _this.handleMouseUp = function () {
	      _this.unbindEventListeners();
	    }, _this.unbindEventListeners = function () {
	      window.removeEventListener('mousemove', _this.handleChange);
	      window.removeEventListener('mouseup', _this.handleMouseUp);
	    }, _temp), possibleConstructorReturn(_this, _ret);
	  }

	  createClass(Alpha, [{
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      this.unbindEventListeners();
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;

	      var rgb = this.props.rgb;
	      var styles = reactCSS({
	        'default': {
	          alpha: {
	            absolute: '0px 0px 0px 0px',
	            borderRadius: this.props.radius
	          },
	          checkboard: {
	            absolute: '0px 0px 0px 0px',
	            overflow: 'hidden',
	            borderRadius: this.props.radius
	          },
	          gradient: {
	            absolute: '0px 0px 0px 0px',
	            background: 'linear-gradient(to right, rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ', 0) 0%,\n           rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ', 1) 100%)',
	            boxShadow: this.props.shadow,
	            borderRadius: this.props.radius
	          },
	          container: {
	            position: 'relative',
	            height: '100%',
	            margin: '0 3px'
	          },
	          pointer: {
	            position: 'absolute',
	            left: rgb.a * 100 + '%'
	          },
	          slider: {
	            width: '4px',
	            borderRadius: '1px',
	            height: '8px',
	            boxShadow: '0 0 2px rgba(0, 0, 0, .6)',
	            background: '#fff',
	            marginTop: '1px',
	            transform: 'translateX(-2px)'
	          }
	        },
	        'vertical': {
	          gradient: {
	            background: 'linear-gradient(to bottom, rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ', 0) 0%,\n           rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ', 1) 100%)'
	          },
	          pointer: {
	            left: 0,
	            top: rgb.a * 100 + '%'
	          }
	        },
	        'overwrite': _extends({}, this.props.style)
	      }, {
	        vertical: this.props.direction === 'vertical',
	        overwrite: true
	      });

	      return React__default.createElement(
	        'div',
	        { style: styles.alpha },
	        React__default.createElement(
	          'div',
	          { style: styles.checkboard },
	          React__default.createElement(Checkboard, { renderers: this.props.renderers })
	        ),
	        React__default.createElement('div', { style: styles.gradient }),
	        React__default.createElement(
	          'div',
	          {
	            style: styles.container,
	            ref: function ref(container) {
	              return _this2.container = container;
	            },
	            onMouseDown: this.handleMouseDown,
	            onTouchMove: this.handleChange,
	            onTouchStart: this.handleChange
	          },
	          React__default.createElement(
	            'div',
	            { style: styles.pointer },
	            this.props.pointer ? React__default.createElement(this.props.pointer, this.props) : React__default.createElement('div', { style: styles.slider })
	          )
	        )
	      );
	    }
	  }]);
	  return Alpha;
	}(React.PureComponent || React.Component);

	var EditableInput = function (_ref) {
	  inherits(EditableInput, _ref);

	  function EditableInput(props) {
	    classCallCheck(this, EditableInput);

	    var _this = possibleConstructorReturn(this, (EditableInput.__proto__ || Object.getPrototypeOf(EditableInput)).call(this));

	    _this.handleBlur = function () {
	      if (_this.state.blurValue) {
	        _this.setState({ value: _this.state.blurValue, blurValue: null });
	      }
	    };

	    _this.handleChange = function (e) {
	      if (_this.props.label) {
	        _this.props.onChange && _this.props.onChange(defineProperty$1({}, _this.props.label, e.target.value), e);
	      } else {
	        _this.props.onChange && _this.props.onChange(e.target.value, e);
	      }

	      _this.setState({ value: e.target.value });
	    };

	    _this.handleKeyDown = function (e) {
	      // In case `e.target.value` is a percentage remove the `%` character
	      // and update accordingly with a percentage
	      // https://github.com/casesandberg/react-color/issues/383
	      var stringValue = String(e.target.value);
	      var isPercentage = stringValue.indexOf('%') > -1;
	      var number = Number(stringValue.replace(/%/g, ''));
	      if (!isNaN(number)) {
	        var amount = _this.props.arrowOffset || 1;

	        // Up
	        if (e.keyCode === 38) {
	          if (_this.props.label !== null) {
	            _this.props.onChange && _this.props.onChange(defineProperty$1({}, _this.props.label, number + amount), e);
	          } else {
	            _this.props.onChange && _this.props.onChange(number + amount, e);
	          }

	          if (isPercentage) {
	            _this.setState({ value: number + amount + '%' });
	          } else {
	            _this.setState({ value: number + amount });
	          }
	        }

	        // Down
	        if (e.keyCode === 40) {
	          if (_this.props.label !== null) {
	            _this.props.onChange && _this.props.onChange(defineProperty$1({}, _this.props.label, number - amount), e);
	          } else {
	            _this.props.onChange && _this.props.onChange(number - amount, e);
	          }

	          if (isPercentage) {
	            _this.setState({ value: number - amount + '%' });
	          } else {
	            _this.setState({ value: number - amount });
	          }
	        }
	      }
	    };

	    _this.handleDrag = function (e) {
	      if (_this.props.dragLabel) {
	        var newValue = Math.round(_this.props.value + e.movementX);
	        if (newValue >= 0 && newValue <= _this.props.dragMax) {
	          _this.props.onChange && _this.props.onChange(defineProperty$1({}, _this.props.label, newValue), e);
	        }
	      }
	    };

	    _this.handleMouseDown = function (e) {
	      if (_this.props.dragLabel) {
	        e.preventDefault();
	        _this.handleDrag(e);
	        window.addEventListener('mousemove', _this.handleDrag);
	        window.addEventListener('mouseup', _this.handleMouseUp);
	      }
	    };

	    _this.handleMouseUp = function () {
	      _this.unbindEventListeners();
	    };

	    _this.unbindEventListeners = function () {
	      window.removeEventListener('mousemove', _this.handleDrag);
	      window.removeEventListener('mouseup', _this.handleMouseUp);
	    };

	    _this.state = {
	      value: String(props.value).toUpperCase(),
	      blurValue: String(props.value).toUpperCase()
	    };
	    return _this;
	  }

	  createClass(EditableInput, [{
	    key: 'componentWillReceiveProps',
	    value: function componentWillReceiveProps(nextProps) {
	      var input = this.input;
	      if (nextProps.value !== this.state.value) {
	        if (input === document.activeElement) {
	          this.setState({ blurValue: String(nextProps.value).toUpperCase() });
	        } else {
	          this.setState({ value: String(nextProps.value).toUpperCase(), blurValue: !this.state.blurValue && String(nextProps.value).toUpperCase() });
	        }
	      }
	    }
	  }, {
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      this.unbindEventListeners();
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;

	      var styles = reactCSS({
	        'default': {
	          wrap: {
	            position: 'relative'
	          }
	        },
	        'user-override': {
	          wrap: this.props.style && this.props.style.wrap ? this.props.style.wrap : {},
	          input: this.props.style && this.props.style.input ? this.props.style.input : {},
	          label: this.props.style && this.props.style.label ? this.props.style.label : {}
	        },
	        'dragLabel-true': {
	          label: {
	            cursor: 'ew-resize'
	          }
	        }
	      }, {
	        'user-override': true
	      }, this.props);

	      return React__default.createElement(
	        'div',
	        { style: styles.wrap },
	        React__default.createElement('input', {
	          style: styles.input,
	          ref: function ref(input) {
	            return _this2.input = input;
	          },
	          value: this.state.value,
	          onKeyDown: this.handleKeyDown,
	          onChange: this.handleChange,
	          onBlur: this.handleBlur,
	          placeholder: this.props.placeholder,
	          spellCheck: 'false'
	        }),
	        this.props.label && !this.props.hideLabel ? React__default.createElement(
	          'span',
	          { style: styles.label, onMouseDown: this.handleMouseDown },
	          this.props.label
	        ) : null
	      );
	    }
	  }]);
	  return EditableInput;
	}(React.PureComponent || React.Component);

	var calculateChange$1 = function calculateChange(e, skip, props, container) {
	  e.preventDefault();
	  var containerWidth = container.clientWidth;
	  var containerHeight = container.clientHeight;
	  var x = typeof e.pageX === 'number' ? e.pageX : e.touches[0].pageX;
	  var y = typeof e.pageY === 'number' ? e.pageY : e.touches[0].pageY;
	  var left = x - (container.getBoundingClientRect().left + window.pageXOffset);
	  var top = y - (container.getBoundingClientRect().top + window.pageYOffset);

	  if (props.direction === 'vertical') {
	    var h = void 0;
	    if (top < 0) {
	      h = 359;
	    } else if (top > containerHeight) {
	      h = 0;
	    } else {
	      var percent = -(top * 100 / containerHeight) + 100;
	      h = 360 * percent / 100;
	    }

	    if (props.hsl.h !== h) {
	      return {
	        h: h,
	        s: props.hsl.s,
	        l: props.hsl.l,
	        a: props.hsl.a,
	        source: 'rgb'
	      };
	    }
	  } else {
	    var _h = void 0;
	    if (left < 0) {
	      _h = 0;
	    } else if (left > containerWidth) {
	      _h = 359;
	    } else {
	      var _percent = left * 100 / containerWidth;
	      _h = 360 * _percent / 100;
	    }

	    if (props.hsl.h !== _h) {
	      return {
	        h: _h,
	        s: props.hsl.s,
	        l: props.hsl.l,
	        a: props.hsl.a,
	        source: 'rgb'
	      };
	    }
	  }
	  return null;
	};

	var Hue = function (_ref) {
	  inherits(Hue, _ref);

	  function Hue() {
	    var _ref2;

	    var _temp, _this, _ret;

	    classCallCheck(this, Hue);

	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref2 = Hue.__proto__ || Object.getPrototypeOf(Hue)).call.apply(_ref2, [this].concat(args))), _this), _this.handleChange = function (e, skip) {
	      var change = calculateChange$1(e, skip, _this.props, _this.container);
	      change && _this.props.onChange && _this.props.onChange(change, e);
	    }, _this.handleMouseDown = function (e) {
	      _this.handleChange(e, true);
	      window.addEventListener('mousemove', _this.handleChange);
	      window.addEventListener('mouseup', _this.handleMouseUp);
	    }, _this.handleMouseUp = function () {
	      _this.unbindEventListeners();
	    }, _temp), possibleConstructorReturn(_this, _ret);
	  }

	  createClass(Hue, [{
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      this.unbindEventListeners();
	    }
	  }, {
	    key: 'unbindEventListeners',
	    value: function unbindEventListeners() {
	      window.removeEventListener('mousemove', this.handleChange);
	      window.removeEventListener('mouseup', this.handleMouseUp);
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;

	      var _props$direction = this.props.direction,
	          direction = _props$direction === undefined ? 'horizontal' : _props$direction;


	      var styles = reactCSS({
	        'default': {
	          hue: {
	            absolute: '0px 0px 0px 0px',
	            borderRadius: this.props.radius,
	            boxShadow: this.props.shadow
	          },
	          container: {
	            padding: '0 2px',
	            position: 'relative',
	            height: '100%',
	            borderRadius: this.props.radius
	          },
	          pointer: {
	            position: 'absolute',
	            left: this.props.hsl.h * 100 / 360 + '%'
	          },
	          slider: {
	            marginTop: '1px',
	            width: '4px',
	            borderRadius: '1px',
	            height: '8px',
	            boxShadow: '0 0 2px rgba(0, 0, 0, .6)',
	            background: '#fff',
	            transform: 'translateX(-2px)'
	          }
	        },
	        'vertical': {
	          pointer: {
	            left: '0px',
	            top: -(this.props.hsl.h * 100 / 360) + 100 + '%'
	          }
	        }
	      }, { vertical: direction === 'vertical' });

	      return React__default.createElement(
	        'div',
	        { style: styles.hue },
	        React__default.createElement(
	          'div',
	          {
	            className: 'hue-' + direction,
	            style: styles.container,
	            ref: function ref(container) {
	              return _this2.container = container;
	            },
	            onMouseDown: this.handleMouseDown,
	            onTouchMove: this.handleChange,
	            onTouchStart: this.handleChange
	          },
	          React__default.createElement(
	            'style',
	            null,
	            '\n            .hue-horizontal {\n              background: linear-gradient(to right, #f00 0%, #ff0 17%, #0f0\n                33%, #0ff 50%, #00f 67%, #f0f 83%, #f00 100%);\n              background: -webkit-linear-gradient(to right, #f00 0%, #ff0\n                17%, #0f0 33%, #0ff 50%, #00f 67%, #f0f 83%, #f00 100%);\n            }\n\n            .hue-vertical {\n              background: linear-gradient(to top, #f00 0%, #ff0 17%, #0f0 33%,\n                #0ff 50%, #00f 67%, #f0f 83%, #f00 100%);\n              background: -webkit-linear-gradient(to top, #f00 0%, #ff0 17%,\n                #0f0 33%, #0ff 50%, #00f 67%, #f0f 83%, #f00 100%);\n            }\n          '
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.pointer },
	            this.props.pointer ? React__default.createElement(this.props.pointer, this.props) : React__default.createElement('div', { style: styles.slider })
	          )
	        )
	      );
	    }
	  }]);
	  return Hue;
	}(React.PureComponent || React.Component);

	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 * 
	 */

	function makeEmptyFunction(arg) {
	  return function () {
	    return arg;
	  };
	}

	/**
	 * This function accepts and discards inputs; it has no side effects. This is
	 * primarily useful idiomatically for overridable function endpoints which
	 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
	 */
	var emptyFunction = function emptyFunction() {};

	emptyFunction.thatReturns = makeEmptyFunction;
	emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
	emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
	emptyFunction.thatReturnsNull = makeEmptyFunction(null);
	emptyFunction.thatReturnsThis = function () {
	  return this;
	};
	emptyFunction.thatReturnsArgument = function (arg) {
	  return arg;
	};

	var emptyFunction_1 = emptyFunction;

	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 *
	 */

	/**
	 * Use invariant() to assert state which your program assumes to be true.
	 *
	 * Provide sprintf-style format (only %s is supported) and arguments
	 * to provide information about what broke and what you were
	 * expecting.
	 *
	 * The invariant message will be stripped in production, but the invariant
	 * will remain to ensure logic does not differ in production.
	 */

	var validateFormat = function validateFormat(format) {};

	{
	  validateFormat = function validateFormat(format) {
	    if (format === undefined) {
	      throw new Error('invariant requires an error message argument');
	    }
	  };
	}

	function invariant(condition, format, a, b, c, d, e, f) {
	  validateFormat(format);

	  if (!condition) {
	    var error;
	    if (format === undefined) {
	      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
	    } else {
	      var args = [a, b, c, d, e, f];
	      var argIndex = 0;
	      error = new Error(format.replace(/%s/g, function () {
	        return args[argIndex++];
	      }));
	      error.name = 'Invariant Violation';
	    }

	    error.framesToPop = 1; // we don't care about invariant's own frame
	    throw error;
	  }
	}

	var invariant_1 = invariant;

	/**
	 * Similar to invariant but only logs a warning if the condition is not met.
	 * This can be used to log issues in development environments in critical
	 * paths. Removing the logging code for production environments will keep the
	 * same logic and follow the same code paths.
	 */

	var warning = emptyFunction_1;

	{
	  var printWarning = function printWarning(format) {
	    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	      args[_key - 1] = arguments[_key];
	    }

	    var argIndex = 0;
	    var message = 'Warning: ' + format.replace(/%s/g, function () {
	      return args[argIndex++];
	    });
	    if (typeof console !== 'undefined') {
	      console.error(message);
	    }
	    try {
	      // --- Welcome to debugging React ---
	      // This error was thrown as a convenience so that you can use this stack
	      // to find the callsite that caused this warning to fire.
	      throw new Error(message);
	    } catch (x) {}
	  };

	  warning = function warning(condition, format) {
	    if (format === undefined) {
	      throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
	    }

	    if (format.indexOf('Failed Composite propType: ') === 0) {
	      return; // Ignore CompositeComponent proptype check.
	    }

	    if (!condition) {
	      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
	        args[_key2 - 2] = arguments[_key2];
	      }

	      printWarning.apply(undefined, [format].concat(args));
	    }
	  };
	}

	var warning_1 = warning;

	/*
	object-assign
	(c) Sindre Sorhus
	@license MIT
	*/
	/* eslint-disable no-unused-vars */
	var getOwnPropertySymbols = Object.getOwnPropertySymbols;
	var hasOwnProperty$13 = Object.prototype.hasOwnProperty;
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;

	function toObject(val) {
		if (val === null || val === undefined) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}

		return Object(val);
	}

	function shouldUseNative() {
		try {
			if (!Object.assign) {
				return false;
			}

			// Detect buggy property enumeration order in older V8 versions.

			// https://bugs.chromium.org/p/v8/issues/detail?id=4118
			var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
			test1[5] = 'de';
			if (Object.getOwnPropertyNames(test1)[0] === '5') {
				return false;
			}

			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test2 = {};
			for (var i = 0; i < 10; i++) {
				test2['_' + String.fromCharCode(i)] = i;
			}
			var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
				return test2[n];
			});
			if (order2.join('') !== '0123456789') {
				return false;
			}

			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test3 = {};
			'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
				test3[letter] = letter;
			});
			if (Object.keys(Object.assign({}, test3)).join('') !==
					'abcdefghijklmnopqrst') {
				return false;
			}

			return true;
		} catch (err) {
			// We don't expect any of the above to throw, but better to be safe.
			return false;
		}
	}

	var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
		var from;
		var to = toObject(target);
		var symbols;

		for (var s = 1; s < arguments.length; s++) {
			from = Object(arguments[s]);

			for (var key in from) {
				if (hasOwnProperty$13.call(from, key)) {
					to[key] = from[key];
				}
			}

			if (getOwnPropertySymbols) {
				symbols = getOwnPropertySymbols(from);
				for (var i = 0; i < symbols.length; i++) {
					if (propIsEnumerable.call(from, symbols[i])) {
						to[symbols[i]] = from[symbols[i]];
					}
				}
			}
		}

		return to;
	};

	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

	var ReactPropTypesSecret_1 = ReactPropTypesSecret;

	{
	  var invariant$1 = invariant_1;
	  var warning$1 = warning_1;
	  var ReactPropTypesSecret$1 = ReactPropTypesSecret_1;
	  var loggedTypeFailures = {};
	}

	/**
	 * Assert that the values match with the type specs.
	 * Error messages are memorized and will only be shown once.
	 *
	 * @param {object} typeSpecs Map of name to a ReactPropType
	 * @param {object} values Runtime values that need to be type-checked
	 * @param {string} location e.g. "prop", "context", "child context"
	 * @param {string} componentName Name of the component for error messages.
	 * @param {?Function} getStack Returns the component stack.
	 * @private
	 */
	function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
	  {
	    for (var typeSpecName in typeSpecs) {
	      if (typeSpecs.hasOwnProperty(typeSpecName)) {
	        var error;
	        // Prop type validation may throw. In case they do, we don't want to
	        // fail the render phase where it didn't fail before. So we log it.
	        // After these have been cleaned up, we'll let them throw.
	        try {
	          // This is intentionally an invariant that gets caught. It's the same
	          // behavior as without this statement except with a better message.
	          invariant$1(typeof typeSpecs[typeSpecName] === 'function', '%s: %s type `%s` is invalid; it must be a function, usually from ' + 'the `prop-types` package, but received `%s`.', componentName || 'React class', location, typeSpecName, typeof typeSpecs[typeSpecName]);
	          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret$1);
	        } catch (ex) {
	          error = ex;
	        }
	        warning$1(!error || error instanceof Error, '%s: type specification of %s `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error);
	        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
	          // Only monitor this failure once because there tends to be a lot of the
	          // same error.
	          loggedTypeFailures[error.message] = true;

	          var stack = getStack ? getStack() : '';

	          warning$1(false, 'Failed %s type: %s%s', location, error.message, stack != null ? stack : '');
	        }
	      }
	    }
	  }
	}

	var checkPropTypes_1 = checkPropTypes;

	var factoryWithTypeCheckers = function(isValidElement, throwOnDirectAccess) {
	  /* global Symbol */
	  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
	  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

	  /**
	   * Returns the iterator method function contained on the iterable object.
	   *
	   * Be sure to invoke the function with the iterable as context:
	   *
	   *     var iteratorFn = getIteratorFn(myIterable);
	   *     if (iteratorFn) {
	   *       var iterator = iteratorFn.call(myIterable);
	   *       ...
	   *     }
	   *
	   * @param {?object} maybeIterable
	   * @return {?function}
	   */
	  function getIteratorFn(maybeIterable) {
	    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
	    if (typeof iteratorFn === 'function') {
	      return iteratorFn;
	    }
	  }

	  /**
	   * Collection of methods that allow declaration and validation of props that are
	   * supplied to React components. Example usage:
	   *
	   *   var Props = require('ReactPropTypes');
	   *   var MyArticle = React.createClass({
	   *     propTypes: {
	   *       // An optional string prop named "description".
	   *       description: Props.string,
	   *
	   *       // A required enum prop named "category".
	   *       category: Props.oneOf(['News','Photos']).isRequired,
	   *
	   *       // A prop named "dialog" that requires an instance of Dialog.
	   *       dialog: Props.instanceOf(Dialog).isRequired
	   *     },
	   *     render: function() { ... }
	   *   });
	   *
	   * A more formal specification of how these methods are used:
	   *
	   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
	   *   decl := ReactPropTypes.{type}(.isRequired)?
	   *
	   * Each and every declaration produces a function with the same signature. This
	   * allows the creation of custom validation functions. For example:
	   *
	   *  var MyLink = React.createClass({
	   *    propTypes: {
	   *      // An optional string or URI prop named "href".
	   *      href: function(props, propName, componentName) {
	   *        var propValue = props[propName];
	   *        if (propValue != null && typeof propValue !== 'string' &&
	   *            !(propValue instanceof URI)) {
	   *          return new Error(
	   *            'Expected a string or an URI for ' + propName + ' in ' +
	   *            componentName
	   *          );
	   *        }
	   *      }
	   *    },
	   *    render: function() {...}
	   *  });
	   *
	   * @internal
	   */

	  var ANONYMOUS = '<<anonymous>>';

	  // Important!
	  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
	  var ReactPropTypes = {
	    array: createPrimitiveTypeChecker('array'),
	    bool: createPrimitiveTypeChecker('boolean'),
	    func: createPrimitiveTypeChecker('function'),
	    number: createPrimitiveTypeChecker('number'),
	    object: createPrimitiveTypeChecker('object'),
	    string: createPrimitiveTypeChecker('string'),
	    symbol: createPrimitiveTypeChecker('symbol'),

	    any: createAnyTypeChecker(),
	    arrayOf: createArrayOfTypeChecker,
	    element: createElementTypeChecker(),
	    instanceOf: createInstanceTypeChecker,
	    node: createNodeChecker(),
	    objectOf: createObjectOfTypeChecker,
	    oneOf: createEnumTypeChecker,
	    oneOfType: createUnionTypeChecker,
	    shape: createShapeTypeChecker,
	    exact: createStrictShapeTypeChecker,
	  };

	  /**
	   * inlined Object.is polyfill to avoid requiring consumers ship their own
	   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
	   */
	  /*eslint-disable no-self-compare*/
	  function is(x, y) {
	    // SameValue algorithm
	    if (x === y) {
	      // Steps 1-5, 7-10
	      // Steps 6.b-6.e: +0 != -0
	      return x !== 0 || 1 / x === 1 / y;
	    } else {
	      // Step 6.a: NaN == NaN
	      return x !== x && y !== y;
	    }
	  }
	  /*eslint-enable no-self-compare*/

	  /**
	   * We use an Error-like object for backward compatibility as people may call
	   * PropTypes directly and inspect their output. However, we don't use real
	   * Errors anymore. We don't inspect their stack anyway, and creating them
	   * is prohibitively expensive if they are created too often, such as what
	   * happens in oneOfType() for any type before the one that matched.
	   */
	  function PropTypeError(message) {
	    this.message = message;
	    this.stack = '';
	  }
	  // Make `instanceof Error` still work for returned errors.
	  PropTypeError.prototype = Error.prototype;

	  function createChainableTypeChecker(validate) {
	    {
	      var manualPropTypeCallCache = {};
	      var manualPropTypeWarningCount = 0;
	    }
	    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
	      componentName = componentName || ANONYMOUS;
	      propFullName = propFullName || propName;

	      if (secret !== ReactPropTypesSecret_1) {
	        if (throwOnDirectAccess) {
	          // New behavior only for users of `prop-types` package
	          invariant_1(
	            false,
	            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
	            'Use `PropTypes.checkPropTypes()` to call them. ' +
	            'Read more at http://fb.me/use-check-prop-types'
	          );
	        } else if (typeof console !== 'undefined') {
	          // Old behavior for people using React.PropTypes
	          var cacheKey = componentName + ':' + propName;
	          if (
	            !manualPropTypeCallCache[cacheKey] &&
	            // Avoid spamming the console because they are often not actionable except for lib authors
	            manualPropTypeWarningCount < 3
	          ) {
	            warning_1(
	              false,
	              'You are manually calling a React.PropTypes validation ' +
	              'function for the `%s` prop on `%s`. This is deprecated ' +
	              'and will throw in the standalone `prop-types` package. ' +
	              'You may be seeing this warning due to a third-party PropTypes ' +
	              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.',
	              propFullName,
	              componentName
	            );
	            manualPropTypeCallCache[cacheKey] = true;
	            manualPropTypeWarningCount++;
	          }
	        }
	      }
	      if (props[propName] == null) {
	        if (isRequired) {
	          if (props[propName] === null) {
	            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
	          }
	          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
	        }
	        return null;
	      } else {
	        return validate(props, propName, componentName, location, propFullName);
	      }
	    }

	    var chainedCheckType = checkType.bind(null, false);
	    chainedCheckType.isRequired = checkType.bind(null, true);

	    return chainedCheckType;
	  }

	  function createPrimitiveTypeChecker(expectedType) {
	    function validate(props, propName, componentName, location, propFullName, secret) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== expectedType) {
	        // `propValue` being instance of, say, date/regexp, pass the 'object'
	        // check, but we can offer a more precise error message here rather than
	        // 'of type `object`'.
	        var preciseType = getPreciseType(propValue);

	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createAnyTypeChecker() {
	    return createChainableTypeChecker(emptyFunction_1.thatReturnsNull);
	  }

	  function createArrayOfTypeChecker(typeChecker) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (typeof typeChecker !== 'function') {
	        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
	      }
	      var propValue = props[propName];
	      if (!Array.isArray(propValue)) {
	        var propType = getPropType(propValue);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
	      }
	      for (var i = 0; i < propValue.length; i++) {
	        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);
	        if (error instanceof Error) {
	          return error;
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createElementTypeChecker() {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      if (!isValidElement(propValue)) {
	        var propType = getPropType(propValue);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createInstanceTypeChecker(expectedClass) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (!(props[propName] instanceof expectedClass)) {
	        var expectedClassName = expectedClass.name || ANONYMOUS;
	        var actualClassName = getClassName(props[propName]);
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createEnumTypeChecker(expectedValues) {
	    if (!Array.isArray(expectedValues)) {
	      warning_1(false, 'Invalid argument supplied to oneOf, expected an instance of array.');
	      return emptyFunction_1.thatReturnsNull;
	    }

	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      for (var i = 0; i < expectedValues.length; i++) {
	        if (is(propValue, expectedValues[i])) {
	          return null;
	        }
	      }

	      var valuesString = JSON.stringify(expectedValues);
	      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createObjectOfTypeChecker(typeChecker) {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (typeof typeChecker !== 'function') {
	        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
	      }
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
	      }
	      for (var key in propValue) {
	        if (propValue.hasOwnProperty(key)) {
	          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
	          if (error instanceof Error) {
	            return error;
	          }
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createUnionTypeChecker(arrayOfTypeCheckers) {
	    if (!Array.isArray(arrayOfTypeCheckers)) {
	      warning_1(false, 'Invalid argument supplied to oneOfType, expected an instance of array.');
	      return emptyFunction_1.thatReturnsNull;
	    }

	    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
	      var checker = arrayOfTypeCheckers[i];
	      if (typeof checker !== 'function') {
	        warning_1(
	          false,
	          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
	          'received %s at index %s.',
	          getPostfixForTypeWarning(checker),
	          i
	        );
	        return emptyFunction_1.thatReturnsNull;
	      }
	    }

	    function validate(props, propName, componentName, location, propFullName) {
	      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
	        var checker = arrayOfTypeCheckers[i];
	        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1) == null) {
	          return null;
	        }
	      }

	      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createNodeChecker() {
	    function validate(props, propName, componentName, location, propFullName) {
	      if (!isNode(props[propName])) {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createShapeTypeChecker(shapeTypes) {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
	      }
	      for (var key in shapeTypes) {
	        var checker = shapeTypes[key];
	        if (!checker) {
	          continue;
	        }
	        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
	        if (error) {
	          return error;
	        }
	      }
	      return null;
	    }
	    return createChainableTypeChecker(validate);
	  }

	  function createStrictShapeTypeChecker(shapeTypes) {
	    function validate(props, propName, componentName, location, propFullName) {
	      var propValue = props[propName];
	      var propType = getPropType(propValue);
	      if (propType !== 'object') {
	        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
	      }
	      // We need to check all keys in case some are required but missing from
	      // props.
	      var allKeys = objectAssign({}, props[propName], shapeTypes);
	      for (var key in allKeys) {
	        var checker = shapeTypes[key];
	        if (!checker) {
	          return new PropTypeError(
	            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
	            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
	            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
	          );
	        }
	        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
	        if (error) {
	          return error;
	        }
	      }
	      return null;
	    }

	    return createChainableTypeChecker(validate);
	  }

	  function isNode(propValue) {
	    switch (typeof propValue) {
	      case 'number':
	      case 'string':
	      case 'undefined':
	        return true;
	      case 'boolean':
	        return !propValue;
	      case 'object':
	        if (Array.isArray(propValue)) {
	          return propValue.every(isNode);
	        }
	        if (propValue === null || isValidElement(propValue)) {
	          return true;
	        }

	        var iteratorFn = getIteratorFn(propValue);
	        if (iteratorFn) {
	          var iterator = iteratorFn.call(propValue);
	          var step;
	          if (iteratorFn !== propValue.entries) {
	            while (!(step = iterator.next()).done) {
	              if (!isNode(step.value)) {
	                return false;
	              }
	            }
	          } else {
	            // Iterator will provide entry [k,v] tuples rather than values.
	            while (!(step = iterator.next()).done) {
	              var entry = step.value;
	              if (entry) {
	                if (!isNode(entry[1])) {
	                  return false;
	                }
	              }
	            }
	          }
	        } else {
	          return false;
	        }

	        return true;
	      default:
	        return false;
	    }
	  }

	  function isSymbol(propType, propValue) {
	    // Native Symbol.
	    if (propType === 'symbol') {
	      return true;
	    }

	    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
	    if (propValue['@@toStringTag'] === 'Symbol') {
	      return true;
	    }

	    // Fallback for non-spec compliant Symbols which are polyfilled.
	    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
	      return true;
	    }

	    return false;
	  }

	  // Equivalent of `typeof` but with special handling for array and regexp.
	  function getPropType(propValue) {
	    var propType = typeof propValue;
	    if (Array.isArray(propValue)) {
	      return 'array';
	    }
	    if (propValue instanceof RegExp) {
	      // Old webkits (at least until Android 4.0) return 'function' rather than
	      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
	      // passes PropTypes.object.
	      return 'object';
	    }
	    if (isSymbol(propType, propValue)) {
	      return 'symbol';
	    }
	    return propType;
	  }

	  // This handles more types than `getPropType`. Only used for error messages.
	  // See `createPrimitiveTypeChecker`.
	  function getPreciseType(propValue) {
	    if (typeof propValue === 'undefined' || propValue === null) {
	      return '' + propValue;
	    }
	    var propType = getPropType(propValue);
	    if (propType === 'object') {
	      if (propValue instanceof Date) {
	        return 'date';
	      } else if (propValue instanceof RegExp) {
	        return 'regexp';
	      }
	    }
	    return propType;
	  }

	  // Returns a string that is postfixed to a warning about an invalid type.
	  // For example, "undefined" or "of type array"
	  function getPostfixForTypeWarning(value) {
	    var type = getPreciseType(value);
	    switch (type) {
	      case 'array':
	      case 'object':
	        return 'an ' + type;
	      case 'boolean':
	      case 'date':
	      case 'regexp':
	        return 'a ' + type;
	      default:
	        return type;
	    }
	  }

	  // Returns class name of the object, if any.
	  function getClassName(propValue) {
	    if (!propValue.constructor || !propValue.constructor.name) {
	      return ANONYMOUS;
	    }
	    return propValue.constructor.name;
	  }

	  ReactPropTypes.checkPropTypes = checkPropTypes_1;
	  ReactPropTypes.PropTypes = ReactPropTypes;

	  return ReactPropTypes;
	};

	var propTypes = createCommonjsModule(function (module) {
	/**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */

	{
	  var REACT_ELEMENT_TYPE = (typeof Symbol === 'function' &&
	    Symbol.for &&
	    Symbol.for('react.element')) ||
	    0xeac7;

	  var isValidElement = function(object) {
	    return typeof object === 'object' &&
	      object !== null &&
	      object.$$typeof === REACT_ELEMENT_TYPE;
	  };

	  // By explicitly using `prop-types` you are opting into new development behavior.
	  // http://fb.me/prop-types-in-prod
	  var throwOnDirectAccess = true;
	  module.exports = factoryWithTypeCheckers(isValidElement, throwOnDirectAccess);
	}
	});

	var Raised = function Raised(_ref) {
	  var zDepth = _ref.zDepth,
	      radius = _ref.radius,
	      background = _ref.background,
	      children = _ref.children;

	  var styles = reactCSS({
	    'default': {
	      wrap: {
	        position: 'relative',
	        display: 'inline-block'
	      },
	      content: {
	        position: 'relative'
	      },
	      bg: {
	        absolute: '0px 0px 0px 0px',
	        boxShadow: '0 ' + zDepth + 'px ' + zDepth * 4 + 'px rgba(0,0,0,.24)',
	        borderRadius: radius,
	        background: background
	      }
	    },
	    'zDepth-0': {
	      bg: {
	        boxShadow: 'none'
	      }
	    },

	    'zDepth-1': {
	      bg: {
	        boxShadow: '0 2px 10px rgba(0,0,0,.12), 0 2px 5px rgba(0,0,0,.16)'
	      }
	    },
	    'zDepth-2': {
	      bg: {
	        boxShadow: '0 6px 20px rgba(0,0,0,.19), 0 8px 17px rgba(0,0,0,.2)'
	      }
	    },
	    'zDepth-3': {
	      bg: {
	        boxShadow: '0 17px 50px rgba(0,0,0,.19), 0 12px 15px rgba(0,0,0,.24)'
	      }
	    },
	    'zDepth-4': {
	      bg: {
	        boxShadow: '0 25px 55px rgba(0,0,0,.21), 0 16px 28px rgba(0,0,0,.22)'
	      }
	    },
	    'zDepth-5': {
	      bg: {
	        boxShadow: '0 40px 77px rgba(0,0,0,.22), 0 27px 24px rgba(0,0,0,.2)'
	      }
	    },
	    'square': {
	      bg: {
	        borderRadius: '0'
	      }
	    },
	    'circle': {
	      bg: {
	        borderRadius: '50%'
	      }
	    }
	  }, { 'zDepth-1': zDepth === 1 });

	  return React__default.createElement(
	    'div',
	    { style: styles.wrap },
	    React__default.createElement('div', { style: styles.bg }),
	    React__default.createElement(
	      'div',
	      { style: styles.content },
	      children
	    )
	  );
	};

	Raised.propTypes = {
	  background: propTypes.string,
	  zDepth: propTypes.oneOf([0, 1, 2, 3, 4, 5]),
	  radius: propTypes.number
	};

	Raised.defaultProps = {
	  background: '#fff',
	  zDepth: 1,
	  radius: 2
	};

	/**
	 * Gets the timestamp of the number of milliseconds that have elapsed since
	 * the Unix epoch (1 January 1970 00:00:00 UTC).
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Date
	 * @returns {number} Returns the timestamp.
	 * @example
	 *
	 * _.defer(function(stamp) {
	 *   console.log(_.now() - stamp);
	 * }, _.now());
	 * // => Logs the number of milliseconds it took for the deferred invocation.
	 */
	var now = function() {
	  return _root.Date.now();
	};

	var now_1 = now;

	/** Used as references for various `Number` constants. */
	var NAN = 0 / 0;

	/** Used to match leading and trailing whitespace. */
	var reTrim = /^\s+|\s+$/g;

	/** Used to detect bad signed hexadecimal string values. */
	var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

	/** Used to detect binary string values. */
	var reIsBinary = /^0b[01]+$/i;

	/** Used to detect octal string values. */
	var reIsOctal = /^0o[0-7]+$/i;

	/** Built-in method references without a dependency on `root`. */
	var freeParseInt = parseInt;

	/**
	 * Converts `value` to a number.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to process.
	 * @returns {number} Returns the number.
	 * @example
	 *
	 * _.toNumber(3.2);
	 * // => 3.2
	 *
	 * _.toNumber(Number.MIN_VALUE);
	 * // => 5e-324
	 *
	 * _.toNumber(Infinity);
	 * // => Infinity
	 *
	 * _.toNumber('3.2');
	 * // => 3.2
	 */
	function toNumber(value) {
	  if (typeof value == 'number') {
	    return value;
	  }
	  if (isSymbol_1(value)) {
	    return NAN;
	  }
	  if (isObject_1(value)) {
	    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
	    value = isObject_1(other) ? (other + '') : other;
	  }
	  if (typeof value != 'string') {
	    return value === 0 ? value : +value;
	  }
	  value = value.replace(reTrim, '');
	  var isBinary = reIsBinary.test(value);
	  return (isBinary || reIsOctal.test(value))
	    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
	    : (reIsBadHex.test(value) ? NAN : +value);
	}

	var toNumber_1 = toNumber;

	/** Error message constants. */
	var FUNC_ERROR_TEXT$1 = 'Expected a function';

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeMax = Math.max,
	    nativeMin = Math.min;

	/**
	 * Creates a debounced function that delays invoking `func` until after `wait`
	 * milliseconds have elapsed since the last time the debounced function was
	 * invoked. The debounced function comes with a `cancel` method to cancel
	 * delayed `func` invocations and a `flush` method to immediately invoke them.
	 * Provide `options` to indicate whether `func` should be invoked on the
	 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
	 * with the last arguments provided to the debounced function. Subsequent
	 * calls to the debounced function return the result of the last `func`
	 * invocation.
	 *
	 * **Note:** If `leading` and `trailing` options are `true`, `func` is
	 * invoked on the trailing edge of the timeout only if the debounced function
	 * is invoked more than once during the `wait` timeout.
	 *
	 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
	 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
	 *
	 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
	 * for details over the differences between `_.debounce` and `_.throttle`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to debounce.
	 * @param {number} [wait=0] The number of milliseconds to delay.
	 * @param {Object} [options={}] The options object.
	 * @param {boolean} [options.leading=false]
	 *  Specify invoking on the leading edge of the timeout.
	 * @param {number} [options.maxWait]
	 *  The maximum time `func` is allowed to be delayed before it's invoked.
	 * @param {boolean} [options.trailing=true]
	 *  Specify invoking on the trailing edge of the timeout.
	 * @returns {Function} Returns the new debounced function.
	 * @example
	 *
	 * // Avoid costly calculations while the window size is in flux.
	 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
	 *
	 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
	 * jQuery(element).on('click', _.debounce(sendMail, 300, {
	 *   'leading': true,
	 *   'trailing': false
	 * }));
	 *
	 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
	 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
	 * var source = new EventSource('/stream');
	 * jQuery(source).on('message', debounced);
	 *
	 * // Cancel the trailing debounced invocation.
	 * jQuery(window).on('popstate', debounced.cancel);
	 */
	function debounce(func, wait, options) {
	  var lastArgs,
	      lastThis,
	      maxWait,
	      result,
	      timerId,
	      lastCallTime,
	      lastInvokeTime = 0,
	      leading = false,
	      maxing = false,
	      trailing = true;

	  if (typeof func != 'function') {
	    throw new TypeError(FUNC_ERROR_TEXT$1);
	  }
	  wait = toNumber_1(wait) || 0;
	  if (isObject_1(options)) {
	    leading = !!options.leading;
	    maxing = 'maxWait' in options;
	    maxWait = maxing ? nativeMax(toNumber_1(options.maxWait) || 0, wait) : maxWait;
	    trailing = 'trailing' in options ? !!options.trailing : trailing;
	  }

	  function invokeFunc(time) {
	    var args = lastArgs,
	        thisArg = lastThis;

	    lastArgs = lastThis = undefined;
	    lastInvokeTime = time;
	    result = func.apply(thisArg, args);
	    return result;
	  }

	  function leadingEdge(time) {
	    // Reset any `maxWait` timer.
	    lastInvokeTime = time;
	    // Start the timer for the trailing edge.
	    timerId = setTimeout(timerExpired, wait);
	    // Invoke the leading edge.
	    return leading ? invokeFunc(time) : result;
	  }

	  function remainingWait(time) {
	    var timeSinceLastCall = time - lastCallTime,
	        timeSinceLastInvoke = time - lastInvokeTime,
	        timeWaiting = wait - timeSinceLastCall;

	    return maxing
	      ? nativeMin(timeWaiting, maxWait - timeSinceLastInvoke)
	      : timeWaiting;
	  }

	  function shouldInvoke(time) {
	    var timeSinceLastCall = time - lastCallTime,
	        timeSinceLastInvoke = time - lastInvokeTime;

	    // Either this is the first call, activity has stopped and we're at the
	    // trailing edge, the system time has gone backwards and we're treating
	    // it as the trailing edge, or we've hit the `maxWait` limit.
	    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
	      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
	  }

	  function timerExpired() {
	    var time = now_1();
	    if (shouldInvoke(time)) {
	      return trailingEdge(time);
	    }
	    // Restart the timer.
	    timerId = setTimeout(timerExpired, remainingWait(time));
	  }

	  function trailingEdge(time) {
	    timerId = undefined;

	    // Only invoke if we have `lastArgs` which means `func` has been
	    // debounced at least once.
	    if (trailing && lastArgs) {
	      return invokeFunc(time);
	    }
	    lastArgs = lastThis = undefined;
	    return result;
	  }

	  function cancel() {
	    if (timerId !== undefined) {
	      clearTimeout(timerId);
	    }
	    lastInvokeTime = 0;
	    lastArgs = lastCallTime = lastThis = timerId = undefined;
	  }

	  function flush() {
	    return timerId === undefined ? result : trailingEdge(now_1());
	  }

	  function debounced() {
	    var time = now_1(),
	        isInvoking = shouldInvoke(time);

	    lastArgs = arguments;
	    lastThis = this;
	    lastCallTime = time;

	    if (isInvoking) {
	      if (timerId === undefined) {
	        return leadingEdge(lastCallTime);
	      }
	      if (maxing) {
	        // Handle invocations in a tight loop.
	        timerId = setTimeout(timerExpired, wait);
	        return invokeFunc(lastCallTime);
	      }
	    }
	    if (timerId === undefined) {
	      timerId = setTimeout(timerExpired, wait);
	    }
	    return result;
	  }
	  debounced.cancel = cancel;
	  debounced.flush = flush;
	  return debounced;
	}

	var debounce_1 = debounce;

	/** Error message constants. */
	var FUNC_ERROR_TEXT$2 = 'Expected a function';

	/**
	 * Creates a throttled function that only invokes `func` at most once per
	 * every `wait` milliseconds. The throttled function comes with a `cancel`
	 * method to cancel delayed `func` invocations and a `flush` method to
	 * immediately invoke them. Provide `options` to indicate whether `func`
	 * should be invoked on the leading and/or trailing edge of the `wait`
	 * timeout. The `func` is invoked with the last arguments provided to the
	 * throttled function. Subsequent calls to the throttled function return the
	 * result of the last `func` invocation.
	 *
	 * **Note:** If `leading` and `trailing` options are `true`, `func` is
	 * invoked on the trailing edge of the timeout only if the throttled function
	 * is invoked more than once during the `wait` timeout.
	 *
	 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
	 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
	 *
	 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
	 * for details over the differences between `_.throttle` and `_.debounce`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to throttle.
	 * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
	 * @param {Object} [options={}] The options object.
	 * @param {boolean} [options.leading=true]
	 *  Specify invoking on the leading edge of the timeout.
	 * @param {boolean} [options.trailing=true]
	 *  Specify invoking on the trailing edge of the timeout.
	 * @returns {Function} Returns the new throttled function.
	 * @example
	 *
	 * // Avoid excessively updating the position while scrolling.
	 * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
	 *
	 * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
	 * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
	 * jQuery(element).on('click', throttled);
	 *
	 * // Cancel the trailing throttled invocation.
	 * jQuery(window).on('popstate', throttled.cancel);
	 */
	function throttle(func, wait, options) {
	  var leading = true,
	      trailing = true;

	  if (typeof func != 'function') {
	    throw new TypeError(FUNC_ERROR_TEXT$2);
	  }
	  if (isObject_1(options)) {
	    leading = 'leading' in options ? !!options.leading : leading;
	    trailing = 'trailing' in options ? !!options.trailing : trailing;
	  }
	  return debounce_1(func, wait, {
	    'leading': leading,
	    'maxWait': wait,
	    'trailing': trailing
	  });
	}

	var throttle_1 = throttle;

	var calculateChange$2 = function calculateChange(e, skip, props, container) {
	  e.preventDefault();

	  var _container$getBoundin = container.getBoundingClientRect(),
	      containerWidth = _container$getBoundin.width,
	      containerHeight = _container$getBoundin.height;

	  var x = typeof e.pageX === 'number' ? e.pageX : e.touches[0].pageX;
	  var y = typeof e.pageY === 'number' ? e.pageY : e.touches[0].pageY;
	  var left = x - (container.getBoundingClientRect().left + window.pageXOffset);
	  var top = y - (container.getBoundingClientRect().top + window.pageYOffset);

	  if (left < 0) {
	    left = 0;
	  } else if (left > containerWidth) {
	    left = containerWidth;
	  } else if (top < 0) {
	    top = 0;
	  } else if (top > containerHeight) {
	    top = containerHeight;
	  }

	  var saturation = left * 100 / containerWidth;
	  var bright = -(top * 100 / containerHeight) + 100;

	  return {
	    h: props.hsl.h,
	    s: saturation,
	    v: bright,
	    a: props.hsl.a,
	    source: 'rgb'
	  };
	};

	var Saturation = function (_ref) {
	  inherits(Saturation, _ref);

	  function Saturation(props) {
	    classCallCheck(this, Saturation);

	    var _this = possibleConstructorReturn(this, (Saturation.__proto__ || Object.getPrototypeOf(Saturation)).call(this, props));

	    _this.handleChange = function (e, skip) {
	      _this.props.onChange && _this.throttle(_this.props.onChange, calculateChange$2(e, skip, _this.props, _this.container), e);
	    };

	    _this.handleMouseDown = function (e) {
	      _this.handleChange(e, true);
	      window.addEventListener('mousemove', _this.handleChange);
	      window.addEventListener('mouseup', _this.handleMouseUp);
	    };

	    _this.handleMouseUp = function () {
	      _this.unbindEventListeners();
	    };

	    _this.throttle = throttle_1(function (fn, data, e) {
	      fn(data, e);
	    }, 50);
	    return _this;
	  }

	  createClass(Saturation, [{
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      this.unbindEventListeners();
	    }
	  }, {
	    key: 'unbindEventListeners',
	    value: function unbindEventListeners() {
	      window.removeEventListener('mousemove', this.handleChange);
	      window.removeEventListener('mouseup', this.handleMouseUp);
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;

	      var _ref2 = this.props.style || {},
	          color = _ref2.color,
	          white = _ref2.white,
	          black = _ref2.black,
	          pointer = _ref2.pointer,
	          circle = _ref2.circle;

	      var styles = reactCSS({
	        'default': {
	          color: {
	            absolute: '0px 0px 0px 0px',
	            background: 'hsl(' + this.props.hsl.h + ',100%, 50%)',
	            borderRadius: this.props.radius
	          },
	          white: {
	            absolute: '0px 0px 0px 0px',
	            borderRadius: this.props.radius
	          },
	          black: {
	            absolute: '0px 0px 0px 0px',
	            boxShadow: this.props.shadow,
	            borderRadius: this.props.radius
	          },
	          pointer: {
	            position: 'absolute',
	            top: -(this.props.hsv.v * 100) + 100 + '%',
	            left: this.props.hsv.s * 100 + '%',
	            cursor: 'default'
	          },
	          circle: {
	            width: '4px',
	            height: '4px',
	            boxShadow: '0 0 0 1.5px #fff, inset 0 0 1px 1px rgba(0,0,0,.3),\n            0 0 1px 2px rgba(0,0,0,.4)',
	            borderRadius: '50%',
	            cursor: 'hand',
	            transform: 'translate(-2px, -2px)'
	          }
	        },
	        'custom': {
	          color: color,
	          white: white,
	          black: black,
	          pointer: pointer,
	          circle: circle
	        }
	      }, { 'custom': !!this.props.style });

	      return React__default.createElement(
	        'div',
	        {
	          style: styles.color,
	          ref: function ref(container) {
	            return _this2.container = container;
	          },
	          onMouseDown: this.handleMouseDown,
	          onTouchMove: this.handleChange,
	          onTouchStart: this.handleChange
	        },
	        React__default.createElement(
	          'style',
	          null,
	          '\n          .saturation-white {\n            background: -webkit-linear-gradient(to right, #fff, rgba(255,255,255,0));\n            background: linear-gradient(to right, #fff, rgba(255,255,255,0));\n          }\n          .saturation-black {\n            background: -webkit-linear-gradient(to top, #000, rgba(0,0,0,0));\n            background: linear-gradient(to top, #000, rgba(0,0,0,0));\n          }\n        '
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.white, className: 'saturation-white' },
	          React__default.createElement('div', { style: styles.black, className: 'saturation-black' }),
	          React__default.createElement(
	            'div',
	            { style: styles.pointer },
	            this.props.pointer ? React__default.createElement(this.props.pointer, this.props) : React__default.createElement('div', { style: styles.circle })
	          )
	        )
	      );
	    }
	  }]);
	  return Saturation;
	}(React.PureComponent || React.Component);

	/**
	 * Iterates over elements of `collection` and invokes `iteratee` for each element.
	 * The iteratee is invoked with three arguments: (value, index|key, collection).
	 * Iteratee functions may exit iteration early by explicitly returning `false`.
	 *
	 * **Note:** As with other "Collections" methods, objects with a "length"
	 * property are iterated like arrays. To avoid this behavior use `_.forIn`
	 * or `_.forOwn` for object iteration.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @alias each
	 * @category Collection
	 * @param {Array|Object} collection The collection to iterate over.
	 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
	 * @returns {Array|Object} Returns `collection`.
	 * @see _.forEachRight
	 * @example
	 *
	 * _.forEach([1, 2], function(value) {
	 *   console.log(value);
	 * });
	 * // => Logs `1` then `2`.
	 *
	 * _.forEach({ 'a': 1, 'b': 2 }, function(value, key) {
	 *   console.log(key);
	 * });
	 * // => Logs 'a' then 'b' (iteration order is not guaranteed).
	 */
	function forEach(collection, iteratee) {
	  var func = isArray_1(collection) ? _arrayEach : _baseEach;
	  return func(collection, _castFunction(iteratee));
	}

	var forEach_1 = forEach;

	var each = forEach_1;

	var tinycolor = createCommonjsModule(function (module) {
	// TinyColor v1.4.1
	// https://github.com/bgrins/TinyColor
	// Brian Grinstead, MIT License

	(function(Math) {

	var trimLeft = /^\s+/,
	    trimRight = /\s+$/,
	    tinyCounter = 0,
	    mathRound = Math.round,
	    mathMin = Math.min,
	    mathMax = Math.max,
	    mathRandom = Math.random;

	function tinycolor (color, opts) {

	    color = (color) ? color : '';
	    opts = opts || { };

	    // If input is already a tinycolor, return itself
	    if (color instanceof tinycolor) {
	       return color;
	    }
	    // If we are called as a function, call using new instead
	    if (!(this instanceof tinycolor)) {
	        return new tinycolor(color, opts);
	    }

	    var rgb = inputToRGB(color);
	    this._originalInput = color,
	    this._r = rgb.r,
	    this._g = rgb.g,
	    this._b = rgb.b,
	    this._a = rgb.a,
	    this._roundA = mathRound(100*this._a) / 100,
	    this._format = opts.format || rgb.format;
	    this._gradientType = opts.gradientType;

	    // Don't let the range of [0,255] come back in [0,1].
	    // Potentially lose a little bit of precision here, but will fix issues where
	    // .5 gets interpreted as half of the total, instead of half of 1
	    // If it was supposed to be 128, this was already taken care of by `inputToRgb`
	    if (this._r < 1) { this._r = mathRound(this._r); }
	    if (this._g < 1) { this._g = mathRound(this._g); }
	    if (this._b < 1) { this._b = mathRound(this._b); }

	    this._ok = rgb.ok;
	    this._tc_id = tinyCounter++;
	}

	tinycolor.prototype = {
	    isDark: function() {
	        return this.getBrightness() < 128;
	    },
	    isLight: function() {
	        return !this.isDark();
	    },
	    isValid: function() {
	        return this._ok;
	    },
	    getOriginalInput: function() {
	      return this._originalInput;
	    },
	    getFormat: function() {
	        return this._format;
	    },
	    getAlpha: function() {
	        return this._a;
	    },
	    getBrightness: function() {
	        //http://www.w3.org/TR/AERT#color-contrast
	        var rgb = this.toRgb();
	        return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
	    },
	    getLuminance: function() {
	        //http://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
	        var rgb = this.toRgb();
	        var RsRGB, GsRGB, BsRGB, R, G, B;
	        RsRGB = rgb.r/255;
	        GsRGB = rgb.g/255;
	        BsRGB = rgb.b/255;

	        if (RsRGB <= 0.03928) {R = RsRGB / 12.92;} else {R = Math.pow(((RsRGB + 0.055) / 1.055), 2.4);}
	        if (GsRGB <= 0.03928) {G = GsRGB / 12.92;} else {G = Math.pow(((GsRGB + 0.055) / 1.055), 2.4);}
	        if (BsRGB <= 0.03928) {B = BsRGB / 12.92;} else {B = Math.pow(((BsRGB + 0.055) / 1.055), 2.4);}
	        return (0.2126 * R) + (0.7152 * G) + (0.0722 * B);
	    },
	    setAlpha: function(value) {
	        this._a = boundAlpha(value);
	        this._roundA = mathRound(100*this._a) / 100;
	        return this;
	    },
	    toHsv: function() {
	        var hsv = rgbToHsv(this._r, this._g, this._b);
	        return { h: hsv.h * 360, s: hsv.s, v: hsv.v, a: this._a };
	    },
	    toHsvString: function() {
	        var hsv = rgbToHsv(this._r, this._g, this._b);
	        var h = mathRound(hsv.h * 360), s = mathRound(hsv.s * 100), v = mathRound(hsv.v * 100);
	        return (this._a == 1) ?
	          "hsv("  + h + ", " + s + "%, " + v + "%)" :
	          "hsva(" + h + ", " + s + "%, " + v + "%, "+ this._roundA + ")";
	    },
	    toHsl: function() {
	        var hsl = rgbToHsl(this._r, this._g, this._b);
	        return { h: hsl.h * 360, s: hsl.s, l: hsl.l, a: this._a };
	    },
	    toHslString: function() {
	        var hsl = rgbToHsl(this._r, this._g, this._b);
	        var h = mathRound(hsl.h * 360), s = mathRound(hsl.s * 100), l = mathRound(hsl.l * 100);
	        return (this._a == 1) ?
	          "hsl("  + h + ", " + s + "%, " + l + "%)" :
	          "hsla(" + h + ", " + s + "%, " + l + "%, "+ this._roundA + ")";
	    },
	    toHex: function(allow3Char) {
	        return rgbToHex(this._r, this._g, this._b, allow3Char);
	    },
	    toHexString: function(allow3Char) {
	        return '#' + this.toHex(allow3Char);
	    },
	    toHex8: function(allow4Char) {
	        return rgbaToHex(this._r, this._g, this._b, this._a, allow4Char);
	    },
	    toHex8String: function(allow4Char) {
	        return '#' + this.toHex8(allow4Char);
	    },
	    toRgb: function() {
	        return { r: mathRound(this._r), g: mathRound(this._g), b: mathRound(this._b), a: this._a };
	    },
	    toRgbString: function() {
	        return (this._a == 1) ?
	          "rgb("  + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ")" :
	          "rgba(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ", " + this._roundA + ")";
	    },
	    toPercentageRgb: function() {
	        return { r: mathRound(bound01(this._r, 255) * 100) + "%", g: mathRound(bound01(this._g, 255) * 100) + "%", b: mathRound(bound01(this._b, 255) * 100) + "%", a: this._a };
	    },
	    toPercentageRgbString: function() {
	        return (this._a == 1) ?
	          "rgb("  + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%)" :
	          "rgba(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%, " + this._roundA + ")";
	    },
	    toName: function() {
	        if (this._a === 0) {
	            return "transparent";
	        }

	        if (this._a < 1) {
	            return false;
	        }

	        return hexNames[rgbToHex(this._r, this._g, this._b, true)] || false;
	    },
	    toFilter: function(secondColor) {
	        var hex8String = '#' + rgbaToArgbHex(this._r, this._g, this._b, this._a);
	        var secondHex8String = hex8String;
	        var gradientType = this._gradientType ? "GradientType = 1, " : "";

	        if (secondColor) {
	            var s = tinycolor(secondColor);
	            secondHex8String = '#' + rgbaToArgbHex(s._r, s._g, s._b, s._a);
	        }

	        return "progid:DXImageTransform.Microsoft.gradient("+gradientType+"startColorstr="+hex8String+",endColorstr="+secondHex8String+")";
	    },
	    toString: function(format) {
	        var formatSet = !!format;
	        format = format || this._format;

	        var formattedString = false;
	        var hasAlpha = this._a < 1 && this._a >= 0;
	        var needsAlphaFormat = !formatSet && hasAlpha && (format === "hex" || format === "hex6" || format === "hex3" || format === "hex4" || format === "hex8" || format === "name");

	        if (needsAlphaFormat) {
	            // Special case for "transparent", all other non-alpha formats
	            // will return rgba when there is transparency.
	            if (format === "name" && this._a === 0) {
	                return this.toName();
	            }
	            return this.toRgbString();
	        }
	        if (format === "rgb") {
	            formattedString = this.toRgbString();
	        }
	        if (format === "prgb") {
	            formattedString = this.toPercentageRgbString();
	        }
	        if (format === "hex" || format === "hex6") {
	            formattedString = this.toHexString();
	        }
	        if (format === "hex3") {
	            formattedString = this.toHexString(true);
	        }
	        if (format === "hex4") {
	            formattedString = this.toHex8String(true);
	        }
	        if (format === "hex8") {
	            formattedString = this.toHex8String();
	        }
	        if (format === "name") {
	            formattedString = this.toName();
	        }
	        if (format === "hsl") {
	            formattedString = this.toHslString();
	        }
	        if (format === "hsv") {
	            formattedString = this.toHsvString();
	        }

	        return formattedString || this.toHexString();
	    },
	    clone: function() {
	        return tinycolor(this.toString());
	    },

	    _applyModification: function(fn, args) {
	        var color = fn.apply(null, [this].concat([].slice.call(args)));
	        this._r = color._r;
	        this._g = color._g;
	        this._b = color._b;
	        this.setAlpha(color._a);
	        return this;
	    },
	    lighten: function() {
	        return this._applyModification(lighten, arguments);
	    },
	    brighten: function() {
	        return this._applyModification(brighten, arguments);
	    },
	    darken: function() {
	        return this._applyModification(darken, arguments);
	    },
	    desaturate: function() {
	        return this._applyModification(desaturate, arguments);
	    },
	    saturate: function() {
	        return this._applyModification(saturate, arguments);
	    },
	    greyscale: function() {
	        return this._applyModification(greyscale, arguments);
	    },
	    spin: function() {
	        return this._applyModification(spin, arguments);
	    },

	    _applyCombination: function(fn, args) {
	        return fn.apply(null, [this].concat([].slice.call(args)));
	    },
	    analogous: function() {
	        return this._applyCombination(analogous, arguments);
	    },
	    complement: function() {
	        return this._applyCombination(complement, arguments);
	    },
	    monochromatic: function() {
	        return this._applyCombination(monochromatic, arguments);
	    },
	    splitcomplement: function() {
	        return this._applyCombination(splitcomplement, arguments);
	    },
	    triad: function() {
	        return this._applyCombination(triad, arguments);
	    },
	    tetrad: function() {
	        return this._applyCombination(tetrad, arguments);
	    }
	};

	// If input is an object, force 1 into "1.0" to handle ratios properly
	// String input requires "1.0" as input, so 1 will be treated as 1
	tinycolor.fromRatio = function(color, opts) {
	    if (typeof color == "object") {
	        var newColor = {};
	        for (var i in color) {
	            if (color.hasOwnProperty(i)) {
	                if (i === "a") {
	                    newColor[i] = color[i];
	                }
	                else {
	                    newColor[i] = convertToPercentage(color[i]);
	                }
	            }
	        }
	        color = newColor;
	    }

	    return tinycolor(color, opts);
	};

	// Given a string or object, convert that input to RGB
	// Possible string inputs:
	//
	//     "red"
	//     "#f00" or "f00"
	//     "#ff0000" or "ff0000"
	//     "#ff000000" or "ff000000"
	//     "rgb 255 0 0" or "rgb (255, 0, 0)"
	//     "rgb 1.0 0 0" or "rgb (1, 0, 0)"
	//     "rgba (255, 0, 0, 1)" or "rgba 255, 0, 0, 1"
	//     "rgba (1.0, 0, 0, 1)" or "rgba 1.0, 0, 0, 1"
	//     "hsl(0, 100%, 50%)" or "hsl 0 100% 50%"
	//     "hsla(0, 100%, 50%, 1)" or "hsla 0 100% 50%, 1"
	//     "hsv(0, 100%, 100%)" or "hsv 0 100% 100%"
	//
	function inputToRGB(color) {

	    var rgb = { r: 0, g: 0, b: 0 };
	    var a = 1;
	    var s = null;
	    var v = null;
	    var l = null;
	    var ok = false;
	    var format = false;

	    if (typeof color == "string") {
	        color = stringInputToObject(color);
	    }

	    if (typeof color == "object") {
	        if (isValidCSSUnit(color.r) && isValidCSSUnit(color.g) && isValidCSSUnit(color.b)) {
	            rgb = rgbToRgb(color.r, color.g, color.b);
	            ok = true;
	            format = String(color.r).substr(-1) === "%" ? "prgb" : "rgb";
	        }
	        else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.v)) {
	            s = convertToPercentage(color.s);
	            v = convertToPercentage(color.v);
	            rgb = hsvToRgb(color.h, s, v);
	            ok = true;
	            format = "hsv";
	        }
	        else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.l)) {
	            s = convertToPercentage(color.s);
	            l = convertToPercentage(color.l);
	            rgb = hslToRgb(color.h, s, l);
	            ok = true;
	            format = "hsl";
	        }

	        if (color.hasOwnProperty("a")) {
	            a = color.a;
	        }
	    }

	    a = boundAlpha(a);

	    return {
	        ok: ok,
	        format: color.format || format,
	        r: mathMin(255, mathMax(rgb.r, 0)),
	        g: mathMin(255, mathMax(rgb.g, 0)),
	        b: mathMin(255, mathMax(rgb.b, 0)),
	        a: a
	    };
	}


	// Conversion Functions
	// --------------------

	// `rgbToHsl`, `rgbToHsv`, `hslToRgb`, `hsvToRgb` modified from:
	// <http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript>

	// `rgbToRgb`
	// Handle bounds / percentage checking to conform to CSS color spec
	// <http://www.w3.org/TR/css3-color/>
	// *Assumes:* r, g, b in [0, 255] or [0, 1]
	// *Returns:* { r, g, b } in [0, 255]
	function rgbToRgb(r, g, b){
	    return {
	        r: bound01(r, 255) * 255,
	        g: bound01(g, 255) * 255,
	        b: bound01(b, 255) * 255
	    };
	}

	// `rgbToHsl`
	// Converts an RGB color value to HSL.
	// *Assumes:* r, g, and b are contained in [0, 255] or [0, 1]
	// *Returns:* { h, s, l } in [0,1]
	function rgbToHsl(r, g, b) {

	    r = bound01(r, 255);
	    g = bound01(g, 255);
	    b = bound01(b, 255);

	    var max = mathMax(r, g, b), min = mathMin(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min) {
	        h = s = 0; // achromatic
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max) {
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }

	        h /= 6;
	    }

	    return { h: h, s: s, l: l };
	}

	// `hslToRgb`
	// Converts an HSL color value to RGB.
	// *Assumes:* h is contained in [0, 1] or [0, 360] and s and l are contained [0, 1] or [0, 100]
	// *Returns:* { r, g, b } in the set [0, 255]
	function hslToRgb(h, s, l) {
	    var r, g, b;

	    h = bound01(h, 360);
	    s = bound01(s, 100);
	    l = bound01(l, 100);

	    function hue2rgb(p, q, t) {
	        if(t < 0) t += 1;
	        if(t > 1) t -= 1;
	        if(t < 1/6) return p + (q - p) * 6 * t;
	        if(t < 1/2) return q;
	        if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
	        return p;
	    }

	    if(s === 0) {
	        r = g = b = l; // achromatic
	    }
	    else {
	        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	        var p = 2 * l - q;
	        r = hue2rgb(p, q, h + 1/3);
	        g = hue2rgb(p, q, h);
	        b = hue2rgb(p, q, h - 1/3);
	    }

	    return { r: r * 255, g: g * 255, b: b * 255 };
	}

	// `rgbToHsv`
	// Converts an RGB color value to HSV
	// *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
	// *Returns:* { h, s, v } in [0,1]
	function rgbToHsv(r, g, b) {

	    r = bound01(r, 255);
	    g = bound01(g, 255);
	    b = bound01(b, 255);

	    var max = mathMax(r, g, b), min = mathMin(r, g, b);
	    var h, s, v = max;

	    var d = max - min;
	    s = max === 0 ? 0 : d / max;

	    if(max == min) {
	        h = 0; // achromatic
	    }
	    else {
	        switch(max) {
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return { h: h, s: s, v: v };
	}

	// `hsvToRgb`
	// Converts an HSV color value to RGB.
	// *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
	// *Returns:* { r, g, b } in the set [0, 255]
	 function hsvToRgb(h, s, v) {

	    h = bound01(h, 360) * 6;
	    s = bound01(s, 100);
	    v = bound01(v, 100);

	    var i = Math.floor(h),
	        f = h - i,
	        p = v * (1 - s),
	        q = v * (1 - f * s),
	        t = v * (1 - (1 - f) * s),
	        mod = i % 6,
	        r = [v, q, p, p, t, v][mod],
	        g = [t, v, v, q, p, p][mod],
	        b = [p, p, t, v, v, q][mod];

	    return { r: r * 255, g: g * 255, b: b * 255 };
	}

	// `rgbToHex`
	// Converts an RGB color to hex
	// Assumes r, g, and b are contained in the set [0, 255]
	// Returns a 3 or 6 character hex
	function rgbToHex(r, g, b, allow3Char) {

	    var hex = [
	        pad2(mathRound(r).toString(16)),
	        pad2(mathRound(g).toString(16)),
	        pad2(mathRound(b).toString(16))
	    ];

	    // Return a 3 character hex if possible
	    if (allow3Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1)) {
	        return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0);
	    }

	    return hex.join("");
	}

	// `rgbaToHex`
	// Converts an RGBA color plus alpha transparency to hex
	// Assumes r, g, b are contained in the set [0, 255] and
	// a in [0, 1]. Returns a 4 or 8 character rgba hex
	function rgbaToHex(r, g, b, a, allow4Char) {

	    var hex = [
	        pad2(mathRound(r).toString(16)),
	        pad2(mathRound(g).toString(16)),
	        pad2(mathRound(b).toString(16)),
	        pad2(convertDecimalToHex(a))
	    ];

	    // Return a 4 character hex if possible
	    if (allow4Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1) && hex[3].charAt(0) == hex[3].charAt(1)) {
	        return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0) + hex[3].charAt(0);
	    }

	    return hex.join("");
	}

	// `rgbaToArgbHex`
	// Converts an RGBA color to an ARGB Hex8 string
	// Rarely used, but required for "toFilter()"
	function rgbaToArgbHex(r, g, b, a) {

	    var hex = [
	        pad2(convertDecimalToHex(a)),
	        pad2(mathRound(r).toString(16)),
	        pad2(mathRound(g).toString(16)),
	        pad2(mathRound(b).toString(16))
	    ];

	    return hex.join("");
	}

	// `equals`
	// Can be called with any tinycolor input
	tinycolor.equals = function (color1, color2) {
	    if (!color1 || !color2) { return false; }
	    return tinycolor(color1).toRgbString() == tinycolor(color2).toRgbString();
	};

	tinycolor.random = function() {
	    return tinycolor.fromRatio({
	        r: mathRandom(),
	        g: mathRandom(),
	        b: mathRandom()
	    });
	};


	// Modification Functions
	// ----------------------
	// Thanks to less.js for some of the basics here
	// <https://github.com/cloudhead/less.js/blob/master/lib/less/functions.js>

	function desaturate(color, amount) {
	    amount = (amount === 0) ? 0 : (amount || 10);
	    var hsl = tinycolor(color).toHsl();
	    hsl.s -= amount / 100;
	    hsl.s = clamp01(hsl.s);
	    return tinycolor(hsl);
	}

	function saturate(color, amount) {
	    amount = (amount === 0) ? 0 : (amount || 10);
	    var hsl = tinycolor(color).toHsl();
	    hsl.s += amount / 100;
	    hsl.s = clamp01(hsl.s);
	    return tinycolor(hsl);
	}

	function greyscale(color) {
	    return tinycolor(color).desaturate(100);
	}

	function lighten (color, amount) {
	    amount = (amount === 0) ? 0 : (amount || 10);
	    var hsl = tinycolor(color).toHsl();
	    hsl.l += amount / 100;
	    hsl.l = clamp01(hsl.l);
	    return tinycolor(hsl);
	}

	function brighten(color, amount) {
	    amount = (amount === 0) ? 0 : (amount || 10);
	    var rgb = tinycolor(color).toRgb();
	    rgb.r = mathMax(0, mathMin(255, rgb.r - mathRound(255 * - (amount / 100))));
	    rgb.g = mathMax(0, mathMin(255, rgb.g - mathRound(255 * - (amount / 100))));
	    rgb.b = mathMax(0, mathMin(255, rgb.b - mathRound(255 * - (amount / 100))));
	    return tinycolor(rgb);
	}

	function darken (color, amount) {
	    amount = (amount === 0) ? 0 : (amount || 10);
	    var hsl = tinycolor(color).toHsl();
	    hsl.l -= amount / 100;
	    hsl.l = clamp01(hsl.l);
	    return tinycolor(hsl);
	}

	// Spin takes a positive or negative amount within [-360, 360] indicating the change of hue.
	// Values outside of this range will be wrapped into this range.
	function spin(color, amount) {
	    var hsl = tinycolor(color).toHsl();
	    var hue = (hsl.h + amount) % 360;
	    hsl.h = hue < 0 ? 360 + hue : hue;
	    return tinycolor(hsl);
	}

	// Combination Functions
	// ---------------------
	// Thanks to jQuery xColor for some of the ideas behind these
	// <https://github.com/infusion/jQuery-xcolor/blob/master/jquery.xcolor.js>

	function complement(color) {
	    var hsl = tinycolor(color).toHsl();
	    hsl.h = (hsl.h + 180) % 360;
	    return tinycolor(hsl);
	}

	function triad(color) {
	    var hsl = tinycolor(color).toHsl();
	    var h = hsl.h;
	    return [
	        tinycolor(color),
	        tinycolor({ h: (h + 120) % 360, s: hsl.s, l: hsl.l }),
	        tinycolor({ h: (h + 240) % 360, s: hsl.s, l: hsl.l })
	    ];
	}

	function tetrad(color) {
	    var hsl = tinycolor(color).toHsl();
	    var h = hsl.h;
	    return [
	        tinycolor(color),
	        tinycolor({ h: (h + 90) % 360, s: hsl.s, l: hsl.l }),
	        tinycolor({ h: (h + 180) % 360, s: hsl.s, l: hsl.l }),
	        tinycolor({ h: (h + 270) % 360, s: hsl.s, l: hsl.l })
	    ];
	}

	function splitcomplement(color) {
	    var hsl = tinycolor(color).toHsl();
	    var h = hsl.h;
	    return [
	        tinycolor(color),
	        tinycolor({ h: (h + 72) % 360, s: hsl.s, l: hsl.l}),
	        tinycolor({ h: (h + 216) % 360, s: hsl.s, l: hsl.l})
	    ];
	}

	function analogous(color, results, slices) {
	    results = results || 6;
	    slices = slices || 30;

	    var hsl = tinycolor(color).toHsl();
	    var part = 360 / slices;
	    var ret = [tinycolor(color)];

	    for (hsl.h = ((hsl.h - (part * results >> 1)) + 720) % 360; --results; ) {
	        hsl.h = (hsl.h + part) % 360;
	        ret.push(tinycolor(hsl));
	    }
	    return ret;
	}

	function monochromatic(color, results) {
	    results = results || 6;
	    var hsv = tinycolor(color).toHsv();
	    var h = hsv.h, s = hsv.s, v = hsv.v;
	    var ret = [];
	    var modification = 1 / results;

	    while (results--) {
	        ret.push(tinycolor({ h: h, s: s, v: v}));
	        v = (v + modification) % 1;
	    }

	    return ret;
	}

	// Utility Functions
	// ---------------------

	tinycolor.mix = function(color1, color2, amount) {
	    amount = (amount === 0) ? 0 : (amount || 50);

	    var rgb1 = tinycolor(color1).toRgb();
	    var rgb2 = tinycolor(color2).toRgb();

	    var p = amount / 100;

	    var rgba = {
	        r: ((rgb2.r - rgb1.r) * p) + rgb1.r,
	        g: ((rgb2.g - rgb1.g) * p) + rgb1.g,
	        b: ((rgb2.b - rgb1.b) * p) + rgb1.b,
	        a: ((rgb2.a - rgb1.a) * p) + rgb1.a
	    };

	    return tinycolor(rgba);
	};


	// Readability Functions
	// ---------------------
	// <http://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef (WCAG Version 2)

	// `contrast`
	// Analyze the 2 colors and returns the color contrast defined by (WCAG Version 2)
	tinycolor.readability = function(color1, color2) {
	    var c1 = tinycolor(color1);
	    var c2 = tinycolor(color2);
	    return (Math.max(c1.getLuminance(),c2.getLuminance())+0.05) / (Math.min(c1.getLuminance(),c2.getLuminance())+0.05);
	};

	// `isReadable`
	// Ensure that foreground and background color combinations meet WCAG2 guidelines.
	// The third argument is an optional Object.
	//      the 'level' property states 'AA' or 'AAA' - if missing or invalid, it defaults to 'AA';
	//      the 'size' property states 'large' or 'small' - if missing or invalid, it defaults to 'small'.
	// If the entire object is absent, isReadable defaults to {level:"AA",size:"small"}.

	// *Example*
	//    tinycolor.isReadable("#000", "#111") => false
	//    tinycolor.isReadable("#000", "#111",{level:"AA",size:"large"}) => false
	tinycolor.isReadable = function(color1, color2, wcag2) {
	    var readability = tinycolor.readability(color1, color2);
	    var wcag2Parms, out;

	    out = false;

	    wcag2Parms = validateWCAG2Parms(wcag2);
	    switch (wcag2Parms.level + wcag2Parms.size) {
	        case "AAsmall":
	        case "AAAlarge":
	            out = readability >= 4.5;
	            break;
	        case "AAlarge":
	            out = readability >= 3;
	            break;
	        case "AAAsmall":
	            out = readability >= 7;
	            break;
	    }
	    return out;

	};

	// `mostReadable`
	// Given a base color and a list of possible foreground or background
	// colors for that base, returns the most readable color.
	// Optionally returns Black or White if the most readable color is unreadable.
	// *Example*
	//    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:false}).toHexString(); // "#112255"
	//    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:true}).toHexString();  // "#ffffff"
	//    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"large"}).toHexString(); // "#faf3f3"
	//    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"small"}).toHexString(); // "#ffffff"
	tinycolor.mostReadable = function(baseColor, colorList, args) {
	    var bestColor = null;
	    var bestScore = 0;
	    var readability;
	    var includeFallbackColors, level, size ;
	    args = args || {};
	    includeFallbackColors = args.includeFallbackColors ;
	    level = args.level;
	    size = args.size;

	    for (var i= 0; i < colorList.length ; i++) {
	        readability = tinycolor.readability(baseColor, colorList[i]);
	        if (readability > bestScore) {
	            bestScore = readability;
	            bestColor = tinycolor(colorList[i]);
	        }
	    }

	    if (tinycolor.isReadable(baseColor, bestColor, {"level":level,"size":size}) || !includeFallbackColors) {
	        return bestColor;
	    }
	    else {
	        args.includeFallbackColors=false;
	        return tinycolor.mostReadable(baseColor,["#fff", "#000"],args);
	    }
	};


	// Big List of Colors
	// ------------------
	// <http://www.w3.org/TR/css3-color/#svg-color>
	var names = tinycolor.names = {
	    aliceblue: "f0f8ff",
	    antiquewhite: "faebd7",
	    aqua: "0ff",
	    aquamarine: "7fffd4",
	    azure: "f0ffff",
	    beige: "f5f5dc",
	    bisque: "ffe4c4",
	    black: "000",
	    blanchedalmond: "ffebcd",
	    blue: "00f",
	    blueviolet: "8a2be2",
	    brown: "a52a2a",
	    burlywood: "deb887",
	    burntsienna: "ea7e5d",
	    cadetblue: "5f9ea0",
	    chartreuse: "7fff00",
	    chocolate: "d2691e",
	    coral: "ff7f50",
	    cornflowerblue: "6495ed",
	    cornsilk: "fff8dc",
	    crimson: "dc143c",
	    cyan: "0ff",
	    darkblue: "00008b",
	    darkcyan: "008b8b",
	    darkgoldenrod: "b8860b",
	    darkgray: "a9a9a9",
	    darkgreen: "006400",
	    darkgrey: "a9a9a9",
	    darkkhaki: "bdb76b",
	    darkmagenta: "8b008b",
	    darkolivegreen: "556b2f",
	    darkorange: "ff8c00",
	    darkorchid: "9932cc",
	    darkred: "8b0000",
	    darksalmon: "e9967a",
	    darkseagreen: "8fbc8f",
	    darkslateblue: "483d8b",
	    darkslategray: "2f4f4f",
	    darkslategrey: "2f4f4f",
	    darkturquoise: "00ced1",
	    darkviolet: "9400d3",
	    deeppink: "ff1493",
	    deepskyblue: "00bfff",
	    dimgray: "696969",
	    dimgrey: "696969",
	    dodgerblue: "1e90ff",
	    firebrick: "b22222",
	    floralwhite: "fffaf0",
	    forestgreen: "228b22",
	    fuchsia: "f0f",
	    gainsboro: "dcdcdc",
	    ghostwhite: "f8f8ff",
	    gold: "ffd700",
	    goldenrod: "daa520",
	    gray: "808080",
	    green: "008000",
	    greenyellow: "adff2f",
	    grey: "808080",
	    honeydew: "f0fff0",
	    hotpink: "ff69b4",
	    indianred: "cd5c5c",
	    indigo: "4b0082",
	    ivory: "fffff0",
	    khaki: "f0e68c",
	    lavender: "e6e6fa",
	    lavenderblush: "fff0f5",
	    lawngreen: "7cfc00",
	    lemonchiffon: "fffacd",
	    lightblue: "add8e6",
	    lightcoral: "f08080",
	    lightcyan: "e0ffff",
	    lightgoldenrodyellow: "fafad2",
	    lightgray: "d3d3d3",
	    lightgreen: "90ee90",
	    lightgrey: "d3d3d3",
	    lightpink: "ffb6c1",
	    lightsalmon: "ffa07a",
	    lightseagreen: "20b2aa",
	    lightskyblue: "87cefa",
	    lightslategray: "789",
	    lightslategrey: "789",
	    lightsteelblue: "b0c4de",
	    lightyellow: "ffffe0",
	    lime: "0f0",
	    limegreen: "32cd32",
	    linen: "faf0e6",
	    magenta: "f0f",
	    maroon: "800000",
	    mediumaquamarine: "66cdaa",
	    mediumblue: "0000cd",
	    mediumorchid: "ba55d3",
	    mediumpurple: "9370db",
	    mediumseagreen: "3cb371",
	    mediumslateblue: "7b68ee",
	    mediumspringgreen: "00fa9a",
	    mediumturquoise: "48d1cc",
	    mediumvioletred: "c71585",
	    midnightblue: "191970",
	    mintcream: "f5fffa",
	    mistyrose: "ffe4e1",
	    moccasin: "ffe4b5",
	    navajowhite: "ffdead",
	    navy: "000080",
	    oldlace: "fdf5e6",
	    olive: "808000",
	    olivedrab: "6b8e23",
	    orange: "ffa500",
	    orangered: "ff4500",
	    orchid: "da70d6",
	    palegoldenrod: "eee8aa",
	    palegreen: "98fb98",
	    paleturquoise: "afeeee",
	    palevioletred: "db7093",
	    papayawhip: "ffefd5",
	    peachpuff: "ffdab9",
	    peru: "cd853f",
	    pink: "ffc0cb",
	    plum: "dda0dd",
	    powderblue: "b0e0e6",
	    purple: "800080",
	    rebeccapurple: "663399",
	    red: "f00",
	    rosybrown: "bc8f8f",
	    royalblue: "4169e1",
	    saddlebrown: "8b4513",
	    salmon: "fa8072",
	    sandybrown: "f4a460",
	    seagreen: "2e8b57",
	    seashell: "fff5ee",
	    sienna: "a0522d",
	    silver: "c0c0c0",
	    skyblue: "87ceeb",
	    slateblue: "6a5acd",
	    slategray: "708090",
	    slategrey: "708090",
	    snow: "fffafa",
	    springgreen: "00ff7f",
	    steelblue: "4682b4",
	    tan: "d2b48c",
	    teal: "008080",
	    thistle: "d8bfd8",
	    tomato: "ff6347",
	    turquoise: "40e0d0",
	    violet: "ee82ee",
	    wheat: "f5deb3",
	    white: "fff",
	    whitesmoke: "f5f5f5",
	    yellow: "ff0",
	    yellowgreen: "9acd32"
	};

	// Make it easy to access colors via `hexNames[hex]`
	var hexNames = tinycolor.hexNames = flip(names);


	// Utilities
	// ---------

	// `{ 'name1': 'val1' }` becomes `{ 'val1': 'name1' }`
	function flip(o) {
	    var flipped = { };
	    for (var i in o) {
	        if (o.hasOwnProperty(i)) {
	            flipped[o[i]] = i;
	        }
	    }
	    return flipped;
	}

	// Return a valid alpha value [0,1] with all invalid values being set to 1
	function boundAlpha(a) {
	    a = parseFloat(a);

	    if (isNaN(a) || a < 0 || a > 1) {
	        a = 1;
	    }

	    return a;
	}

	// Take input from [0, n] and return it as [0, 1]
	function bound01(n, max) {
	    if (isOnePointZero(n)) { n = "100%"; }

	    var processPercent = isPercentage(n);
	    n = mathMin(max, mathMax(0, parseFloat(n)));

	    // Automatically convert percentage into number
	    if (processPercent) {
	        n = parseInt(n * max, 10) / 100;
	    }

	    // Handle floating point rounding errors
	    if ((Math.abs(n - max) < 0.000001)) {
	        return 1;
	    }

	    // Convert into [0, 1] range if it isn't already
	    return (n % max) / parseFloat(max);
	}

	// Force a number between 0 and 1
	function clamp01(val) {
	    return mathMin(1, mathMax(0, val));
	}

	// Parse a base-16 hex value into a base-10 integer
	function parseIntFromHex(val) {
	    return parseInt(val, 16);
	}

	// Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
	// <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>
	function isOnePointZero(n) {
	    return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
	}

	// Check to see if string passed in is a percentage
	function isPercentage(n) {
	    return typeof n === "string" && n.indexOf('%') != -1;
	}

	// Force a hex value to have 2 characters
	function pad2(c) {
	    return c.length == 1 ? '0' + c : '' + c;
	}

	// Replace a decimal with it's percentage value
	function convertToPercentage(n) {
	    if (n <= 1) {
	        n = (n * 100) + "%";
	    }

	    return n;
	}

	// Converts a decimal to a hex value
	function convertDecimalToHex(d) {
	    return Math.round(parseFloat(d) * 255).toString(16);
	}
	// Converts a hex value to a decimal
	function convertHexToDecimal(h) {
	    return (parseIntFromHex(h) / 255);
	}

	var matchers = (function() {

	    // <http://www.w3.org/TR/css3-values/#integers>
	    var CSS_INTEGER = "[-\\+]?\\d+%?";

	    // <http://www.w3.org/TR/css3-values/#number-value>
	    var CSS_NUMBER = "[-\\+]?\\d*\\.\\d+%?";

	    // Allow positive/negative integer/number.  Don't capture the either/or, just the entire outcome.
	    var CSS_UNIT = "(?:" + CSS_NUMBER + ")|(?:" + CSS_INTEGER + ")";

	    // Actual matching.
	    // Parentheses and commas are optional, but not required.
	    // Whitespace can take the place of commas or opening paren
	    var PERMISSIVE_MATCH3 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
	    var PERMISSIVE_MATCH4 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";

	    return {
	        CSS_UNIT: new RegExp(CSS_UNIT),
	        rgb: new RegExp("rgb" + PERMISSIVE_MATCH3),
	        rgba: new RegExp("rgba" + PERMISSIVE_MATCH4),
	        hsl: new RegExp("hsl" + PERMISSIVE_MATCH3),
	        hsla: new RegExp("hsla" + PERMISSIVE_MATCH4),
	        hsv: new RegExp("hsv" + PERMISSIVE_MATCH3),
	        hsva: new RegExp("hsva" + PERMISSIVE_MATCH4),
	        hex3: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
	        hex6: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/,
	        hex4: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
	        hex8: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/
	    };
	})();

	// `isValidCSSUnit`
	// Take in a single string / number and check to see if it looks like a CSS unit
	// (see `matchers` above for definition).
	function isValidCSSUnit(color) {
	    return !!matchers.CSS_UNIT.exec(color);
	}

	// `stringInputToObject`
	// Permissive string parsing.  Take in a number of formats, and output an object
	// based on detected format.  Returns `{ r, g, b }` or `{ h, s, l }` or `{ h, s, v}`
	function stringInputToObject(color) {

	    color = color.replace(trimLeft,'').replace(trimRight, '').toLowerCase();
	    var named = false;
	    if (names[color]) {
	        color = names[color];
	        named = true;
	    }
	    else if (color == 'transparent') {
	        return { r: 0, g: 0, b: 0, a: 0, format: "name" };
	    }

	    // Try to match string input using regular expressions.
	    // Keep most of the number bounding out of this function - don't worry about [0,1] or [0,100] or [0,360]
	    // Just return an object and let the conversion functions handle that.
	    // This way the result will be the same whether the tinycolor is initialized with string or object.
	    var match;
	    if ((match = matchers.rgb.exec(color))) {
	        return { r: match[1], g: match[2], b: match[3] };
	    }
	    if ((match = matchers.rgba.exec(color))) {
	        return { r: match[1], g: match[2], b: match[3], a: match[4] };
	    }
	    if ((match = matchers.hsl.exec(color))) {
	        return { h: match[1], s: match[2], l: match[3] };
	    }
	    if ((match = matchers.hsla.exec(color))) {
	        return { h: match[1], s: match[2], l: match[3], a: match[4] };
	    }
	    if ((match = matchers.hsv.exec(color))) {
	        return { h: match[1], s: match[2], v: match[3] };
	    }
	    if ((match = matchers.hsva.exec(color))) {
	        return { h: match[1], s: match[2], v: match[3], a: match[4] };
	    }
	    if ((match = matchers.hex8.exec(color))) {
	        return {
	            r: parseIntFromHex(match[1]),
	            g: parseIntFromHex(match[2]),
	            b: parseIntFromHex(match[3]),
	            a: convertHexToDecimal(match[4]),
	            format: named ? "name" : "hex8"
	        };
	    }
	    if ((match = matchers.hex6.exec(color))) {
	        return {
	            r: parseIntFromHex(match[1]),
	            g: parseIntFromHex(match[2]),
	            b: parseIntFromHex(match[3]),
	            format: named ? "name" : "hex"
	        };
	    }
	    if ((match = matchers.hex4.exec(color))) {
	        return {
	            r: parseIntFromHex(match[1] + '' + match[1]),
	            g: parseIntFromHex(match[2] + '' + match[2]),
	            b: parseIntFromHex(match[3] + '' + match[3]),
	            a: convertHexToDecimal(match[4] + '' + match[4]),
	            format: named ? "name" : "hex8"
	        };
	    }
	    if ((match = matchers.hex3.exec(color))) {
	        return {
	            r: parseIntFromHex(match[1] + '' + match[1]),
	            g: parseIntFromHex(match[2] + '' + match[2]),
	            b: parseIntFromHex(match[3] + '' + match[3]),
	            format: named ? "name" : "hex"
	        };
	    }

	    return false;
	}

	function validateWCAG2Parms(parms) {
	    // return valid WCAG2 parms for isReadable.
	    // If input parms are invalid, return {"level":"AA", "size":"small"}
	    var level, size;
	    parms = parms || {"level":"AA", "size":"small"};
	    level = (parms.level || "AA").toUpperCase();
	    size = (parms.size || "small").toLowerCase();
	    if (level !== "AA" && level !== "AAA") {
	        level = "AA";
	    }
	    if (size !== "small" && size !== "large") {
	        size = "small";
	    }
	    return {"level":level, "size":size};
	}

	// Node: Export function
	if (module.exports) {
	    module.exports = tinycolor;
	}
	// AMD/requirejs: Define the module
	else if (typeof undefined === 'function' && undefined.amd) {
	    undefined(function () {return tinycolor;});
	}
	// Browser: Expose to window
	else {
	    window.tinycolor = tinycolor;
	}

	})(Math);
	});

	var colorUtils = {
	  simpleCheckForValidColor: function simpleCheckForValidColor(data) {
	    var keysToCheck = ['r', 'g', 'b', 'a', 'h', 's', 'l', 'v'];
	    var checked = 0;
	    var passed = 0;
	    each(keysToCheck, function (letter) {
	      if (data[letter]) {
	        checked += 1;
	        if (!isNaN(data[letter])) {
	          passed += 1;
	        }
	        if (letter === 's' || letter === 'l') {
	          var percentPatt = /^\d+%$/;
	          if (percentPatt.test(data[letter])) {
	            passed += 1;
	          }
	        }
	      }
	    });
	    return checked === passed ? data : false;
	  },
	  toState: function toState(data, oldHue) {
	    var color = data.hex ? tinycolor(data.hex) : tinycolor(data);
	    var hsl = color.toHsl();
	    var hsv = color.toHsv();
	    var rgb = color.toRgb();
	    var hex = color.toHex();
	    if (hsl.s === 0) {
	      hsl.h = oldHue || 0;
	      hsv.h = oldHue || 0;
	    }
	    var transparent = hex === '000000' && rgb.a === 0;

	    return {
	      hsl: hsl,
	      hex: transparent ? 'transparent' : '#' + hex,
	      rgb: rgb,
	      hsv: hsv,
	      oldHue: data.h || oldHue || hsl.h,
	      source: data.source
	    };
	  },
	  isValidHex: function isValidHex(hex) {
	    // disable hex4 and hex8
	    var lh = String(hex).charAt(0) === '#' ? 1 : 0;
	    return hex.length !== 4 + lh && hex.length < 7 + lh && tinycolor(hex).isValid();
	  },
	  getContrastingColor: function getContrastingColor(data) {
	    if (!data) {
	      return '#fff';
	    }
	    var col = this.toState(data);
	    if (col.hex === 'transparent') {
	      return 'rgba(0,0,0,0.4)';
	    }
	    var yiq = (col.rgb.r * 299 + col.rgb.g * 587 + col.rgb.b * 114) / 1000;
	    return yiq >= 128 ? '#000' : '#fff';
	  }
	};

	var ColorWrap = function ColorWrap(Picker) {
	  var ColorPicker = function (_ref) {
	    inherits(ColorPicker, _ref);

	    function ColorPicker(props) {
	      classCallCheck(this, ColorPicker);

	      var _this = possibleConstructorReturn(this, (ColorPicker.__proto__ || Object.getPrototypeOf(ColorPicker)).call(this));

	      _this.handleChange = function (data, event) {
	        var isValidColor = colorUtils.simpleCheckForValidColor(data);
	        if (isValidColor) {
	          var colors = colorUtils.toState(data, data.h || _this.state.oldHue);
	          _this.setState(colors);
	          _this.props.onChangeComplete && _this.debounce(_this.props.onChangeComplete, colors, event);
	          _this.props.onChange && _this.props.onChange(colors, event);
	        }
	      };

	      _this.handleSwatchHover = function (data, event) {
	        var isValidColor = colorUtils.simpleCheckForValidColor(data);
	        if (isValidColor) {
	          var colors = colorUtils.toState(data, data.h || _this.state.oldHue);
	          _this.setState(colors);
	          _this.props.onSwatchHover && _this.props.onSwatchHover(colors, event);
	        }
	      };

	      _this.state = _extends({}, colorUtils.toState(props.color, 0));

	      _this.debounce = debounce_1(function (fn, data, event) {
	        fn(data, event);
	      }, 100);
	      return _this;
	    }

	    createClass(ColorPicker, [{
	      key: 'componentWillReceiveProps',
	      value: function componentWillReceiveProps(nextProps) {
	        this.setState(_extends({}, colorUtils.toState(nextProps.color, this.state.oldHue)));
	      }
	    }, {
	      key: 'render',
	      value: function render() {
	        var optionalEvents = {};
	        if (this.props.onSwatchHover) {
	          optionalEvents.onSwatchHover = this.handleSwatchHover;
	        }

	        return React__default.createElement(Picker, _extends({}, this.props, this.state, {
	          onChange: this.handleChange
	        }, optionalEvents));
	      }
	    }]);
	    return ColorPicker;
	  }(React.PureComponent || React.Component);

	  ColorPicker.propTypes = _extends({}, Picker.propTypes);

	  ColorPicker.defaultProps = _extends({}, Picker.defaultProps, {
	    color: {
	      h: 250,
	      s: 0.50,
	      l: 0.20,
	      a: 1
	    }
	  });

	  return ColorPicker;
	};

	/* eslint-disable no-invalid-this */

	var handleFocus = function handleFocus(Component) {
	  var Span = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'span';
	  return function (_React$Component) {
	    inherits(Focus, _React$Component);

	    function Focus() {
	      var _ref;

	      var _temp, _this, _ret;

	      classCallCheck(this, Focus);

	      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	        args[_key] = arguments[_key];
	      }

	      return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Focus.__proto__ || Object.getPrototypeOf(Focus)).call.apply(_ref, [this].concat(args))), _this), _this.state = { focus: false }, _this.handleFocus = function () {
	        return _this.setState({ focus: true });
	      }, _this.handleBlur = function () {
	        return _this.setState({ focus: false });
	      }, _temp), possibleConstructorReturn(_this, _ret);
	    }

	    createClass(Focus, [{
	      key: 'render',
	      value: function render() {
	        return React__default.createElement(
	          Span,
	          { onFocus: this.handleFocus, onBlur: this.handleBlur },
	          React__default.createElement(Component, _extends({}, this.props, this.state))
	        );
	      }
	    }]);
	    return Focus;
	  }(React__default.Component);
	};

	var ENTER = 13;

	var Swatch = function Swatch(_ref) {
	  var color = _ref.color,
	      style = _ref.style,
	      _ref$onClick = _ref.onClick,
	      onClick = _ref$onClick === undefined ? function () {} : _ref$onClick,
	      onHover = _ref.onHover,
	      _ref$title = _ref.title,
	      title = _ref$title === undefined ? color : _ref$title,
	      children = _ref.children,
	      focus = _ref.focus,
	      _ref$focusStyle = _ref.focusStyle,
	      focusStyle = _ref$focusStyle === undefined ? {} : _ref$focusStyle;

	  var transparent = color === 'transparent';
	  var styles = reactCSS({
	    default: {
	      swatch: _extends({
	        background: color,
	        height: '100%',
	        width: '100%',
	        cursor: 'pointer',
	        position: 'relative',
	        outline: 'none'
	      }, style, focus ? focusStyle : {})
	    }
	  });

	  var handleClick = function handleClick(e) {
	    return onClick(color, e);
	  };
	  var handleKeyDown = function handleKeyDown(e) {
	    return e.keyCode === ENTER && onClick(color, e);
	  };
	  var handleHover = function handleHover(e) {
	    return onHover(color, e);
	  };

	  var optionalEvents = {};
	  if (onHover) {
	    optionalEvents.onMouseOver = handleHover;
	  }

	  return React__default.createElement(
	    'div',
	    _extends({
	      style: styles.swatch,
	      onClick: handleClick,
	      title: title,
	      tabIndex: 0,
	      onKeyDown: handleKeyDown
	    }, optionalEvents),
	    children,
	    transparent && React__default.createElement(Checkboard, {
	      borderRadius: styles.swatch.borderRadius,
	      boxShadow: 'inset 0 0 0 1px rgba(0,0,0,0.1)'
	    })
	  );
	};

	var Swatch$1 = handleFocus(Swatch);

	var AlphaPointer = function AlphaPointer(_ref) {
	  var direction = _ref.direction;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '18px',
	        height: '18px',
	        borderRadius: '50%',
	        transform: 'translate(-9px, -1px)',
	        backgroundColor: 'rgb(248, 248, 248)',
	        boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)'
	      }
	    },
	    'vertical': {
	      picker: {
	        transform: 'translate(-3px, -9px)'
	      }
	    }
	  }, { vertical: direction === 'vertical' });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var AlphaPicker = function AlphaPicker(_ref) {
	  var rgb = _ref.rgb,
	      hsl = _ref.hsl,
	      width = _ref.width,
	      height = _ref.height,
	      onChange = _ref.onChange,
	      direction = _ref.direction,
	      style = _ref.style,
	      renderers = _ref.renderers,
	      pointer = _ref.pointer,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        position: 'relative',
	        width: width,
	        height: height
	      },
	      alpha: {
	        radius: '2px',
	        style: style
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { style: styles.picker, className: 'alpha-picker ' + className },
	    React__default.createElement(Alpha, _extends({}, styles.alpha, {
	      rgb: rgb,
	      hsl: hsl,
	      pointer: pointer,
	      renderers: renderers,
	      onChange: onChange,
	      direction: direction
	    }))
	  );
	};

	AlphaPicker.defaultProps = {
	  width: '316px',
	  height: '16px',
	  direction: 'horizontal',
	  pointer: AlphaPointer
	};

	var Alpha$1 = ColorWrap(AlphaPicker);

	var BlockSwatches = function BlockSwatches(_ref) {
	  var colors = _ref.colors,
	      onClick = _ref.onClick,
	      onSwatchHover = _ref.onSwatchHover;

	  var styles = reactCSS({
	    'default': {
	      swatches: {
	        marginRight: '-10px'
	      },
	      swatch: {
	        width: '22px',
	        height: '22px',
	        float: 'left',
	        marginRight: '10px',
	        marginBottom: '10px',
	        borderRadius: '4px'
	      },
	      clear: {
	        clear: 'both'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { style: styles.swatches },
	    map_1(colors, function (c) {
	      return React__default.createElement(Swatch$1, {
	        key: c,
	        color: c,
	        style: styles.swatch,
	        onClick: onClick,
	        onHover: onSwatchHover,
	        focusStyle: {
	          boxShadow: '0 0 4px ' + c
	        }
	      });
	    }),
	    React__default.createElement('div', { style: styles.clear })
	  );
	};

	var Block = function Block(_ref) {
	  var onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      hex = _ref.hex,
	      colors = _ref.colors,
	      width = _ref.width,
	      triangle = _ref.triangle,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var transparent = hex === 'transparent';
	  var handleChange = function handleChange(hexCode, e) {
	    colorUtils.isValidHex(hexCode) && onChange({
	      hex: hexCode,
	      source: 'hex'
	    }, e);
	  };

	  var styles = reactCSS({
	    'default': {
	      card: {
	        width: width,
	        background: '#fff',
	        boxShadow: '0 1px rgba(0,0,0,.1)',
	        borderRadius: '6px',
	        position: 'relative'
	      },
	      head: {
	        height: '110px',
	        background: hex,
	        borderRadius: '6px 6px 0 0',
	        display: 'flex',
	        alignItems: 'center',
	        justifyContent: 'center',
	        position: 'relative'
	      },
	      body: {
	        padding: '10px'
	      },
	      label: {
	        fontSize: '18px',
	        color: colorUtils.getContrastingColor(hex),
	        position: 'relative'
	      },
	      triangle: {
	        width: '0px',
	        height: '0px',
	        borderStyle: 'solid',
	        borderWidth: '0 10px 10px 10px',
	        borderColor: 'transparent transparent ' + hex + ' transparent',
	        position: 'absolute',
	        top: '-10px',
	        left: '50%',
	        marginLeft: '-10px'
	      },
	      input: {
	        width: '100%',
	        fontSize: '12px',
	        color: '#666',
	        border: '0px',
	        outline: 'none',
	        height: '22px',
	        boxShadow: 'inset 0 0 0 1px #ddd',
	        borderRadius: '4px',
	        padding: '0 7px',
	        boxSizing: 'border-box'
	      }
	    },
	    'hide-triangle': {
	      triangle: {
	        display: 'none'
	      }
	    }
	  }, { 'hide-triangle': triangle === 'hide' });

	  return React__default.createElement(
	    'div',
	    { style: styles.card, className: 'block-picker ' + className },
	    React__default.createElement('div', { style: styles.triangle }),
	    React__default.createElement(
	      'div',
	      { style: styles.head },
	      transparent && React__default.createElement(Checkboard, { borderRadius: '6px 6px 0 0' }),
	      React__default.createElement(
	        'div',
	        { style: styles.label },
	        hex
	      )
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.body },
	      React__default.createElement(BlockSwatches, { colors: colors, onClick: handleChange, onSwatchHover: onSwatchHover }),
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input },
	        value: hex,
	        onChange: handleChange
	      })
	    )
	  );
	};

	Block.propTypes = {
	  width: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  colors: propTypes.arrayOf(propTypes.string),
	  triangle: propTypes.oneOf(['top', 'hide'])
	};

	Block.defaultProps = {
	  width: 170,
	  colors: ['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555', '#dce775', '#ff8a65', '#ba68c8'],
	  triangle: 'top'
	};

	var Block$1 = ColorWrap(Block);

	var red$1 = {"50":"#ffebee","100":"#ffcdd2","200":"#ef9a9a","300":"#e57373","400":"#ef5350","500":"#f44336","600":"#e53935","700":"#d32f2f","800":"#c62828","900":"#b71c1c","a100":"#ff8a80","a200":"#ff5252","a400":"#ff1744","a700":"#d50000"};
	var pink = {"50":"#fce4ec","100":"#f8bbd0","200":"#f48fb1","300":"#f06292","400":"#ec407a","500":"#e91e63","600":"#d81b60","700":"#c2185b","800":"#ad1457","900":"#880e4f","a100":"#ff80ab","a200":"#ff4081","a400":"#f50057","a700":"#c51162"};
	var purple = {"50":"#f3e5f5","100":"#e1bee7","200":"#ce93d8","300":"#ba68c8","400":"#ab47bc","500":"#9c27b0","600":"#8e24aa","700":"#7b1fa2","800":"#6a1b9a","900":"#4a148c","a100":"#ea80fc","a200":"#e040fb","a400":"#d500f9","a700":"#aa00ff"};
	var deepPurple = {"50":"#ede7f6","100":"#d1c4e9","200":"#b39ddb","300":"#9575cd","400":"#7e57c2","500":"#673ab7","600":"#5e35b1","700":"#512da8","800":"#4527a0","900":"#311b92","a100":"#b388ff","a200":"#7c4dff","a400":"#651fff","a700":"#6200ea"};
	var indigo = {"50":"#e8eaf6","100":"#c5cae9","200":"#9fa8da","300":"#7986cb","400":"#5c6bc0","500":"#3f51b5","600":"#3949ab","700":"#303f9f","800":"#283593","900":"#1a237e","a100":"#8c9eff","a200":"#536dfe","a400":"#3d5afe","a700":"#304ffe"};
	var blue = {"50":"#e3f2fd","100":"#bbdefb","200":"#90caf9","300":"#64b5f6","400":"#42a5f5","500":"#2196f3","600":"#1e88e5","700":"#1976d2","800":"#1565c0","900":"#0d47a1","a100":"#82b1ff","a200":"#448aff","a400":"#2979ff","a700":"#2962ff"};
	var lightBlue = {"50":"#e1f5fe","100":"#b3e5fc","200":"#81d4fa","300":"#4fc3f7","400":"#29b6f6","500":"#03a9f4","600":"#039be5","700":"#0288d1","800":"#0277bd","900":"#01579b","a100":"#80d8ff","a200":"#40c4ff","a400":"#00b0ff","a700":"#0091ea"};
	var cyan = {"50":"#e0f7fa","100":"#b2ebf2","200":"#80deea","300":"#4dd0e1","400":"#26c6da","500":"#00bcd4","600":"#00acc1","700":"#0097a7","800":"#00838f","900":"#006064","a100":"#84ffff","a200":"#18ffff","a400":"#00e5ff","a700":"#00b8d4"};
	var teal = {"50":"#e0f2f1","100":"#b2dfdb","200":"#80cbc4","300":"#4db6ac","400":"#26a69a","500":"#009688","600":"#00897b","700":"#00796b","800":"#00695c","900":"#004d40","a100":"#a7ffeb","a200":"#64ffda","a400":"#1de9b6","a700":"#00bfa5"};
	var green = {"50":"#e8f5e9","100":"#c8e6c9","200":"#a5d6a7","300":"#81c784","400":"#66bb6a","500":"#4caf50","600":"#43a047","700":"#388e3c","800":"#2e7d32","900":"#1b5e20","a100":"#b9f6ca","a200":"#69f0ae","a400":"#00e676","a700":"#00c853"};
	var lightGreen = {"50":"#f1f8e9","100":"#dcedc8","200":"#c5e1a5","300":"#aed581","400":"#9ccc65","500":"#8bc34a","600":"#7cb342","700":"#689f38","800":"#558b2f","900":"#33691e","a100":"#ccff90","a200":"#b2ff59","a400":"#76ff03","a700":"#64dd17"};
	var lime = {"50":"#f9fbe7","100":"#f0f4c3","200":"#e6ee9c","300":"#dce775","400":"#d4e157","500":"#cddc39","600":"#c0ca33","700":"#afb42b","800":"#9e9d24","900":"#827717","a100":"#f4ff81","a200":"#eeff41","a400":"#c6ff00","a700":"#aeea00"};
	var yellow = {"50":"#fffde7","100":"#fff9c4","200":"#fff59d","300":"#fff176","400":"#ffee58","500":"#ffeb3b","600":"#fdd835","700":"#fbc02d","800":"#f9a825","900":"#f57f17","a100":"#ffff8d","a200":"#ffff00","a400":"#ffea00","a700":"#ffd600"};
	var amber = {"50":"#fff8e1","100":"#ffecb3","200":"#ffe082","300":"#ffd54f","400":"#ffca28","500":"#ffc107","600":"#ffb300","700":"#ffa000","800":"#ff8f00","900":"#ff6f00","a100":"#ffe57f","a200":"#ffd740","a400":"#ffc400","a700":"#ffab00"};
	var orange = {"50":"#fff3e0","100":"#ffe0b2","200":"#ffcc80","300":"#ffb74d","400":"#ffa726","500":"#ff9800","600":"#fb8c00","700":"#f57c00","800":"#ef6c00","900":"#e65100","a100":"#ffd180","a200":"#ffab40","a400":"#ff9100","a700":"#ff6d00"};
	var deepOrange = {"50":"#fbe9e7","100":"#ffccbc","200":"#ffab91","300":"#ff8a65","400":"#ff7043","500":"#ff5722","600":"#f4511e","700":"#e64a19","800":"#d84315","900":"#bf360c","a100":"#ff9e80","a200":"#ff6e40","a400":"#ff3d00","a700":"#dd2c00"};
	var brown = {"50":"#efebe9","100":"#d7ccc8","200":"#bcaaa4","300":"#a1887f","400":"#8d6e63","500":"#795548","600":"#6d4c41","700":"#5d4037","800":"#4e342e","900":"#3e2723"};
	var blueGrey = {"50":"#eceff1","100":"#cfd8dc","200":"#b0bec5","300":"#90a4ae","400":"#78909c","500":"#607d8b","600":"#546e7a","700":"#455a64","800":"#37474f","900":"#263238"};

	var CircleSwatch = function CircleSwatch(_ref) {
	  var color = _ref.color,
	      onClick = _ref.onClick,
	      onSwatchHover = _ref.onSwatchHover,
	      hover = _ref.hover,
	      active = _ref.active,
	      circleSize = _ref.circleSize,
	      circleSpacing = _ref.circleSpacing;

	  var styles = reactCSS({
	    'default': {
	      swatch: {
	        width: circleSize,
	        height: circleSize,
	        marginRight: circleSpacing,
	        marginBottom: circleSpacing,
	        transform: 'scale(1)',
	        transition: '100ms transform ease'
	      },
	      Swatch: {
	        borderRadius: '50%',
	        background: 'transparent',
	        boxShadow: 'inset 0 0 0 ' + circleSize / 2 + 'px ' + color,
	        transition: '100ms box-shadow ease'
	      }
	    },
	    'hover': {
	      swatch: {
	        transform: 'scale(1.2)'
	      }
	    },
	    'active': {
	      Swatch: {
	        boxShadow: 'inset 0 0 0 3px ' + color
	      }
	    }
	  }, { hover: hover, active: active });

	  return React__default.createElement(
	    'div',
	    { style: styles.swatch },
	    React__default.createElement(Swatch$1, {
	      style: styles.Swatch,
	      color: color,
	      onClick: onClick,
	      onHover: onSwatchHover,
	      focusStyle: { boxShadow: styles.Swatch.boxShadow + ', 0 0 5px ' + color }
	    })
	  );
	};

	CircleSwatch.defaultProps = {
	  circleSize: 28,
	  circleSpacing: 14
	};

	var CircleSwatch$1 = lib_4(CircleSwatch);

	var Circle = function Circle(_ref) {
	  var width = _ref.width,
	      onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      colors = _ref.colors,
	      hex = _ref.hex,
	      circleSize = _ref.circleSize,
	      circleSpacing = _ref.circleSpacing,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      card: {
	        width: width,
	        display: 'flex',
	        flexWrap: 'wrap',
	        marginRight: -circleSpacing,
	        marginBottom: -circleSpacing
	      }
	    }
	  });

	  var handleChange = function handleChange(hexCode, e) {
	    return onChange({ hex: hexCode, source: 'hex' }, e);
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.card, className: 'circle-picker ' + className },
	    map_1(colors, function (c) {
	      return React__default.createElement(CircleSwatch$1, {
	        key: c,
	        color: c,
	        onClick: handleChange,
	        onSwatchHover: onSwatchHover,
	        active: hex === c.toLowerCase(),
	        circleSize: circleSize,
	        circleSpacing: circleSpacing
	      });
	    })
	  );
	};

	Circle.propTypes = {
	  width: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  circleSize: propTypes.number,
	  circleSpacing: propTypes.number
	};

	Circle.defaultProps = {
	  width: 252,
	  circleSize: 28,
	  circleSpacing: 14,
	  colors: [red$1['500'], pink['500'], purple['500'], deepPurple['500'], indigo['500'], blue['500'], lightBlue['500'], cyan['500'], teal['500'], green['500'], lightGreen['500'], lime['500'], yellow['500'], amber['500'], orange['500'], deepOrange['500'], brown['500'], blueGrey['500']]
	};

	var Circle$1 = ColorWrap(Circle);

	var UnfoldMoreHorizontalIcon = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



	var _react2 = _interopRequireDefault(React__default);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

	var DEFAULT_SIZE = 24;

	exports.default = function (_ref) {
	  var _ref$fill = _ref.fill,
	      fill = _ref$fill === undefined ? 'currentColor' : _ref$fill,
	      _ref$width = _ref.width,
	      width = _ref$width === undefined ? DEFAULT_SIZE : _ref$width,
	      _ref$height = _ref.height,
	      height = _ref$height === undefined ? DEFAULT_SIZE : _ref$height,
	      _ref$style = _ref.style,
	      style = _ref$style === undefined ? {} : _ref$style,
	      props = _objectWithoutProperties(_ref, ['fill', 'width', 'height', 'style']);

	  return _react2.default.createElement(
	    'svg',
	    _extends({
	      viewBox: '0 0 ' + DEFAULT_SIZE + ' ' + DEFAULT_SIZE,
	      style: _extends({ fill: fill, width: width, height: height }, style)
	    }, props),
	    _react2.default.createElement('path', { d: 'M12,18.17L8.83,15L7.42,16.41L12,21L16.59,16.41L15.17,15M12,5.83L15.17,9L16.58,7.59L12,3L7.41,7.59L8.83,9L12,5.83Z' })
	  );
	};
	});

	var UnfoldMoreHorizontalIcon$1 = unwrapExports(UnfoldMoreHorizontalIcon);

	/* eslint-disable react/no-did-mount-set-state, no-param-reassign */

	var ChromeFields = function (_React$Component) {
	  inherits(ChromeFields, _React$Component);

	  function ChromeFields() {
	    var _ref;

	    var _temp, _this, _ret;

	    classCallCheck(this, ChromeFields);

	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }

	    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = ChromeFields.__proto__ || Object.getPrototypeOf(ChromeFields)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
	      view: ''
	    }, _this.toggleViews = function () {
	      if (_this.state.view === 'hex') {
	        _this.setState({ view: 'rgb' });
	      } else if (_this.state.view === 'rgb') {
	        _this.setState({ view: 'hsl' });
	      } else if (_this.state.view === 'hsl') {
	        if (_this.props.hsl.a === 1) {
	          _this.setState({ view: 'hex' });
	        } else {
	          _this.setState({ view: 'rgb' });
	        }
	      }
	    }, _this.handleChange = function (data, e) {
	      if (data.hex) {
	        colorUtils.isValidHex(data.hex) && _this.props.onChange({
	          hex: data.hex,
	          source: 'hex'
	        }, e);
	      } else if (data.r || data.g || data.b) {
	        _this.props.onChange({
	          r: data.r || _this.props.rgb.r,
	          g: data.g || _this.props.rgb.g,
	          b: data.b || _this.props.rgb.b,
	          source: 'rgb'
	        }, e);
	      } else if (data.a) {
	        if (data.a < 0) {
	          data.a = 0;
	        } else if (data.a > 1) {
	          data.a = 1;
	        }

	        _this.props.onChange({
	          h: _this.props.hsl.h,
	          s: _this.props.hsl.s,
	          l: _this.props.hsl.l,
	          a: Math.round(data.a * 100) / 100,
	          source: 'rgb'
	        }, e);
	      } else if (data.h || data.s || data.l) {
	        _this.props.onChange({
	          h: data.h || _this.props.hsl.h,
	          s: Number(data.s && data.s || _this.props.hsl.s),
	          l: Number(data.l && data.l || _this.props.hsl.l),
	          source: 'hsl'
	        }, e);
	      }
	    }, _this.showHighlight = function (e) {
	      e.target.style.background = '#eee';
	    }, _this.hideHighlight = function (e) {
	      e.target.style.background = 'transparent';
	    }, _temp), possibleConstructorReturn(_this, _ret);
	  }

	  createClass(ChromeFields, [{
	    key: 'componentDidMount',
	    value: function componentDidMount() {
	      if (this.props.hsl.a === 1 && this.state.view !== 'hex') {
	        this.setState({ view: 'hex' });
	      } else if (this.state.view !== 'rgb' && this.state.view !== 'hsl') {
	        this.setState({ view: 'rgb' });
	      }
	    }
	  }, {
	    key: 'componentWillReceiveProps',
	    value: function componentWillReceiveProps(nextProps) {
	      if (nextProps.hsl.a !== 1 && this.state.view === 'hex') {
	        this.setState({ view: 'rgb' });
	      }
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      var _this2 = this;

	      var styles = reactCSS({
	        'default': {
	          wrap: {
	            paddingTop: '16px',
	            display: 'flex'
	          },
	          fields: {
	            flex: '1',
	            display: 'flex',
	            marginLeft: '-6px'
	          },
	          field: {
	            paddingLeft: '6px',
	            width: '100%'
	          },
	          alpha: {
	            paddingLeft: '6px',
	            width: '100%'
	          },
	          toggle: {
	            width: '32px',
	            textAlign: 'right',
	            position: 'relative'
	          },
	          icon: {
	            marginRight: '-4px',
	            marginTop: '12px',
	            cursor: 'pointer',
	            position: 'relative'
	          },
	          iconHighlight: {
	            position: 'absolute',
	            width: '24px',
	            height: '28px',
	            background: '#eee',
	            borderRadius: '4px',
	            top: '10px',
	            left: '12px',
	            display: 'none'
	          },
	          input: {
	            fontSize: '11px',
	            color: '#333',
	            width: '100%',
	            borderRadius: '2px',
	            border: 'none',
	            boxShadow: 'inset 0 0 0 1px #dadada',
	            height: '21px',
	            textAlign: 'center'
	          },
	          label: {
	            textTransform: 'uppercase',
	            fontSize: '11px',
	            lineHeight: '11px',
	            color: '#969696',
	            textAlign: 'center',
	            display: 'block',
	            marginTop: '12px'
	          },
	          svg: {
	            fill: '#333',
	            width: '24px',
	            height: '24px',
	            border: '1px transparent solid',
	            borderRadius: '5px'
	          }
	        },
	        'disableAlpha': {
	          alpha: {
	            display: 'none'
	          }
	        }
	      }, this.props, this.state);

	      var fields = void 0;
	      if (this.state.view === 'hex') {
	        fields = React__default.createElement(
	          'div',
	          { style: styles.fields, className: 'flexbox-fix' },
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'hex', value: this.props.hex,
	              onChange: this.handleChange
	            })
	          )
	        );
	      } else if (this.state.view === 'rgb') {
	        fields = React__default.createElement(
	          'div',
	          { style: styles.fields, className: 'flexbox-fix' },
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'r',
	              value: this.props.rgb.r,
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'g',
	              value: this.props.rgb.g,
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'b',
	              value: this.props.rgb.b,
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.alpha },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'a',
	              value: this.props.rgb.a,
	              arrowOffset: 0.01,
	              onChange: this.handleChange
	            })
	          )
	        );
	      } else if (this.state.view === 'hsl') {
	        fields = React__default.createElement(
	          'div',
	          { style: styles.fields, className: 'flexbox-fix' },
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'h',
	              value: Math.round(this.props.hsl.h),
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 's',
	              value: Math.round(this.props.hsl.s * 100) + '%',
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.field },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'l',
	              value: Math.round(this.props.hsl.l * 100) + '%',
	              onChange: this.handleChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.alpha },
	            React__default.createElement(EditableInput, {
	              style: { input: styles.input, label: styles.label },
	              label: 'a',
	              value: this.props.hsl.a,
	              arrowOffset: 0.01,
	              onChange: this.handleChange
	            })
	          )
	        );
	      }

	      return React__default.createElement(
	        'div',
	        { style: styles.wrap, className: 'flexbox-fix' },
	        fields,
	        React__default.createElement(
	          'div',
	          { style: styles.toggle },
	          React__default.createElement(
	            'div',
	            { style: styles.icon, onClick: this.toggleViews, ref: function ref(icon) {
	                return _this2.icon = icon;
	              } },
	            React__default.createElement(UnfoldMoreHorizontalIcon$1, {
	              style: styles.svg,
	              onMouseOver: this.showHighlight,
	              onMouseEnter: this.showHighlight,
	              onMouseOut: this.hideHighlight
	            })
	          )
	        )
	      );
	    }
	  }]);
	  return ChromeFields;
	}(React__default.Component);

	var ChromePointer = function ChromePointer() {
	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '12px',
	        height: '12px',
	        borderRadius: '6px',
	        transform: 'translate(-6px, -1px)',
	        backgroundColor: 'rgb(248, 248, 248)',
	        boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)'
	      }
	    }
	  });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var ChromePointerCircle = function ChromePointerCircle() {
	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '12px',
	        height: '12px',
	        borderRadius: '6px',
	        boxShadow: 'inset 0 0 0 1px #fff',
	        transform: 'translate(-6px, -6px)'
	      }
	    }
	  });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var Chrome = function Chrome(_ref) {
	  var onChange = _ref.onChange,
	      disableAlpha = _ref.disableAlpha,
	      rgb = _ref.rgb,
	      hsl = _ref.hsl,
	      hsv = _ref.hsv,
	      hex = _ref.hex,
	      renderers = _ref.renderers,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        background: '#fff',
	        borderRadius: '2px',
	        boxShadow: '0 0 2px rgba(0,0,0,.3), 0 4px 8px rgba(0,0,0,.3)',
	        boxSizing: 'initial',
	        width: '225px',
	        fontFamily: 'Menlo'
	      },
	      saturation: {
	        width: '100%',
	        paddingBottom: '55%',
	        position: 'relative',
	        borderRadius: '2px 2px 0 0',
	        overflow: 'hidden'
	      },
	      Saturation: {
	        radius: '2px 2px 0 0'
	      },
	      body: {
	        padding: '16px 16px 12px'
	      },
	      controls: {
	        display: 'flex'
	      },
	      color: {
	        width: '32px'
	      },
	      swatch: {
	        marginTop: '6px',
	        width: '16px',
	        height: '16px',
	        borderRadius: '8px',
	        position: 'relative',
	        overflow: 'hidden'
	      },
	      active: {
	        absolute: '0px 0px 0px 0px',
	        borderRadius: '8px',
	        boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.1)',
	        background: 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ' + rgb.a + ')',
	        zIndex: '2'
	      },
	      toggles: {
	        flex: '1'
	      },
	      hue: {
	        height: '10px',
	        position: 'relative',
	        marginBottom: '8px'
	      },
	      Hue: {
	        radius: '2px'
	      },
	      alpha: {
	        height: '10px',
	        position: 'relative'
	      },
	      Alpha: {
	        radius: '2px'
	      }
	    },
	    'disableAlpha': {
	      color: {
	        width: '22px'
	      },
	      alpha: {
	        display: 'none'
	      },
	      hue: {
	        marginBottom: '0px'
	      },
	      swatch: {
	        width: '10px',
	        height: '10px',
	        marginTop: '0px'
	      }
	    }
	  }, { disableAlpha: disableAlpha });

	  return React__default.createElement(
	    'div',
	    { style: styles.picker, className: 'chrome-picker ' + className },
	    React__default.createElement(
	      'div',
	      { style: styles.saturation },
	      React__default.createElement(Saturation, {
	        style: styles.Saturation,
	        hsl: hsl,
	        hsv: hsv,
	        pointer: ChromePointerCircle,
	        onChange: onChange
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.body },
	      React__default.createElement(
	        'div',
	        { style: styles.controls, className: 'flexbox-fix' },
	        React__default.createElement(
	          'div',
	          { style: styles.color },
	          React__default.createElement(
	            'div',
	            { style: styles.swatch },
	            React__default.createElement('div', { style: styles.active }),
	            React__default.createElement(Checkboard, { renderers: renderers })
	          )
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.toggles },
	          React__default.createElement(
	            'div',
	            { style: styles.hue },
	            React__default.createElement(Hue, {
	              style: styles.Hue,
	              hsl: hsl,
	              pointer: ChromePointer,
	              onChange: onChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.alpha },
	            React__default.createElement(Alpha, {
	              style: styles.Alpha,
	              rgb: rgb,
	              hsl: hsl,
	              pointer: ChromePointer,
	              renderers: renderers,
	              onChange: onChange
	            })
	          )
	        )
	      ),
	      React__default.createElement(ChromeFields, {
	        rgb: rgb,
	        hsl: hsl,
	        hex: hex,
	        onChange: onChange,
	        disableAlpha: disableAlpha
	      })
	    )
	  );
	};

	Chrome.propTypes = {
	  disableAlpha: propTypes.bool
	};

	Chrome.defaultProps = {
	  disableAlpha: false
	};

	var Chrome$1 = ColorWrap(Chrome);

	var CompactColor = function CompactColor(_ref) {
	  var color = _ref.color,
	      _ref$onClick = _ref.onClick,
	      onClick = _ref$onClick === undefined ? function () {} : _ref$onClick,
	      onSwatchHover = _ref.onSwatchHover,
	      active = _ref.active;

	  var styles = reactCSS({
	    'default': {
	      color: {
	        background: color,
	        width: '15px',
	        height: '15px',
	        float: 'left',
	        marginRight: '5px',
	        marginBottom: '5px',
	        position: 'relative',
	        cursor: 'pointer'
	      },
	      dot: {
	        absolute: '5px 5px 5px 5px',
	        background: colorUtils.getContrastingColor(color),
	        borderRadius: '50%',
	        opacity: '0'
	      }
	    },
	    'active': {
	      dot: {
	        opacity: '1'
	      }
	    },
	    'color-#FFFFFF': {
	      color: {
	        boxShadow: 'inset 0 0 0 1px #ddd'
	      },
	      dot: {
	        background: '#000'
	      }
	    },
	    'transparent': {
	      dot: {
	        background: '#000'
	      }
	    }
	  }, { active: active, 'color-#FFFFFF': color === '#FFFFFF', 'transparent': color === 'transparent' });

	  return React__default.createElement(
	    Swatch$1,
	    {
	      style: styles.color,
	      color: color,
	      onClick: onClick,
	      onHover: onSwatchHover,
	      focusStyle: { boxShadow: '0 0 4px ' + color }
	    },
	    React__default.createElement('div', { style: styles.dot })
	  );
	};

	var CompactFields = function CompactFields(_ref) {
	  var hex = _ref.hex,
	      rgb = _ref.rgb,
	      onChange = _ref.onChange;

	  var styles = reactCSS({
	    'default': {
	      fields: {
	        display: 'flex',
	        paddingBottom: '6px',
	        paddingRight: '5px',
	        position: 'relative'
	      },
	      active: {
	        position: 'absolute',
	        top: '6px',
	        left: '5px',
	        height: '9px',
	        width: '9px',
	        background: hex
	      },
	      HEXwrap: {
	        flex: '6',
	        position: 'relative'
	      },
	      HEXinput: {
	        width: '80%',
	        padding: '0px',
	        paddingLeft: '20%',
	        border: 'none',
	        outline: 'none',
	        background: 'none',
	        fontSize: '12px',
	        color: '#333',
	        height: '16px'
	      },
	      HEXlabel: {
	        display: 'none'
	      },
	      RGBwrap: {
	        flex: '3',
	        position: 'relative'
	      },
	      RGBinput: {
	        width: '70%',
	        padding: '0px',
	        paddingLeft: '30%',
	        border: 'none',
	        outline: 'none',
	        background: 'none',
	        fontSize: '12px',
	        color: '#333',
	        height: '16px'
	      },
	      RGBlabel: {
	        position: 'absolute',
	        top: '3px',
	        left: '0px',
	        lineHeight: '16px',
	        textTransform: 'uppercase',
	        fontSize: '12px',
	        color: '#999'
	      }
	    }
	  });

	  var handleChange = function handleChange(data, e) {
	    if (data.r || data.g || data.b) {
	      onChange({
	        r: data.r || rgb.r,
	        g: data.g || rgb.g,
	        b: data.b || rgb.b,
	        source: 'rgb'
	      }, e);
	    } else {
	      onChange({
	        hex: data.hex,
	        source: 'hex'
	      }, e);
	    }
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.fields, className: 'flexbox-fix' },
	    React__default.createElement('div', { style: styles.active }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.HEXwrap, input: styles.HEXinput, label: styles.HEXlabel },
	      label: 'hex',
	      value: hex,
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'r',
	      value: rgb.r,
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'g',
	      value: rgb.g,
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'b',
	      value: rgb.b,
	      onChange: handleChange
	    })
	  );
	};

	var Compact = function Compact(_ref) {
	  var onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      colors = _ref.colors,
	      hex = _ref.hex,
	      rgb = _ref.rgb,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      Compact: {
	        background: '#f6f6f6',
	        radius: '4px'
	      },
	      compact: {
	        paddingTop: '5px',
	        paddingLeft: '5px',
	        boxSizing: 'initial',
	        width: '240px'
	      },
	      clear: {
	        clear: 'both'
	      }
	    }
	  });

	  var handleChange = function handleChange(data, e) {
	    if (data.hex) {
	      colorUtils.isValidHex(data.hex) && onChange({
	        hex: data.hex,
	        source: 'hex'
	      }, e);
	    } else {
	      onChange(data, e);
	    }
	  };

	  return React__default.createElement(
	    Raised,
	    { style: styles.Compact },
	    React__default.createElement(
	      'div',
	      { style: styles.compact, className: 'compact-picker ' + className },
	      React__default.createElement(
	        'div',
	        null,
	        map_1(colors, function (c) {
	          return React__default.createElement(CompactColor, {
	            key: c,
	            color: c,
	            active: c.toLowerCase() === hex,
	            onClick: handleChange,
	            onSwatchHover: onSwatchHover
	          });
	        }),
	        React__default.createElement('div', { style: styles.clear })
	      ),
	      React__default.createElement(CompactFields, { hex: hex, rgb: rgb, onChange: handleChange })
	    )
	  );
	};

	Compact.propTypes = {
	  colors: propTypes.arrayOf(propTypes.string)
	};

	Compact.defaultProps = {
	  colors: ['#4D4D4D', '#999999', '#FFFFFF', '#F44E3B', '#FE9200', '#FCDC00', '#DBDF00', '#A4DD00', '#68CCCA', '#73D8FF', '#AEA1FF', '#FDA1FF', '#333333', '#808080', '#cccccc', '#D33115', '#E27300', '#FCC400', '#B0BC00', '#68BC00', '#16A5A5', '#009CE0', '#7B64FF', '#FA28FF', '#000000', '#666666', '#B3B3B3', '#9F0500', '#C45100', '#FB9E00', '#808900', '#194D33', '#0C797D', '#0062B1', '#653294', '#AB149E']
	};

	var Compact$1 = ColorWrap(Compact);

	var GithubSwatch = function GithubSwatch(_ref) {
	  var hover = _ref.hover,
	      color = _ref.color,
	      onClick = _ref.onClick,
	      onSwatchHover = _ref.onSwatchHover;

	  var hoverSwatch = {
	    position: 'relative',
	    zIndex: '2',
	    outline: '2px solid #fff',
	    boxShadow: '0 0 5px 2px rgba(0,0,0,0.25)'
	  };

	  var styles = reactCSS({
	    'default': {
	      swatch: {
	        width: '25px',
	        height: '25px',
	        fontSize: '0'
	      }
	    },
	    'hover': {
	      swatch: hoverSwatch
	    }
	  }, { hover: hover });

	  return React__default.createElement(
	    'div',
	    { style: styles.swatch },
	    React__default.createElement(Swatch$1, {
	      color: color,
	      onClick: onClick,
	      onHover: onSwatchHover,
	      focusStyle: hoverSwatch
	    })
	  );
	};

	var GithubSwatch$1 = lib_4(GithubSwatch);

	var Github = function Github(_ref) {
	  var width = _ref.width,
	      colors = _ref.colors,
	      onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      triangle = _ref.triangle,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      card: {
	        width: width,
	        background: '#fff',
	        border: '1px solid rgba(0,0,0,0.2)',
	        boxShadow: '0 3px 12px rgba(0,0,0,0.15)',
	        borderRadius: '4px',
	        position: 'relative',
	        padding: '5px',
	        display: 'flex',
	        flexWrap: 'wrap'
	      },
	      triangle: {
	        position: 'absolute',
	        border: '7px solid transparent',
	        borderBottomColor: '#fff'
	      },
	      triangleShadow: {
	        position: 'absolute',
	        border: '8px solid transparent',
	        borderBottomColor: 'rgba(0,0,0,0.15)'
	      }
	    },
	    'hide-triangle': {
	      triangle: {
	        display: 'none'
	      },
	      triangleShadow: {
	        display: 'none'
	      }
	    },
	    'top-left-triangle': {
	      triangle: {
	        top: '-14px',
	        left: '10px'
	      },
	      triangleShadow: {
	        top: '-16px',
	        left: '9px'
	      }
	    },
	    'top-right-triangle': {
	      triangle: {
	        top: '-14px',
	        right: '10px'
	      },
	      triangleShadow: {
	        top: '-16px',
	        right: '9px'
	      }
	    },
	    'bottom-left-triangle': {
	      triangle: {
	        top: '35px',
	        left: '10px',
	        transform: 'rotate(180deg)'
	      },
	      triangleShadow: {
	        top: '37px',
	        left: '9px',
	        transform: 'rotate(180deg)'
	      }
	    },
	    'bottom-right-triangle': {
	      triangle: {
	        top: '35px',
	        right: '10px',
	        transform: 'rotate(180deg)'
	      },
	      triangleShadow: {
	        top: '37px',
	        right: '9px',
	        transform: 'rotate(180deg)'
	      }
	    }
	  }, {
	    'hide-triangle': triangle === 'hide',
	    'top-left-triangle': triangle === 'top-left',
	    'top-right-triangle': triangle === 'top-right',
	    'bottom-left-triangle': triangle == 'bottom-left',
	    'bottom-right-triangle': triangle === 'bottom-right'
	  });

	  var handleChange = function handleChange(hex, e) {
	    return onChange({ hex: hex, source: 'hex' }, e);
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.card, className: 'github-picker ' + className },
	    React__default.createElement('div', { style: styles.triangleShadow }),
	    React__default.createElement('div', { style: styles.triangle }),
	    map_1(colors, function (c) {
	      return React__default.createElement(GithubSwatch$1, {
	        color: c,
	        key: c,
	        onClick: handleChange,
	        onSwatchHover: onSwatchHover
	      });
	    })
	  );
	};

	Github.propTypes = {
	  width: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  colors: propTypes.arrayOf(propTypes.string),
	  triangle: propTypes.oneOf(['hide', 'top-left', 'top-right', 'bottom-left', 'bottom-right'])
	};

	Github.defaultProps = {
	  width: 200,
	  colors: ['#B80000', '#DB3E00', '#FCCB00', '#008B02', '#006B76', '#1273DE', '#004DCF', '#5300EB', '#EB9694', '#FAD0C3', '#FEF3BD', '#C1E1C5', '#BEDADC', '#C4DEF6', '#BED3F3', '#D4C4FB'],
	  triangle: 'top-left'
	};

	var Github$1 = ColorWrap(Github);

	var SliderPointer = function SliderPointer(_ref) {
	  var direction = _ref.direction;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '18px',
	        height: '18px',
	        borderRadius: '50%',
	        transform: 'translate(-9px, -1px)',
	        backgroundColor: 'rgb(248, 248, 248)',
	        boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)'
	      }
	    },
	    'vertical': {
	      picker: {
	        transform: 'translate(-3px, -9px)'
	      }
	    }
	  }, { vertical: direction === 'vertical' });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var HuePicker = function HuePicker(_ref) {
	  var width = _ref.width,
	      height = _ref.height,
	      onChange = _ref.onChange,
	      hsl = _ref.hsl,
	      direction = _ref.direction,
	      pointer = _ref.pointer,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        position: 'relative',
	        width: width,
	        height: height
	      },
	      hue: {
	        radius: '2px'
	      }
	    }
	  });

	  // Overwrite to provide pure hue color
	  var handleChange = function handleChange(data) {
	    return onChange({ a: 1, h: data.h, l: 0.5, s: 1 });
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.picker, className: 'hue-picker ' + className },
	    React__default.createElement(Hue, _extends({}, styles.hue, {
	      hsl: hsl,
	      pointer: pointer,
	      onChange: handleChange,
	      direction: direction
	    }))
	  );
	};

	HuePicker.defaultProps = {
	  width: '316px',
	  height: '16px',
	  direction: 'horizontal',
	  pointer: SliderPointer
	};

	var Hue$1 = ColorWrap(HuePicker);

	var Material = function Material(_ref) {
	  var onChange = _ref.onChange,
	      hex = _ref.hex,
	      rgb = _ref.rgb,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      material: {
	        width: '98px',
	        height: '98px',
	        padding: '16px',
	        fontFamily: 'Roboto'
	      },
	      HEXwrap: {
	        position: 'relative'
	      },
	      HEXinput: {
	        width: '100%',
	        marginTop: '12px',
	        fontSize: '15px',
	        color: '#333',
	        padding: '0px',
	        border: '0px',
	        borderBottom: '2px solid ' + hex,
	        outline: 'none',
	        height: '30px'
	      },
	      HEXlabel: {
	        position: 'absolute',
	        top: '0px',
	        left: '0px',
	        fontSize: '11px',
	        color: '#999999',
	        textTransform: 'capitalize'
	      },
	      Hex: {
	        style: {}
	      },
	      RGBwrap: {
	        position: 'relative'
	      },
	      RGBinput: {
	        width: '100%',
	        marginTop: '12px',
	        fontSize: '15px',
	        color: '#333',
	        padding: '0px',
	        border: '0px',
	        borderBottom: '1px solid #eee',
	        outline: 'none',
	        height: '30px'
	      },
	      RGBlabel: {
	        position: 'absolute',
	        top: '0px',
	        left: '0px',
	        fontSize: '11px',
	        color: '#999999',
	        textTransform: 'capitalize'
	      },
	      split: {
	        display: 'flex',
	        marginRight: '-10px',
	        paddingTop: '11px'
	      },
	      third: {
	        flex: '1',
	        paddingRight: '10px'
	      }
	    }
	  });

	  var handleChange = function handleChange(data, e) {
	    if (data.hex) {
	      colorUtils.isValidHex(data.hex) && onChange({
	        hex: data.hex,
	        source: 'hex'
	      }, e);
	    } else if (data.r || data.g || data.b) {
	      onChange({
	        r: data.r || rgb.r,
	        g: data.g || rgb.g,
	        b: data.b || rgb.b,
	        source: 'rgb'
	      }, e);
	    }
	  };

	  return React__default.createElement(
	    Raised,
	    null,
	    React__default.createElement(
	      'div',
	      { style: styles.material, className: 'material-picker ' + className },
	      React__default.createElement(EditableInput, {
	        style: { wrap: styles.HEXwrap, input: styles.HEXinput, label: styles.HEXlabel },
	        label: 'hex',
	        value: hex,
	        onChange: handleChange
	      }),
	      React__default.createElement(
	        'div',
	        { style: styles.split, className: 'flexbox-fix' },
	        React__default.createElement(
	          'div',
	          { style: styles.third },
	          React__default.createElement(EditableInput, {
	            style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	            label: 'r', value: rgb.r,
	            onChange: handleChange
	          })
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.third },
	          React__default.createElement(EditableInput, {
	            style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	            label: 'g',
	            value: rgb.g,
	            onChange: handleChange
	          })
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.third },
	          React__default.createElement(EditableInput, {
	            style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	            label: 'b',
	            value: rgb.b,
	            onChange: handleChange
	          })
	        )
	      )
	    )
	  );
	};

	var Material$1 = ColorWrap(Material);

	var PhotoshopPicker = function PhotoshopPicker(_ref) {
	  var onChange = _ref.onChange,
	      rgb = _ref.rgb,
	      hsv = _ref.hsv,
	      hex = _ref.hex;

	  var styles = reactCSS({
	    'default': {
	      fields: {
	        paddingTop: '5px',
	        paddingBottom: '9px',
	        width: '80px',
	        position: 'relative'
	      },
	      divider: {
	        height: '5px'
	      },
	      RGBwrap: {
	        position: 'relative'
	      },
	      RGBinput: {
	        marginLeft: '40%',
	        width: '40%',
	        height: '18px',
	        border: '1px solid #888888',
	        boxShadow: 'inset 0 1px 1px rgba(0,0,0,.1), 0 1px 0 0 #ECECEC',
	        marginBottom: '5px',
	        fontSize: '13px',
	        paddingLeft: '3px',
	        marginRight: '10px'
	      },
	      RGBlabel: {
	        left: '0px',
	        width: '34px',
	        textTransform: 'uppercase',
	        fontSize: '13px',
	        height: '18px',
	        lineHeight: '22px',
	        position: 'absolute'
	      },
	      HEXwrap: {
	        position: 'relative'
	      },
	      HEXinput: {
	        marginLeft: '20%',
	        width: '80%',
	        height: '18px',
	        border: '1px solid #888888',
	        boxShadow: 'inset 0 1px 1px rgba(0,0,0,.1), 0 1px 0 0 #ECECEC',
	        marginBottom: '6px',
	        fontSize: '13px',
	        paddingLeft: '3px'
	      },
	      HEXlabel: {
	        position: 'absolute',
	        top: '0px',
	        left: '0px',
	        width: '14px',
	        textTransform: 'uppercase',
	        fontSize: '13px',
	        height: '18px',
	        lineHeight: '22px'
	      },
	      fieldSymbols: {
	        position: 'absolute',
	        top: '5px',
	        right: '-7px',
	        fontSize: '13px'
	      },
	      symbol: {
	        height: '20px',
	        lineHeight: '22px',
	        paddingBottom: '7px'
	      }
	    }
	  });

	  var handleChange = function handleChange(data, e) {
	    if (data['#']) {
	      colorUtils.isValidHex(data['#']) && onChange({
	        hex: data['#'],
	        source: 'hex'
	      }, e);
	    } else if (data.r || data.g || data.b) {
	      onChange({
	        r: data.r || rgb.r,
	        g: data.g || rgb.g,
	        b: data.b || rgb.b,
	        source: 'rgb'
	      }, e);
	    } else if (data.h || data.s || data.v) {
	      onChange({
	        h: data.h || hsv.h,
	        s: data.s || hsv.s,
	        v: data.v || hsv.v,
	        source: 'hsv'
	      }, e);
	    }
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.fields },
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'h',
	      value: Math.round(hsv.h),
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 's',
	      value: Math.round(hsv.s * 100),
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'v',
	      value: Math.round(hsv.v * 100),
	      onChange: handleChange
	    }),
	    React__default.createElement('div', { style: styles.divider }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'r',
	      value: rgb.r,
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'g',
	      value: rgb.g,
	      onChange: handleChange
	    }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.RGBwrap, input: styles.RGBinput, label: styles.RGBlabel },
	      label: 'b',
	      value: rgb.b,
	      onChange: handleChange
	    }),
	    React__default.createElement('div', { style: styles.divider }),
	    React__default.createElement(EditableInput, {
	      style: { wrap: styles.HEXwrap, input: styles.HEXinput, label: styles.HEXlabel },
	      label: '#',
	      value: hex.replace('#', ''),
	      onChange: handleChange
	    }),
	    React__default.createElement(
	      'div',
	      { style: styles.fieldSymbols },
	      React__default.createElement(
	        'div',
	        { style: styles.symbol },
	        '\xB0'
	      ),
	      React__default.createElement(
	        'div',
	        { style: styles.symbol },
	        '%'
	      ),
	      React__default.createElement(
	        'div',
	        { style: styles.symbol },
	        '%'
	      )
	    )
	  );
	};

	var PhotoshopPointerCircle = function PhotoshopPointerCircle(_ref) {
	  var hsl = _ref.hsl;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '12px',
	        height: '12px',
	        borderRadius: '6px',
	        boxShadow: 'inset 0 0 0 1px #fff',
	        transform: 'translate(-6px, -6px)'
	      }
	    },
	    'black-outline': {
	      picker: {
	        boxShadow: 'inset 0 0 0 1px #000'
	      }
	    }
	  }, { 'black-outline': hsl.l > 0.5 });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var PhotoshopPointerCircle$1 = function PhotoshopPointerCircle() {
	  var styles = reactCSS({
	    'default': {
	      triangle: {
	        width: 0,
	        height: 0,
	        borderStyle: 'solid',
	        borderWidth: '4px 0 4px 6px',
	        borderColor: 'transparent transparent transparent #fff',
	        position: 'absolute',
	        top: '1px',
	        left: '1px'
	      },
	      triangleBorder: {
	        width: 0,
	        height: 0,
	        borderStyle: 'solid',
	        borderWidth: '5px 0 5px 8px',
	        borderColor: 'transparent transparent transparent #555'
	      },

	      left: {
	        Extend: 'triangleBorder',
	        transform: 'translate(-13px, -4px)'
	      },
	      leftInside: {
	        Extend: 'triangle',
	        transform: 'translate(-8px, -5px)'
	      },

	      right: {
	        Extend: 'triangleBorder',
	        transform: 'translate(20px, -14px) rotate(180deg)'
	      },
	      rightInside: {
	        Extend: 'triangle',
	        transform: 'translate(-8px, -5px)'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { style: styles.pointer },
	    React__default.createElement(
	      'div',
	      { style: styles.left },
	      React__default.createElement('div', { style: styles.leftInside })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.right },
	      React__default.createElement('div', { style: styles.rightInside })
	    )
	  );
	};

	var PhotoshopBotton = function PhotoshopBotton(_ref) {
	  var onClick = _ref.onClick,
	      label = _ref.label,
	      children = _ref.children,
	      active = _ref.active;

	  var styles = reactCSS({
	    'default': {
	      button: {
	        backgroundImage: 'linear-gradient(-180deg, #FFFFFF 0%, #E6E6E6 100%)',
	        border: '1px solid #878787',
	        borderRadius: '2px',
	        height: '20px',
	        boxShadow: '0 1px 0 0 #EAEAEA',
	        fontSize: '14px',
	        color: '#000',
	        lineHeight: '20px',
	        textAlign: 'center',
	        marginBottom: '10px',
	        cursor: 'pointer'
	      }
	    },
	    'active': {
	      button: {
	        boxShadow: '0 0 0 1px #878787'
	      }
	    }
	  }, { active: active });

	  return React__default.createElement(
	    'div',
	    { style: styles.button, onClick: onClick },
	    label || children
	  );
	};

	var PhotoshopPreviews = function PhotoshopPreviews(_ref) {
	  var rgb = _ref.rgb,
	      currentColor = _ref.currentColor;

	  var styles = reactCSS({
	    'default': {
	      swatches: {
	        border: '1px solid #B3B3B3',
	        borderBottom: '1px solid #F0F0F0',
	        marginBottom: '2px',
	        marginTop: '1px'
	      },
	      new: {
	        height: '34px',
	        background: 'rgb(' + rgb.r + ',' + rgb.g + ', ' + rgb.b + ')',
	        boxShadow: 'inset 1px 0 0 #000, inset -1px 0 0 #000, inset 0 1px 0 #000'
	      },
	      current: {
	        height: '34px',
	        background: currentColor,
	        boxShadow: 'inset 1px 0 0 #000, inset -1px 0 0 #000, inset 0 -1px 0 #000'
	      },
	      label: {
	        fontSize: '14px',
	        color: '#000',
	        textAlign: 'center'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    null,
	    React__default.createElement(
	      'div',
	      { style: styles.label },
	      'new'
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatches },
	      React__default.createElement('div', { style: styles.new }),
	      React__default.createElement('div', { style: styles.current })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.label },
	      'current'
	    )
	  );
	};

	var Photoshop = function (_React$Component) {
	  inherits(Photoshop, _React$Component);

	  function Photoshop(props) {
	    classCallCheck(this, Photoshop);

	    var _this = possibleConstructorReturn(this, (Photoshop.__proto__ || Object.getPrototypeOf(Photoshop)).call(this));

	    _this.state = {
	      currentColor: props.hex
	    };
	    return _this;
	  }

	  createClass(Photoshop, [{
	    key: 'render',
	    value: function render() {
	      var _props$className = this.props.className,
	          className = _props$className === undefined ? '' : _props$className;

	      var styles = reactCSS({
	        'default': {
	          picker: {
	            background: '#DCDCDC',
	            borderRadius: '4px',
	            boxShadow: '0 0 0 1px rgba(0,0,0,.25), 0 8px 16px rgba(0,0,0,.15)',
	            boxSizing: 'initial',
	            width: '513px'
	          },
	          head: {
	            backgroundImage: 'linear-gradient(-180deg, #F0F0F0 0%, #D4D4D4 100%)',
	            borderBottom: '1px solid #B1B1B1',
	            boxShadow: 'inset 0 1px 0 0 rgba(255,255,255,.2), inset 0 -1px 0 0 rgba(0,0,0,.02)',
	            height: '23px',
	            lineHeight: '24px',
	            borderRadius: '4px 4px 0 0',
	            fontSize: '13px',
	            color: '#4D4D4D',
	            textAlign: 'center'
	          },
	          body: {
	            padding: '15px 15px 0',
	            display: 'flex'
	          },
	          saturation: {
	            width: '256px',
	            height: '256px',
	            position: 'relative',
	            border: '2px solid #B3B3B3',
	            borderBottom: '2px solid #F0F0F0',
	            overflow: 'hidden'
	          },
	          hue: {
	            position: 'relative',
	            height: '256px',
	            width: '19px',
	            marginLeft: '10px',
	            border: '2px solid #B3B3B3',
	            borderBottom: '2px solid #F0F0F0'
	          },
	          controls: {
	            width: '180px',
	            marginLeft: '10px'
	          },
	          top: {
	            display: 'flex'
	          },
	          previews: {
	            width: '60px'
	          },
	          actions: {
	            flex: '1',
	            marginLeft: '20px'
	          }
	        }
	      });

	      return React__default.createElement(
	        'div',
	        { style: styles.picker, className: 'photoshop-picker ' + className },
	        React__default.createElement(
	          'div',
	          { style: styles.head },
	          this.props.header
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.body, className: 'flexbox-fix' },
	          React__default.createElement(
	            'div',
	            { style: styles.saturation },
	            React__default.createElement(Saturation, {
	              hsl: this.props.hsl,
	              hsv: this.props.hsv,
	              pointer: PhotoshopPointerCircle,
	              onChange: this.props.onChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.hue },
	            React__default.createElement(Hue, {
	              direction: 'vertical',
	              hsl: this.props.hsl,
	              pointer: PhotoshopPointerCircle$1,
	              onChange: this.props.onChange
	            })
	          ),
	          React__default.createElement(
	            'div',
	            { style: styles.controls },
	            React__default.createElement(
	              'div',
	              { style: styles.top, className: 'flexbox-fix' },
	              React__default.createElement(
	                'div',
	                { style: styles.previews },
	                React__default.createElement(PhotoshopPreviews, {
	                  rgb: this.props.rgb,
	                  currentColor: this.state.currentColor
	                })
	              ),
	              React__default.createElement(
	                'div',
	                { style: styles.actions },
	                React__default.createElement(PhotoshopBotton, { label: 'OK', onClick: this.props.onAccept, active: true }),
	                React__default.createElement(PhotoshopBotton, { label: 'Cancel', onClick: this.props.onCancel }),
	                React__default.createElement(PhotoshopPicker, {
	                  onChange: this.props.onChange,
	                  rgb: this.props.rgb,
	                  hsv: this.props.hsv,
	                  hex: this.props.hex
	                })
	              )
	            )
	          )
	        )
	      );
	    }
	  }]);
	  return Photoshop;
	}(React__default.Component);

	Photoshop.propTypes = {
	  header: propTypes.string
	};

	Photoshop.defaultProps = {
	  header: 'Color Picker'
	};

	var Photoshop$1 = ColorWrap(Photoshop);

	/* eslint-disable no-param-reassign */

	var SketchFields = function SketchFields(_ref) {
	  var onChange = _ref.onChange,
	      rgb = _ref.rgb,
	      hsl = _ref.hsl,
	      hex = _ref.hex,
	      disableAlpha = _ref.disableAlpha;

	  var styles = reactCSS({
	    'default': {
	      fields: {
	        display: 'flex',
	        paddingTop: '4px'
	      },
	      single: {
	        flex: '1',
	        paddingLeft: '6px'
	      },
	      alpha: {
	        flex: '1',
	        paddingLeft: '6px'
	      },
	      double: {
	        flex: '2'
	      },
	      input: {
	        width: '80%',
	        padding: '4px 10% 3px',
	        border: 'none',
	        boxShadow: 'inset 0 0 0 1px #ccc',
	        fontSize: '11px'
	      },
	      label: {
	        display: 'block',
	        textAlign: 'center',
	        fontSize: '11px',
	        color: '#222',
	        paddingTop: '3px',
	        paddingBottom: '4px',
	        textTransform: 'capitalize'
	      }
	    },
	    'disableAlpha': {
	      alpha: {
	        display: 'none'
	      }
	    }
	  }, { disableAlpha: disableAlpha });

	  var handleChange = function handleChange(data, e) {
	    if (data.hex) {
	      colorUtils.isValidHex(data.hex) && onChange({
	        hex: data.hex,
	        source: 'hex'
	      }, e);
	    } else if (data.r || data.g || data.b) {
	      onChange({
	        r: data.r || rgb.r,
	        g: data.g || rgb.g,
	        b: data.b || rgb.b,
	        a: rgb.a,
	        source: 'rgb'
	      }, e);
	    } else if (data.a) {
	      if (data.a < 0) {
	        data.a = 0;
	      } else if (data.a > 100) {
	        data.a = 100;
	      }

	      data.a /= 100;
	      onChange({
	        h: hsl.h,
	        s: hsl.s,
	        l: hsl.l,
	        a: data.a,
	        source: 'rgb'
	      }, e);
	    }
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.fields, className: 'flexbox-fix' },
	    React__default.createElement(
	      'div',
	      { style: styles.double },
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input, label: styles.label },
	        label: 'hex',
	        value: hex.replace('#', ''),
	        onChange: handleChange
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.single },
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input, label: styles.label },
	        label: 'r',
	        value: rgb.r,
	        onChange: handleChange,
	        dragLabel: 'true',
	        dragMax: '255'
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.single },
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input, label: styles.label },
	        label: 'g',
	        value: rgb.g,
	        onChange: handleChange,
	        dragLabel: 'true',
	        dragMax: '255'
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.single },
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input, label: styles.label },
	        label: 'b',
	        value: rgb.b,
	        onChange: handleChange,
	        dragLabel: 'true',
	        dragMax: '255'
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.alpha },
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input, label: styles.label },
	        label: 'a',
	        value: Math.round(rgb.a * 100),
	        onChange: handleChange,
	        dragLabel: 'true',
	        dragMax: '100'
	      })
	    )
	  );
	};

	var SketchPresetColors = function SketchPresetColors(_ref) {
	  var colors = _ref.colors,
	      _ref$onClick = _ref.onClick,
	      onClick = _ref$onClick === undefined ? function () {} : _ref$onClick,
	      onSwatchHover = _ref.onSwatchHover;

	  var styles = reactCSS({
	    'default': {
	      colors: {
	        margin: '0 -10px',
	        padding: '10px 0 0 10px',
	        borderTop: '1px solid #eee',
	        display: 'flex',
	        flexWrap: 'wrap',
	        position: 'relative'
	      },
	      swatchWrap: {
	        width: '16px',
	        height: '16px',
	        margin: '0 10px 10px 0'
	      },
	      swatch: {
	        borderRadius: '3px',
	        boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.15)'
	      }
	    },
	    'no-presets': {
	      colors: {
	        display: 'none'
	      }
	    }
	  }, {
	    'no-presets': !colors || !colors.length
	  });

	  var handleClick = function handleClick(hex, e) {
	    onClick({
	      hex: hex,
	      source: 'hex'
	    }, e);
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.colors, className: 'flexbox-fix' },
	    colors.map(function (colorObjOrString) {
	      var c = typeof colorObjOrString === 'string' ? { color: colorObjOrString } : colorObjOrString;
	      var key = '' + c.color + (c.title || '');
	      return React__default.createElement(
	        'div',
	        { key: key, style: styles.swatchWrap },
	        React__default.createElement(Swatch$1, _extends({}, c, {
	          style: styles.swatch,
	          onClick: handleClick,
	          onHover: onSwatchHover,
	          focusStyle: {
	            boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.15), 0 0 4px ' + c.color
	          }
	        }))
	      );
	    })
	  );
	};

	SketchPresetColors.propTypes = {
	  colors: propTypes.arrayOf(propTypes.oneOfType([propTypes.string, propTypes.shape({
	    color: propTypes.string,
	    title: propTypes.string
	  })])).isRequired
	};

	var Sketch = function Sketch(_ref) {
	  var width = _ref.width,
	      rgb = _ref.rgb,
	      hex = _ref.hex,
	      hsv = _ref.hsv,
	      hsl = _ref.hsl,
	      onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      disableAlpha = _ref.disableAlpha,
	      presetColors = _ref.presetColors,
	      renderers = _ref.renderers,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: width,
	        padding: '10px 10px 0',
	        boxSizing: 'initial',
	        background: '#fff',
	        borderRadius: '4px',
	        boxShadow: '0 0 0 1px rgba(0,0,0,.15), 0 8px 16px rgba(0,0,0,.15)'
	      },
	      saturation: {
	        width: '100%',
	        paddingBottom: '75%',
	        position: 'relative',
	        overflow: 'hidden'
	      },
	      Saturation: {
	        radius: '3px',
	        shadow: 'inset 0 0 0 1px rgba(0,0,0,.15), inset 0 0 4px rgba(0,0,0,.25)'
	      },
	      controls: {
	        display: 'flex'
	      },
	      sliders: {
	        padding: '4px 0',
	        flex: '1'
	      },
	      color: {
	        width: '24px',
	        height: '24px',
	        position: 'relative',
	        marginTop: '4px',
	        marginLeft: '4px',
	        borderRadius: '3px'
	      },
	      activeColor: {
	        absolute: '0px 0px 0px 0px',
	        borderRadius: '2px',
	        background: 'rgba(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ',' + rgb.a + ')',
	        boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.15), inset 0 0 4px rgba(0,0,0,.25)'
	      },
	      hue: {
	        position: 'relative',
	        height: '10px',
	        overflow: 'hidden'
	      },
	      Hue: {
	        radius: '2px',
	        shadow: 'inset 0 0 0 1px rgba(0,0,0,.15), inset 0 0 4px rgba(0,0,0,.25)'
	      },

	      alpha: {
	        position: 'relative',
	        height: '10px',
	        marginTop: '4px',
	        overflow: 'hidden'
	      },
	      Alpha: {
	        radius: '2px',
	        shadow: 'inset 0 0 0 1px rgba(0,0,0,.15), inset 0 0 4px rgba(0,0,0,.25)'
	      }
	    },
	    'disableAlpha': {
	      color: {
	        height: '10px'
	      },
	      hue: {
	        height: '10px'
	      },
	      alpha: {
	        display: 'none'
	      }
	    }
	  }, { disableAlpha: disableAlpha });

	  return React__default.createElement(
	    'div',
	    { style: styles.picker, className: 'sketch-picker ' + className },
	    React__default.createElement(
	      'div',
	      { style: styles.saturation },
	      React__default.createElement(Saturation, {
	        style: styles.Saturation,
	        hsl: hsl,
	        hsv: hsv,
	        onChange: onChange
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.controls, className: 'flexbox-fix' },
	      React__default.createElement(
	        'div',
	        { style: styles.sliders },
	        React__default.createElement(
	          'div',
	          { style: styles.hue },
	          React__default.createElement(Hue, {
	            style: styles.Hue,
	            hsl: hsl,
	            onChange: onChange
	          })
	        ),
	        React__default.createElement(
	          'div',
	          { style: styles.alpha },
	          React__default.createElement(Alpha, {
	            style: styles.Alpha,
	            rgb: rgb,
	            hsl: hsl,
	            renderers: renderers,
	            onChange: onChange
	          })
	        )
	      ),
	      React__default.createElement(
	        'div',
	        { style: styles.color },
	        React__default.createElement(Checkboard, null),
	        React__default.createElement('div', { style: styles.activeColor })
	      )
	    ),
	    React__default.createElement(SketchFields, {
	      rgb: rgb,
	      hsl: hsl,
	      hex: hex,
	      onChange: onChange,
	      disableAlpha: disableAlpha
	    }),
	    React__default.createElement(SketchPresetColors, {
	      colors: presetColors,
	      onClick: onChange,
	      onSwatchHover: onSwatchHover
	    })
	  );
	};

	Sketch.propTypes = {
	  disableAlpha: propTypes.bool,
	  width: propTypes.oneOfType([propTypes.string, propTypes.number])
	};

	Sketch.defaultProps = {
	  disableAlpha: false,
	  width: 200,
	  presetColors: ['#D0021B', '#F5A623', '#F8E71C', '#8B572A', '#7ED321', '#417505', '#BD10E0', '#9013FE', '#4A90E2', '#50E3C2', '#B8E986', '#000000', '#4A4A4A', '#9B9B9B', '#FFFFFF']
	};

	var Sketch$1 = ColorWrap(Sketch);

	var SliderSwatch = function SliderSwatch(_ref) {
	  var hsl = _ref.hsl,
	      offset = _ref.offset,
	      _ref$onClick = _ref.onClick,
	      onClick = _ref$onClick === undefined ? function () {} : _ref$onClick,
	      active = _ref.active,
	      first = _ref.first,
	      last = _ref.last;

	  var styles = reactCSS({
	    'default': {
	      swatch: {
	        height: '12px',
	        background: 'hsl(' + hsl.h + ', 50%, ' + offset * 100 + '%)',
	        cursor: 'pointer'
	      }
	    },
	    'first': {
	      swatch: {
	        borderRadius: '2px 0 0 2px'
	      }
	    },
	    'last': {
	      swatch: {
	        borderRadius: '0 2px 2px 0'
	      }
	    },
	    'active': {
	      swatch: {
	        transform: 'scaleY(1.8)',
	        borderRadius: '3.6px/2px'
	      }
	    }
	  }, { active: active, first: first, last: last });

	  var handleClick = function handleClick(e) {
	    return onClick({
	      h: hsl.h,
	      s: 0.5,
	      l: offset,
	      source: 'hsl'
	    }, e);
	  };

	  return React__default.createElement('div', { style: styles.swatch, onClick: handleClick });
	};

	var SliderSwatches = function SliderSwatches(_ref) {
	  var onClick = _ref.onClick,
	      hsl = _ref.hsl;

	  var styles = reactCSS({
	    'default': {
	      swatches: {
	        marginTop: '20px'
	      },
	      swatch: {
	        boxSizing: 'border-box',
	        width: '20%',
	        paddingRight: '1px',
	        float: 'left'
	      },
	      clear: {
	        clear: 'both'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { style: styles.swatches },
	    React__default.createElement(
	      'div',
	      { style: styles.swatch },
	      React__default.createElement(SliderSwatch, {
	        hsl: hsl,
	        offset: '.80',
	        active: Math.round(hsl.l * 100) / 100 === 0.80 && Math.round(hsl.s * 100) / 100 === 0.50,
	        onClick: onClick,
	        first: true
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatch },
	      React__default.createElement(SliderSwatch, {
	        hsl: hsl,
	        offset: '.65',
	        active: Math.round(hsl.l * 100) / 100 === 0.65 && Math.round(hsl.s * 100) / 100 === 0.50,
	        onClick: onClick
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatch },
	      React__default.createElement(SliderSwatch, {
	        hsl: hsl,
	        offset: '.50',
	        active: Math.round(hsl.l * 100) / 100 === 0.50 && Math.round(hsl.s * 100) / 100 === 0.50,
	        onClick: onClick
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatch },
	      React__default.createElement(SliderSwatch, {
	        hsl: hsl,
	        offset: '.35',
	        active: Math.round(hsl.l * 100) / 100 === 0.35 && Math.round(hsl.s * 100) / 100 === 0.50,
	        onClick: onClick
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatch },
	      React__default.createElement(SliderSwatch, {
	        hsl: hsl,
	        offset: '.20',
	        active: Math.round(hsl.l * 100) / 100 === 0.20 && Math.round(hsl.s * 100) / 100 === 0.50,
	        onClick: onClick,
	        last: true
	      })
	    ),
	    React__default.createElement('div', { style: styles.clear })
	  );
	};

	var SliderPointer$1 = function SliderPointer() {
	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: '14px',
	        height: '14px',
	        borderRadius: '6px',
	        transform: 'translate(-7px, -1px)',
	        backgroundColor: 'rgb(248, 248, 248)',
	        boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)'
	      }
	    }
	  });

	  return React__default.createElement('div', { style: styles.picker });
	};

	var Slider = function Slider(_ref) {
	  var hsl = _ref.hsl,
	      onChange = _ref.onChange,
	      pointer = _ref.pointer,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      hue: {
	        height: '12px',
	        position: 'relative'
	      },
	      Hue: {
	        radius: '2px'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { className: 'slider-picker ' + className },
	    React__default.createElement(
	      'div',
	      { style: styles.hue },
	      React__default.createElement(Hue, {
	        style: styles.Hue,
	        hsl: hsl,
	        pointer: pointer,
	        onChange: onChange
	      })
	    ),
	    React__default.createElement(
	      'div',
	      { style: styles.swatches },
	      React__default.createElement(SliderSwatches, { hsl: hsl, onClick: onChange })
	    )
	  );
	};

	Slider.defaultProps = {
	  pointer: SliderPointer$1
	};

	var Slider$1 = ColorWrap(Slider);

	var CheckIcon = createCommonjsModule(function (module, exports) {

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };



	var _react2 = _interopRequireDefault(React__default);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

	var DEFAULT_SIZE = 24;

	exports.default = function (_ref) {
	  var _ref$fill = _ref.fill,
	      fill = _ref$fill === undefined ? 'currentColor' : _ref$fill,
	      _ref$width = _ref.width,
	      width = _ref$width === undefined ? DEFAULT_SIZE : _ref$width,
	      _ref$height = _ref.height,
	      height = _ref$height === undefined ? DEFAULT_SIZE : _ref$height,
	      _ref$style = _ref.style,
	      style = _ref$style === undefined ? {} : _ref$style,
	      props = _objectWithoutProperties(_ref, ['fill', 'width', 'height', 'style']);

	  return _react2.default.createElement(
	    'svg',
	    _extends({
	      viewBox: '0 0 ' + DEFAULT_SIZE + ' ' + DEFAULT_SIZE,
	      style: _extends({ fill: fill, width: width, height: height }, style)
	    }, props),
	    _react2.default.createElement('path', { d: 'M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z' })
	  );
	};
	});

	var CheckIcon$1 = unwrapExports(CheckIcon);

	var SwatchesColor = function SwatchesColor(_ref) {
	  var color = _ref.color,
	      _ref$onClick = _ref.onClick,
	      onClick = _ref$onClick === undefined ? function () {} : _ref$onClick,
	      onSwatchHover = _ref.onSwatchHover,
	      first = _ref.first,
	      last = _ref.last,
	      active = _ref.active;

	  var styles = reactCSS({
	    'default': {
	      color: {
	        width: '40px',
	        height: '24px',
	        cursor: 'pointer',
	        background: color,
	        marginBottom: '1px'
	      },
	      check: {
	        color: colorUtils.getContrastingColor(color),
	        marginLeft: '8px',
	        display: 'none'
	      }
	    },
	    'first': {
	      color: {
	        overflow: 'hidden',
	        borderRadius: '2px 2px 0 0'
	      }
	    },
	    'last': {
	      color: {
	        overflow: 'hidden',
	        borderRadius: '0 0 2px 2px'
	      }
	    },
	    'active': {
	      check: {
	        display: 'block'
	      }
	    },
	    'color-#FFFFFF': {
	      color: {
	        boxShadow: 'inset 0 0 0 1px #ddd'
	      },
	      check: {
	        color: '#333'
	      }
	    },
	    'transparent': {
	      check: {
	        color: '#333'
	      }
	    }
	  }, {
	    first: first,
	    last: last,
	    active: active,
	    'color-#FFFFFF': color === '#FFFFFF',
	    'transparent': color === 'transparent'
	  });

	  return React__default.createElement(
	    Swatch$1,
	    {
	      color: color,
	      style: styles.color,
	      onClick: onClick,
	      onHover: onSwatchHover,
	      focusStyle: { boxShadow: '0 0 4px ' + color }
	    },
	    React__default.createElement(
	      'div',
	      { style: styles.check },
	      React__default.createElement(CheckIcon$1, null)
	    )
	  );
	};

	var SwatchesGroup = function SwatchesGroup(_ref) {
	  var onClick = _ref.onClick,
	      onSwatchHover = _ref.onSwatchHover,
	      group = _ref.group,
	      active = _ref.active;

	  var styles = reactCSS({
	    'default': {
	      group: {
	        paddingBottom: '10px',
	        width: '40px',
	        float: 'left',
	        marginRight: '10px'
	      }
	    }
	  });

	  return React__default.createElement(
	    'div',
	    { style: styles.group },
	    map_1(group, function (color, i) {
	      return React__default.createElement(SwatchesColor, {
	        key: color,
	        color: color,
	        active: color.toLowerCase() === active,
	        first: i === 0,
	        last: i === group.length - 1,
	        onClick: onClick,
	        onSwatchHover: onSwatchHover
	      });
	    })
	  );
	};

	var Swatches = function Swatches(_ref) {
	  var width = _ref.width,
	      height = _ref.height,
	      onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      colors = _ref.colors,
	      hex = _ref.hex,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      picker: {
	        width: width,
	        height: height
	      },
	      overflow: {
	        height: height,
	        overflowY: 'scroll'
	      },
	      body: {
	        padding: '16px 0 6px 16px'
	      },
	      clear: {
	        clear: 'both'
	      }
	    }
	  });

	  var handleChange = function handleChange(data, e) {
	    colorUtils.isValidHex(data) && onChange({
	      hex: data,
	      source: 'hex'
	    }, e);
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.picker, className: 'swatches-picker ' + className },
	    React__default.createElement(
	      Raised,
	      null,
	      React__default.createElement(
	        'div',
	        { style: styles.overflow },
	        React__default.createElement(
	          'div',
	          { style: styles.body },
	          map_1(colors, function (group) {
	            return React__default.createElement(SwatchesGroup, {
	              key: group.toString(),
	              group: group,
	              active: hex,
	              onClick: handleChange,
	              onSwatchHover: onSwatchHover
	            });
	          }),
	          React__default.createElement('div', { style: styles.clear })
	        )
	      )
	    )
	  );
	};

	Swatches.propTypes = {
	  width: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  height: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  colors: propTypes.arrayOf(propTypes.arrayOf(propTypes.string))

	  /* eslint-disable max-len */
	};Swatches.defaultProps = {
	  width: 320,
	  height: 240,
	  colors: [[red$1['900'], red$1['700'], red$1['500'], red$1['300'], red$1['100']], [pink['900'], pink['700'], pink['500'], pink['300'], pink['100']], [purple['900'], purple['700'], purple['500'], purple['300'], purple['100']], [deepPurple['900'], deepPurple['700'], deepPurple['500'], deepPurple['300'], deepPurple['100']], [indigo['900'], indigo['700'], indigo['500'], indigo['300'], indigo['100']], [blue['900'], blue['700'], blue['500'], blue['300'], blue['100']], [lightBlue['900'], lightBlue['700'], lightBlue['500'], lightBlue['300'], lightBlue['100']], [cyan['900'], cyan['700'], cyan['500'], cyan['300'], cyan['100']], [teal['900'], teal['700'], teal['500'], teal['300'], teal['100']], ['#194D33', green['700'], green['500'], green['300'], green['100']], [lightGreen['900'], lightGreen['700'], lightGreen['500'], lightGreen['300'], lightGreen['100']], [lime['900'], lime['700'], lime['500'], lime['300'], lime['100']], [yellow['900'], yellow['700'], yellow['500'], yellow['300'], yellow['100']], [amber['900'], amber['700'], amber['500'], amber['300'], amber['100']], [orange['900'], orange['700'], orange['500'], orange['300'], orange['100']], [deepOrange['900'], deepOrange['700'], deepOrange['500'], deepOrange['300'], deepOrange['100']], [brown['900'], brown['700'], brown['500'], brown['300'], brown['100']], [blueGrey['900'], blueGrey['700'], blueGrey['500'], blueGrey['300'], blueGrey['100']], ['#000000', '#525252', '#969696', '#D9D9D9', '#FFFFFF']]
	};

	var Swatches$1 = ColorWrap(Swatches);

	var Twitter = function Twitter(_ref) {
	  var onChange = _ref.onChange,
	      onSwatchHover = _ref.onSwatchHover,
	      hex = _ref.hex,
	      colors = _ref.colors,
	      width = _ref.width,
	      triangle = _ref.triangle,
	      _ref$className = _ref.className,
	      className = _ref$className === undefined ? '' : _ref$className;

	  var styles = reactCSS({
	    'default': {
	      card: {
	        width: width,
	        background: '#fff',
	        border: '0 solid rgba(0,0,0,0.25)',
	        boxShadow: '0 1px 4px rgba(0,0,0,0.25)',
	        borderRadius: '4px',
	        position: 'relative'
	      },
	      body: {
	        padding: '15px 9px 9px 15px'
	      },
	      label: {
	        fontSize: '18px',
	        color: '#fff'
	      },
	      triangle: {
	        width: '0px',
	        height: '0px',
	        borderStyle: 'solid',
	        borderWidth: '0 9px 10px 9px',
	        borderColor: 'transparent transparent #fff transparent',
	        position: 'absolute'
	      },
	      triangleShadow: {
	        width: '0px',
	        height: '0px',
	        borderStyle: 'solid',
	        borderWidth: '0 9px 10px 9px',
	        borderColor: 'transparent transparent rgba(0,0,0,.1) transparent',
	        position: 'absolute'
	      },
	      hash: {
	        background: '#F0F0F0',
	        height: '30px',
	        width: '30px',
	        borderRadius: '4px 0 0 4px',
	        float: 'left',
	        color: '#98A1A4',
	        display: 'flex',
	        alignItems: 'center',
	        justifyContent: 'center'
	      },
	      input: {
	        width: '100px',
	        fontSize: '14px',
	        color: '#666',
	        border: '0px',
	        outline: 'none',
	        height: '28px',
	        boxShadow: 'inset 0 0 0 1px #F0F0F0',
	        boxSizing: 'content-box',
	        borderRadius: '0 4px 4px 0',
	        float: 'left',
	        paddingLeft: '8px'
	      },
	      swatch: {
	        width: '30px',
	        height: '30px',
	        float: 'left',
	        borderRadius: '4px',
	        margin: '0 6px 6px 0'
	      },
	      clear: {
	        clear: 'both'
	      }
	    },
	    'hide-triangle': {
	      triangle: {
	        display: 'none'
	      },
	      triangleShadow: {
	        display: 'none'
	      }
	    },
	    'top-left-triangle': {
	      triangle: {
	        top: '-10px',
	        left: '12px'
	      },
	      triangleShadow: {
	        top: '-11px',
	        left: '12px'
	      }
	    },
	    'top-right-triangle': {
	      triangle: {
	        top: '-10px',
	        right: '12px'
	      },
	      triangleShadow: {
	        top: '-11px',
	        right: '12px'
	      }
	    }
	  }, {
	    'hide-triangle': triangle === 'hide',
	    'top-left-triangle': triangle === 'top-left',
	    'top-right-triangle': triangle === 'top-right'
	  });

	  var handleChange = function handleChange(hexcode, e) {
	    colorUtils.isValidHex(hexcode) && onChange({
	      hex: hexcode,
	      source: 'hex'
	    }, e);
	  };

	  return React__default.createElement(
	    'div',
	    { style: styles.card, className: 'twitter-picker ' + className },
	    React__default.createElement('div', { style: styles.triangleShadow }),
	    React__default.createElement('div', { style: styles.triangle }),
	    React__default.createElement(
	      'div',
	      { style: styles.body },
	      map_1(colors, function (c, i) {
	        return React__default.createElement(Swatch$1, {
	          key: i,
	          color: c,
	          hex: c,
	          style: styles.swatch,
	          onClick: handleChange,
	          onHover: onSwatchHover,
	          focusStyle: {
	            boxShadow: '0 0 4px ' + c
	          }
	        });
	      }),
	      React__default.createElement(
	        'div',
	        { style: styles.hash },
	        '#'
	      ),
	      React__default.createElement(EditableInput, {
	        style: { input: styles.input },
	        value: hex.replace('#', ''),
	        onChange: handleChange
	      }),
	      React__default.createElement('div', { style: styles.clear })
	    )
	  );
	};

	Twitter.propTypes = {
	  width: propTypes.oneOfType([propTypes.string, propTypes.number]),
	  triangle: propTypes.oneOf(['hide', 'top-left', 'top-right']),
	  colors: propTypes.arrayOf(propTypes.string)
	};

	Twitter.defaultProps = {
	  width: 276,
	  colors: ['#FF6900', '#FCB900', '#7BDCB5', '#00D084', '#8ED1FC', '#0693E3', '#ABB8C3', '#EB144C', '#F78DA7', '#9900EF'],
	  triangle: 'top-left'
	};

	var Twitter$1 = ColorWrap(Twitter);

	exports.AlphaPicker = Alpha$1;
	exports.BlockPicker = Block$1;
	exports.CirclePicker = Circle$1;
	exports.ChromePicker = Chrome$1;
	exports.CompactPicker = Compact$1;
	exports.GithubPicker = Github$1;
	exports.HuePicker = Hue$1;
	exports.MaterialPicker = Material$1;
	exports.PhotoshopPicker = Photoshop$1;
	exports.SketchPicker = Sketch$1;
	exports.SliderPicker = Slider$1;
	exports.SwatchesPicker = Swatches$1;
	exports.TwitterPicker = Twitter$1;
	exports.CustomPicker = ColorWrap;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
